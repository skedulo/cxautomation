package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features = {"@target/failed_scenarios.txt"},
        glue = {"Hooks", "Features.StepDefinitions"},
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "json:target/rerun-cucumber.json",
                "junit:target/cucumber-reports/cucumber.xml"})
public class RerunFailedTestsRunner extends AbstractTestNGCucumberTests {

}
