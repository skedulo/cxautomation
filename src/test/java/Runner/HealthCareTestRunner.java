package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features = {"src/test/java/Features/Scenarios/AUHealthcare"},
        glue = {"Hooks", "Features.StepDefinitions"},
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "json:target/cucumber.json",
                "junit:target/cucumber-reports/cucumber.xml",
                "rerun:target/rerun.txt"},
        tags = {"@Healthcare"})
public class HealthCareTestRunner extends AbstractTestNGCucumberTests {
}
