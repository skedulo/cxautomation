package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
        features = {"."},
        glue = {"Hooks", "Features.StepDefinitions"},
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "json:target/cucumber.json",
                "junit:target/cucumber-reports/cucumber.xml",
                "rerun:target/failed_scenarios.txt"},
        tags = {"@ARC-TC-SM-036"})
public class TuRunner extends AbstractTestNGCucumberTests {

}
