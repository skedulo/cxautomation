package Transformation;

import cucumber.api.Transformer;

public abstract class UniqueStringTransform extends Transformer<String> {

    public String transform(String prefix, String suffix) {
        return prefix + "_pre_" + suffix + "_suf";

    }


}
