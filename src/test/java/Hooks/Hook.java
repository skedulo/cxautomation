package Hooks;

import APIHelper.SkeduloAPI;
import Base.ExtentReportUtil;
import Base.HashMapHelper;
import Base.LogHelper;
import Objects.User;
import com.aventstack.extentreports.gherkin.model.Feature;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;

import static Base.WebDriverManager.initializeWebDriver;

public class Hook extends ExtentReportUtil {

    private ExtentReportUtil extentReportUtil;

    public Hook(ExtentReportUtil extentReportUtil) {
        this.extentReportUtil = extentReportUtil;
    }

    @Before
    public void TestInitialize(Scenario scenario) {
        LogHelper.getLogger().info("***** Start scenario: " + scenario.getName() + " *****");

        LogHelper.getLogger().info("##### Start Scenario initialization #####");
        if (createNewFeatureFlag) {
            if (extentReportUtil.features == null ||
                    !extentReportUtil.features.getModel().getName().equals(GetFeatureNameFromScenarioId(scenario))) {
                features = extentReportUtil.extentReports.createTest(Feature.class, GetFeatureNameFromScenarioId(scenario));
                LogHelper.getLogger().info("Start feature: " + GetFeatureNameFromScenarioId(scenario));
            } else {
                features = extentReportUtil.extentReports.createTest(Feature.class, "[Rerun] " + GetFeatureNameFromScenarioId(scenario));
                LogHelper.getLogger().info("Rerun feature: " + GetFeatureNameFromScenarioId(scenario));
            }
            createNewFeatureFlag = false;
        }

        extentReportUtil.scenarioDef = extentReportUtil.features.createNode(scenario.getName());

        initializeWebDriver();
        scenarioContext = new HashMapHelper();
        userContext = new User("Admin");
        initializeBrowserAndLoggedInSalesforce();
        deleteObjects();
        LogHelper.getLogger().info("##### Finish scenario initialization #####");
    }

    @After
    public void TearDownTest(Scenario scenario) {
        LogHelper.getLogger().info("##### Start scenario tear down #####");
        if (scenario.isFailed()) {
            try {
                LogHelper.getLogger().info("Scenario is failed. Try to capture screenshot.");
                extentReportUtil.extentReportScreenshot(scenario);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        deleteObjects();
        LogHelper.getLogger().info("##### Finish scenario tear down #####");
        LogHelper.getLogger().info("***** Finish scenario: " + scenario.getName() + " *****");
    }

    private String GetFeatureNameFromScenarioId(Scenario scenario) {
        String featureName = "Feature ";
        String rawFeatureName = scenario.getId().split(";")[0].replace("-", " ");
        featureName = featureName + rawFeatureName.substring(0, 1).toUpperCase() + rawFeatureName.substring(1);
        return featureName;
    }

    private void deleteObjects() {
        BufferedReader reader;
        MultiValuedMap<String, String> objects = new ArrayListValuedHashMap<>();
        try {
            reader = new BufferedReader(new FileReader(needToDeletePropertyFile));
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                String[] params = line.split(":");
                objects.put(params[0], params[1]);
                line = reader.readLine();
            }
            reader.close();
            Files.deleteIfExists(new File(needToDeletePropertyFile).toPath());
        } catch (IOException e) {
            LogHelper.getLogger().info(e.getStackTrace());
        }
        new SkeduloAPI().deleteMultipleObjects(objects);
    }
}

