package Hooks;

import Base.ExtentReportUtil;
import Base.HashMapHelper;
import Base.LogHelper;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.nio.file.Paths;

import static Base.BaseUtil.*;
import static Utilities.GenericFunctions.generateStringWithTimeStamp;

public class NGTestListener implements ITestListener {

    ExtentReportUtil extentReportUtil;

    public NGTestListener() {
        extentReportUtil = new ExtentReportUtil();
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        LogHelper.getLogger().info("######## Start new feature execution ########");
        createNewFeatureFlag = true;
        LogHelper.getLogger().info("######## Initialize for feature execution ########");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        LogHelper.getLogger().info("######## Finish feature ########");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        LogHelper.getLogger().info("On test failure");
        LogHelper.getLogger().error(iTestResult.getThrowable().getMessage());
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        LogHelper.getLogger().info("On test skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        LogHelper.getLogger().info("On test percentage");
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        //region Create report sub folder
        String subFolder = Paths.get(reportLocation, generateStringWithTimeStamp("report_", "")).toString();
        boolean folderStatus = new File(subFolder).mkdir();
        if (folderStatus)
            reportLocation = subFolder;
        //endregion

        //region Initialize log helper
        LogHelper.initializeLogger(Paths.get(reportLocation, generateStringWithTimeStamp("log", ".log")).toString());
        LogHelper.getLogger().info("************* Start execution ******************");
        //endregion

        //region Assign parameters
        LogHelper.getLogger().info("Set variables from testng xml file");
        extentReportUtil.baseConfiguration.getBaseConfigurationFromTestNGXML(iTestContext);
        //endregion

        // Initialize Extent Report
        extentReportUtil.extentReport();

        // Initialize Execution Context
        executionContext = new HashMapHelper();
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        extentReportUtil.flushReport();
        LogHelper.getLogger().info("************* Finish execution ******************");
        driver.quit();
    }
}
