package Pages.Salesforce;

import APIHelper.SkeduloAPI;
import Base.BaseUtil;
import Base.LogHelper;
import Objects.Job;
import Pages.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.ArrayList;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.storePropertyToFile;

public class SfScheduleJobPage {

    private WebDriver driver;

    public SfScheduleJobPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "input[name='startDate']")
    public WebElement txtStartDate;

    @FindBy(how = How.CSS, using = "input[name='startTime']")
    public WebElement txtStartTime;

    @FindBy(how = How.CSS, using = "input[name='jobDuration']")
    public WebElement txtJobDuration;

    @FindBy(how = How.CSS, using = "select[name='jobUrgency']")
    public WebElement ddlJobUrgency;

    @FindBy(how = How.CSS, using = "textarea[name='description']")
    public WebElement txtDescription;

    @FindBy(how = How.CSS, using = "div[name='region'] > div > div > input")
    public WebElement txtRegion;

    @FindBy(how = How.CSS, using = "select[name^='serviceDeliveryMethod']")
    public WebElement ddlServiceDeliveryMethod;

    @FindBy(how = How.CSS, using = "select[name^='serviceCategory']")
    public WebElement ddlServiceCategory;

    @FindBy(how = How.CSS, using = "select[name^='serviceAgreementItem']")
    public WebElement ddlServiceAgreementItem;

    @FindBy(how = How.XPATH, using = "//*[text()='Create Job']")
    public WebElement btnCreateJob;

    @FindBy(how = How.XPATH, using = "//*[text()='Save']")
    public WebElement btnSaveOnConfictCheck;

    @FindBy(how = How.XPATH, using = "//*[text()='Go to Job Details Page']")
    public WebElement btnGotoJobDetailsPage;

    public void scheduleJob(Job job) throws IOException {
        WebElement iframe = driver.findElement(By.xpath("//iframe"));
        driver.switchTo().frame(iframe);
        waitForSpinnerToDisappear();
        waitForElement(btnCreateJob);
        sendKeys(txtStartDate, job.getStartDate());
        txtStartDate.sendKeys(Keys.ESCAPE);
        sendKeys(txtStartTime, job.getStartTime());
        txtStartTime.sendKeys(Keys.ESCAPE);
        sendKeys(txtJobDuration, String.valueOf(job.getDuration()));
        sendKeys(txtDescription, job.getDescription());
        click(txtRegion);
        click(driver.findElement(By.cssSelector("div[title='" + job.getRegion() + "']")));
        setServiceAgreement();
        click(btnCreateJob);
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//*[text()='Save']")))
            click(btnSaveOnConfictCheck);

        // Get Job UID
        String jobUID = new SkeduloAPI().getJobID(String.format("Description=='%s'", job.getDescription()));
        job.setUID(jobUID);

        if (jobUID != null) {
            storePropertyToFile("Jobs", jobUID, needToDeletePropertyFile);
            LogHelper.getLogger().info(String.format("Job UID: %s", jobUID));
            LogHelper.getLogger().info("Job is scheduled successfully");
        } else {
            LogHelper.getLogger().info("Some thing went wrong on scheduling new job");
        }
    }

    public void setServiceAgreement() {
        Select selectServiceDeliveryMethod = new Select(ddlServiceDeliveryMethod);
        selectServiceDeliveryMethod.selectByVisibleText("Scheduled Time");
        Select selectServiceCategory = new Select(ddlServiceCategory);
        selectServiceCategory.selectByVisibleText("Uncategorised");
        Select selectServiceAgreementItem = new Select(ddlServiceAgreementItem);
        selectServiceAgreementItem.selectByIndex(1);
    }

    public void allocateResource(String resource, String jobAlias) {
        click(driver.findElement(By.cssSelector("button[ng-click*='getJobCostingAndAllocate']")));
        waitForSpinnerToDisappear();
        click(driver.findElement(By.cssSelector("button[ng-click*='ctrl.refreshAvailableResources']")));
        waitForSpinnerToDisappear();
        click(driver.findElement(By.xpath(String.format("//li[contains(@ng-click,'toggleResourceSelection') and .//p/text()='%s']", resource))));
        click(driver.findElement(By.xpath(String.format("//p[@title='%s']/ancestor::li[contains(@ng-click,'toggleGetCostingResourceSelection')]//button[contains(@ng-click,'allocateResource')]", resource))));
        click(driver.findElement(By.cssSelector("button[ng-click*='ctrl.saveJob']")));
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//*[text()='Save']")))
            click(btnSaveOnConfictCheck);
        LogHelper.getLogger().info(String.format("Resource %s is allocated successfully through Salesforce", resource));
    }

    public void goToJobDetailsPage() {
        click(btnGotoJobDetailsPage);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size() - 1));
        waitForPageToLoad();
        By button = By.cssSelector("button[data-sk-value='salesforce-sandbox']");
        if ("Production".equals(userContext.getEnvironmentType())) {
            button = By.cssSelector("button[data-sk-value='salesforce']");
        }
        if (checkElementExist(button, 3)) {
            click(driver.findElement(button));
            waitForPageToLoad();
            if (checkElementExist(By.id("username"))) {
                PageManager pageManager = new PageManager(driver);
                pageManager.getSfLoginPage().login(userContext.getUsername(), userContext.getPassword());
                waitForPageToLoad();
            }
        }
    }
}
