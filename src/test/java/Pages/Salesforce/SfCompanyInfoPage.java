package Pages.Salesforce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class SfCompanyInfoPage {

    private WebDriver driver;

    public SfCompanyInfoPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String GetCompanyOrgId() {
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@title,'Company Information')]")));
        WebElement element = driver.findElement(By.xpath("//td[text()='Environment Hub Org Id']/following-sibling::td[1]"));
        return element.getText();
    }
}
