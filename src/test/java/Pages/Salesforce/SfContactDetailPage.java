package Pages.Salesforce;

import Base.LogHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static Base.BaseUIActions.click;

public class SfContactDetailPage {
    private WebDriver driver;

    public SfContactDetailPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//button[@class='slds-button slds-button_neutral skedhealthcareSkedLightningSingleBookingPopupModal']")
    public WebElement btnScheduleJob;

    public void clickScheduleJob(){
        click(btnScheduleJob);
        LogHelper.getLogger().info("Click Schedule Job button");
    }
}
