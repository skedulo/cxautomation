package Pages.Salesforce.ARC;

import Base.LogHelper;
import Objects.Job;
import Objects.TrainingEvent;
import Pages.Salesforce.SfJobDetailsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.convertDateTimeWithTimezone;
import static Utilities.GenericFunctions.formatDateTime;

public class SfTrainingEventPage {
    private WebDriver driver;

    public SfTrainingEventPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//button[.//text()='Schedule Full Service']")
    public WebElement btnScheduleFullService;

    @FindBy(how = How.XPATH, using = "//button[.//text()='Manage Resource']")
    public WebElement btnManageResource;

    @FindBy(how = How.XPATH, using = "//button[.//text()='Cancel Jobs']")
    public WebElement btnCancelJobs;

    @FindBy(how = How.CSS, using = "table.slds-table")
    public WebElement tblTrainingEventRelatedJobs;

    @FindBy(how = How.CSS, using = "button[ng-click='$ctrl.cancelTrainingEvent()']")
    public WebElement btnYesToCancelJobs;


    public void openTrainingEvent(String uid) {
        driver.navigate().to(String.format("%s/lightning/r/%s/view", userContext.getLoginResource(), uid));
    }

    public void openTrainingEventRelatedJobList(String uid) {
        driver.navigate().to(String.format("%s/lightning/r/%s/related/Jobs__r/view", userContext.getLoginResource(), uid));
    }

    public void checkJobsInfo(String trainingEventAlias) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);

        checkTrainingEventRelatedJobList(trainingEventAlias);

        for (String jobAlias : trainingEvent.getJobs().keySet()) {
            Job job = trainingEvent.getJobs().get(jobAlias);
            scenarioContext.setValue(jobAlias, job);
            SfJobDetailsPage jobDetailsPage = new SfJobDetailsPage(driver);
            jobDetailsPage.openJobDetails(trainingEvent.getJobs().get(jobAlias).getUID());
            jobDetailsPage.checkJobInformation(jobAlias);
            jobDetailsPage.checkJobRelatedAllocations(jobAlias);
        }
    }

    public void checkTrainingEventRelatedJobList(String trainingEventAlias) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        openTrainingEventRelatedJobList(trainingEvent.getUid());
        HashMap<String, Integer> columnIndex = getColumnIndexForSalesforceTable(tblTrainingEventRelatedJobs);
        List<WebElement> rows = tblTrainingEventRelatedJobs.findElements(By.xpath("./tbody/tr"));
        String cellLocator = "./*[self::td or self::th][%s]";

        softAssert = new SoftAssert();
        softAssert.assertEquals(rows.size(), trainingEvent.getJobs().size());

        for (String jobAlias : trainingEvent.getJobs().keySet()) {
            Job job = trainingEvent.getJobs().get(jobAlias);
            String start = convertDateTimeWithTimezone(String.format("%s %s", job.getStartDate(), job.getStartTime()), "dd/MM/yyyy HH:mm",
                    trainingEvent.getTimezone(), userContext.getDateTimeFormat(), userContext.getTimezone());
            String end = formatDateTime(start, job.getDuration(), userContext.getDateTimeFormat(), userContext.getDateTimeFormat());
            WebElement row = driver.findElement(By.xpath(String.format("//*[text()='%s']/ancestor::tr", start)));
            softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Start") + 1))).getText(), start);
            softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("End") + 1))).getText(), end);
            softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Job Status") + 1))).getText(), job.getJobStatus());
            softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Type") + 1))).getText(), job.getJobType());
        }
        softAssert.assertAll();
    }

    public void cancelJobs(String trainingEventAlias, String mode) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        if ("Cancel Jobs".equals(mode)) {
            openTrainingEvent(trainingEvent.getUid());
            click(btnCancelJobs);
            switchToiFrame(driver);
            click(btnYesToCancelJobs);
            waitForSpinnerToDisappear();
        }
        for (String jobAlias : trainingEvent.getJobs().keySet()) {
            Job job = trainingEvent.getJobs().get(jobAlias);
            job.setJobStatus("Cancelled");
            for (String resource : job.getResources().keySet())
                job.getResources().put(resource, "Deleted");
            trainingEvent.getJobs().put(jobAlias, job);
        }
        scenarioContext.setValue(trainingEventAlias, trainingEvent);
        LogHelper.getLogger().info("Cancel jobs for training event successfully");
    }
}
