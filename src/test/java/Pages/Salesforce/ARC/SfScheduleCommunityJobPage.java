package Pages.Salesforce.ARC;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Objects.CommunityJob;
import Objects.Job;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.TimeZone;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.formatDateTime;
import static Utilities.GenericFunctions.generateDate;

public class SfScheduleCommunityJobPage {
    private WebDriver driver;

    public SfScheduleCommunityJobPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "[name='courseCatalog'] input")
    public WebElement txtCourseCatalog;

    @FindBy(how = How.CSS, using = "select[name=\"communityClassSubTypes\"]")
    public WebElement ddlCommunityClassSubType;

    @FindBy(how = How.CSS, using = "[name='neededSkills'] input")
    public WebElement txtAttributesTags;

    @FindBy(how = How.CSS, using = "select[name=\"jobFacility\"]")
    public WebElement ddlFacility;

    @FindBy(how = How.CSS, using = "select[name=\"jobType\"]")
    public WebElement ddljobType;

    @FindBy(how = How.CSS, using = "input[name=\"noEvaluators\"]")
    public WebElement txtNoOfEvaluators;

    @FindBy(how = How.CSS, using = "input[name=\"jobDate\"]")
    public WebElement txtDateOfEvent;

    @FindBy(how = How.CSS, using = "input[name=\"startTime\"]")
    public WebElement txtStartTime;

    @FindBy(how = How.CSS, using = "input[name=\"finishTime\"]")
    public WebElement txtEndTime;

    @FindBy(how = How.CSS, using = "input[name=\"noStudents\"]")
    public WebElement txtNoOfStudents;

    @FindBy(how = How.CSS, using = "input[name=\"noInstructors\"]")
    public WebElement txtNoOfInstructors;

    @FindBy(how = How.CSS, using = "input[name=\"noBreaks\"]")
    public WebElement txtNoOfBreaks;

    @FindBy(how = How.CSS, using = "input[name=\"breakDuration\"]")
    public WebElement txtBreakDuration;

    @FindBy(how = How.CSS, using = "[ng-model=\"$ctrl.job.specialInstructions\"]")
    public WebElement txtSpecialInstructions;

    @FindBy(how = How.CSS, using = "input[name=\"classSetupTime\"]")
    public WebElement txtClassSetupTime;

    @FindBy(how = How.CSS, using = "input[name=\"classPackdownTime\"]")
    public WebElement txtClassPackdownTime;

    @FindBy(how = How.ID, using = "chkRecurring")
    public WebElement chkRecurring;

    @FindBy(how = How.CSS, using = "input[name=\"noClassDays\"]")
    public WebElement txtNoOfClassDays;

    @FindBy(how = How.CSS, using = "input[name=\"txtEvery\"]")
    public WebElement txtRepeatWeeks;

    @FindBy(how = How.ID, using = "chkSkipHolidays")
    public WebElement chkSkipHolidays;

    @FindBy(how = How.ID, using = "chkStackedClass")
    public WebElement chkStackedClass;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.saveJob(ACTION_AFTER_SAVING.ALLOCATE_RESOURCE)\"]")
    public WebElement btnCreateAndAllocate;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.saveJob(ACTION_AFTER_SAVING.CLOSE_MODAL)\"]")
    public WebElement btnCreateAndClose;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.closeModal('cancel')\"")
    public WebElement btnCancel;

    public void scheduleCommunityJob(String communityAlias, String followingAction, DataTable data, String type) {
        CommunityJob communityJob = new CommunityJob(data, type);
        communityJob.insertJobs();
        SfCommunitySchedulingPage communitySchedulingPage = new SfCommunitySchedulingPage(driver);
        if ("Community".equals(communityJob.getJobType()))
            communitySchedulingPage.openCommunitySchedulingPage();
        else if (communityJob.getJobType().contains("Nurse Assistant Testing"))
            communitySchedulingPage.openCEPSchedulingPage();

        // Select region
        if (!"Any".equals(communityJob.getRegion()))
            selectDropdownlistByText(communitySchedulingPage.ddlRegion, communityJob.getRegion());
        else {
            Select selectRegion = new Select(communitySchedulingPage.ddlRegion);
            int size = selectRegion.getOptions().size();
            selectRegion.selectByIndex((int) (Math.random() * (size)) + 1);
            communityJob.setRegion(selectRegion.getFirstSelectedOption().getText());
            communityJob.setTimezone(new SkeduloAPI().getObjectInfo("regions", "Timezone", String.format("Name =='%s'", communityJob.getRegion())));
        }
        waitForSpinnerToDisappear();
        communitySchedulingPage.setCalendar(communityJob.getStartDate());
        if ("Community".equals(communityJob.getJobType()))
            communitySchedulingPage.filter(null, null, communityJob.getCourseCatalog().getName(), null, null);
        communitySchedulingPage.selectSlotFromGrid(communityJob.getLocation(), communityJob.getStartDate());
        inputCommunityJobInfo(communityJob);
        if ("Save & Allocate".equals(followingAction))
            click(btnCreateAndAllocate);
        else if ("Save & Close".equals(followingAction))
            click(btnCreateAndClose);
        else if (("Cancel".equals(followingAction)))
            click(btnCancel);
        waitForSpinnerToDisappear();

        for (Job job : communityJob.getJobs())
            job.updateInfoUsingSkeduloAPI("UID Name");

        scenarioContext.setValue(communityAlias, communityJob);
        LogHelper.getLogger().info("Schedule Community / CEP Job successfully");
    }

    public void inputCommunityJobInfo(CommunityJob communityJob) {
        sendKeys(txtCourseCatalog, communityJob.getCourseCatalog().getName());
        click(driver.findElement(By.xpath(String.format("//*[text()='%s']", communityJob.getCourseCatalog().getName()))));

        if ("Community".equals(communityJob.getJobType()))
            if (!"Any".equals(communityJob.getCommunityClassSubType()))
                selectDropdownlistByText(ddlCommunityClassSubType, communityJob.getCommunityClassSubType());
            else
                communityJob.setCommunityClassSubType(selectDropdownRandomly(ddlCommunityClassSubType));
        else if (communityJob.getJobType().contains("Nurse Assistant Testing")) {
            selectDropdownlistByText(ddljobType, communityJob.getJobType());
            sendKeys(txtNoOfEvaluators, String.valueOf(communityJob.getEvaluators()));
            sendKeys(txtSpecialInstructions, communityJob.getSpecialInstructions());
        }

        sendKeys(txtStartTime, formatDateTime(communityJob.getStartTime(), "HH:mm", "hh:mm a"));
        txtStartTime.sendKeys(Keys.ENTER);

        sendKeys(txtEndTime, formatDateTime(communityJob.getStartTime(), communityJob.getDuration(), "HH:mm", "hh:mm a"));
        txtStartTime.sendKeys(Keys.ENTER);

        if (communityJob.isRecurring()) {
            setCheckbox(chkRecurring, true);
            sendKeys(txtNoOfClassDays, String.valueOf(communityJob.getClassDays()));
            sendKeys(txtRepeatWeeks, String.valueOf(communityJob.getRepeatWeeks()));
            for (String day : communityJob.getWeekDays().split(","))
                setCheckbox(driver.findElement(By.id(String.format("chk%s", day))), true);
        } else
            setCheckbox(chkRecurring, false);
    }

    public void rescheduleJobByEditJob(String communityAlias, String action, DataTable data) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        for (int i = 0; i < data.raw().size(); i++) {
            Job job = communityJob.getJobs().get(Integer.parseInt(data.raw().get(i).get(0)) - 1);
            new SfCommunitySchedulingPage(driver).doActionOnJobBlock(job, "Edit Job");
            for (int j = 1; j < data.raw().get(i).size(); j += 2) {
                String key = data.raw().get(i).get(j);
                String value = data.raw().get(i).get(j + 1);
                switch (key) {
                    case "StartTime":
                        job.setStartDate(generateDate(value.split(" at ")[0].trim(), job.getTimezone()));
                        job.setStartTime(value.split(" at ")[1].trim());
                        if ("Community".equals(job.getJobType()))
                            job.setDescription(String.format("%s, %s %s", communityJob.getCourseCatalog().getName(), formatDateTime(job.getStartTime(), "HH:mm", "hh:mma"),
                                    TimeZone.getTimeZone(job.getTimezone()).getDisplayName(true, TimeZone.SHORT)));
                        sendKeys(txtDateOfEvent, formatDateTime(job.getStartDate(), "dd/MM/yyyy", "MM/dd/yyyy"));
                        txtDateOfEvent.sendKeys(Keys.ESCAPE);
                        sendKeys(txtStartTime, formatDateTime(job.getStartTime(), "HH:mm", "hh:mm a"));
                        txtStartTime.sendKeys(Keys.ENTER);
                        sendKeys(txtEndTime, formatDateTime(job.getStartTime(), job.getDuration(), "HH:mm", "hh:mm a"));
                        txtStartTime.sendKeys(Keys.ENTER);
                        break;
                    case "Location":
                        job.setLocation(value);
                        selectDropdownlistByText(ddlFacility, job.getLocation());
                        break;
                }
            }
        }
        if ("Save & Allocate".equals(action))
            click(btnCreateAndAllocate);
        else if ("Save & Close".equals(action))
            click(btnCreateAndClose);
        else if (("Cancel".equals(action)))
            click(btnCancel);
        waitForSpinnerToDisappear();
        LogHelper.getLogger().info("Reschedule job successfully by Edit Job");
    }

    private void setCheckbox(WebElement checkbox, Boolean value) {
        if ("true".equals(checkbox.getAttribute("checked")) != value)
            click(checkbox.findElement(By.xpath("./following-sibling::label/span")));
    }
}
