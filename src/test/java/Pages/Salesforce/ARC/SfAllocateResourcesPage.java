package Pages.Salesforce.ARC;

import Base.LogHelper;
import Objects.CommunityJob;
import Objects.Job;
import Objects.NATJob;
import Objects.TrainingEvent;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static Base.BaseUIActions.*;
import static Base.BaseUtil.scenarioContext;
import static Utilities.GenericFunctions.formatDateTime;
import static Utilities.GenericFunctions.sleep;

public class SfAllocateResourcesPage {
    private WebDriver driver;

    public SfAllocateResourcesPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "select[name='region']")
    public WebElement ddlRegion;

    @FindBy(how = How.CSS, using = "select[name='orderBy']")
    public WebElement ddlOrderBy;

    @FindBy(how = How.CSS, using = "input[ng-change='$ctrl.applyResourcesFilter()']")
    public WebElement txtResourceFilter;

    @FindBy(how = How.CSS, using = "div[on-drop='$ctrl.assignResource(resource)']")
    public WebElement droppableHeader;

    @FindBy(how = How.ID, using = "chkNotifyResourceOnSave")
    public WebElement chkNotifyResourceOnSave;

    @FindBy(how = How.CSS, using = "button[ng-click='$ctrl.saveAllocations()']")
    public WebElement btnSave;

    @FindBy(how = How.CSS, using = "button[ng-click=\"saveAllocations(this)\"]")
    public WebElement btnSaveAllocation;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.closeModal('cancel')\"]")
    public WebElement btnCancel;

    private String dropJobLocator = "//tr[@on-drop='$ctrl.assignResource(resource, job)' and .//text() ='%s']";
    private String resourceLinkLocator = "//a[text()='%s']";

    public void allocateResources(String trainingEventAlias, String allocationMode, String notificationStatus, DataTable data) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        String resourceStatus = "true".equals(notificationStatus) ? "Dispatched" : "Pending Dispatch";

        if ("Manage Resource".equals(allocationMode)) {
            SfTrainingEventPage trainingEventPage = new SfTrainingEventPage(driver);
            trainingEventPage.openTrainingEvent(trainingEvent.getUid());
            click(trainingEventPage.btnManageResource);
            switchToiFrame(driver);
            waitForSpinnerToDisappear();
        }

        selectDropdownlistByText(ddlRegion, "All Regions");
        selectDropdownlistByText(ddlOrderBy, "Name");

        for (int i = 0; i < data.raw().size(); i++) {
            String resource = data.raw().get(i).get(1);
            String targetJobAlias = data.raw().get(i).get(0);
            sendKeys(txtResourceFilter, resource);
            sleep(1); // Wait for resource filtering
            WebElement resourceElement = driver.findElement(By.xpath(String.format(resourceLinkLocator, resource)));
            if ("All".equals(targetJobAlias)) {
                dragAndDrop(resourceElement, droppableHeader);
                for (String jobAlias : trainingEvent.getJobs().keySet()) {
                    trainingEvent.getJobs().get(jobAlias).setResources(resource, resourceStatus);
                    trainingEvent.getJobs().get(jobAlias).setJobStatus(resourceStatus);
                }
            } else {
                String startDate = formatDateTime(trainingEvent.getJobs().get(targetJobAlias).getStartDate(), "dd/MM/yyyy", "MMM d, yyyy");
                WebElement jobElement = driver.findElement(By.xpath(String.format(dropJobLocator, startDate)));
                dragAndDrop(resourceElement, jobElement);
                trainingEvent.getJobs().get(targetJobAlias).setResources(resource, resourceStatus);
                trainingEvent.getJobs().get(targetJobAlias).setJobStatus(resourceStatus);
            }
        }
        scenarioContext.setValue(trainingEventAlias, trainingEvent);
        setCheckbox(chkNotifyResourceOnSave, "true".equals(notificationStatus));

        click(btnSave);
        if (checkElementExist(btnSaveAllocation))
            click(btnSaveAllocation);
        waitForSpinnerToDisappear();
        LogHelper.getLogger().info("Resources are allocated successfully");
    }

    public void allocateResourcesForCommunityJobs(String communityAlias, String allocationMode, String notificationStatus, DataTable data) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        if ("Allocate Resources".equals(allocationMode)) {
            new SfCommunitySchedulingPage(driver).doActionOnJobBlock(communityJob.getJobs().get(0), allocationMode);
            waitForSpinnerToDisappear();
        }
        allocateResourcesForJobs(communityJob.getJobs(), notificationStatus, data);
        LogHelper.getLogger().info("Resources are allocated successfully for community jobs");
    }

    public void allocateResourcesForNATJobs(String natAlias, String mode, String notificationStatus, DataTable data) {
        NATJob natJob = (NATJob) scenarioContext.getValue(natAlias);
        allocateResourcesForJobs(natJob.getJobs(), notificationStatus, data);
        LogHelper.getLogger().info("Resources are allocated successfully for NAT jobs");
    }

    public void allocateResourcesForJobs(List<Job> jobs, String notificationStatus, DataTable data) {
        String resourceStatus = "true".equals(notificationStatus) ? "Dispatched" : "Pending Dispatch";
        selectDropdownlistByText(ddlRegion, "All Regions");
        selectDropdownlistByText(ddlOrderBy, "Name");
        for (int i = 0; i < data.raw().size(); i++) {
            String resource = data.raw().get(i).get(1);
            String targetJob = data.raw().get(i).get(0);

            sendKeys(txtResourceFilter, resource);
            sleep(1); // Wait for resource filtering
            WebElement resourceElement = driver.findElement(By.xpath(String.format(resourceLinkLocator, resource)));

            if ("All".equals(targetJob)) {
                dragAndDrop(resourceElement, droppableHeader);
                for (Job job : jobs) {
                    job.setResources(resource, resourceStatus);
                    job.setJobStatus(resourceStatus);
                }
            } else {
                Job job = jobs.get(Integer.parseInt(targetJob) - 1);
                String startDate = formatDateTime(job.getStartDate(), "dd/MM/yyyy", "MMM d, yyyy");
                WebElement jobElement = driver.findElement(By.xpath(String.format(dropJobLocator, startDate)));
                dragAndDrop(resourceElement, jobElement);
                job.setResources(resource, resourceStatus);
                job.setJobStatus(resourceStatus);
            }
        }
        setCheckbox(chkNotifyResourceOnSave, "true".equals(notificationStatus));
        click(btnSave);
        if (checkElementExist(btnSaveAllocation))
            click(btnSaveAllocation);
        waitForSpinnerToDisappear();
    }

    public void unassignResources(String trainingEventAlias, String allocationMode, String followingAction, String
            notificationStatus, DataTable data) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        SfTrainingEventPage trainingEventPage = new SfTrainingEventPage(driver);
        if ("Manage Resource".equals(allocationMode)) {
            trainingEventPage.openTrainingEvent(trainingEvent.getUid());
            click(trainingEventPage.btnManageResource);
            switchToiFrame(driver);
            waitForSpinnerToDisappear();
        }
        for (int i = 0; i < data.raw().size(); i++) {
            String[] resources = data.raw().get(i).get(1).split(",");
            String targetJobAlias = data.raw().get(i).get(0);
            String jobDate = formatDateTime(trainingEvent.getJobs().get(targetJobAlias).getStartDate(), "dd/MM/yyyy", "MMM d, yyyy");
            for (String resource : resources) {
                resource = resource.trim();
                setResourceForJob(jobDate, resource, "Unassign", trainingEvent.getCourseCatalog().getNoOfInstructors());
                trainingEvent.getJobs().get(targetJobAlias).setResources(resource, "Deleted");
                if (trainingEvent.getJobs().get(targetJobAlias).getResources().size() == 0 || trainingEvent.getJobs().get(targetJobAlias).isAllResourcesDeleted())
                    trainingEvent.getJobs().get(targetJobAlias).setJobStatus("Pending Allocation");
            }
        }
        scenarioContext.setValue(trainingEventAlias, trainingEvent);
        if ("Save".equals(followingAction)) {
            setCheckbox(chkNotifyResourceOnSave, "true".equals(notificationStatus));
            click(btnSave);
            click(btnSaveAllocation, 3);
            waitForSpinnerToDisappear();
        }
        LogHelper.getLogger().info("Unassign resources successfully");
    }

    public void unassignResourcesForCommunityJob(String communityAlias, String allocationMode, String action, String notificationStatus, DataTable data) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        SfCommunitySchedulingPage communitySchedulingPage = new SfCommunitySchedulingPage(driver);
        if ("Allocate Resources".equals(allocationMode)) {
            communitySchedulingPage.doActionOnJobBlock(communityJob.getJobs().get(0), allocationMode);
            waitForSpinnerToDisappear();
        }
        for (int i = 0; i < data.raw().size(); i++) {
            String[] resources = data.raw().get(i).get(1).split(",");
            Job job = communityJob.getJobs().get(Integer.parseInt(data.raw().get(i).get(0)) - 1);
            String jobDate = formatDateTime(job.getStartDate(), "dd/MM/yyyy", "MMM d, yyyy");
            for (String resource : resources) {
                resource = resource.trim();
                setResourceForJob(jobDate, resource, "Unassign", communityJob.getCourseCatalog().getNoOfInstructors());
                job.setResources(resource, "Deleted");
                if (job.getResources().size() == 0 || job.isAllResourcesDeleted())
                    job.setJobStatus("Pending Allocation");
            }
        }
        if ("Save".equals(action)) {
            setCheckbox(chkNotifyResourceOnSave, "true".equals(notificationStatus));
            click(btnSave);
            click(btnSaveAllocation, 3);
            waitForSpinnerToDisappear();
        }
        LogHelper.getLogger().info("Unassign resources for community jobs successfully");
    }

    private void setResourceForJob(String jobDate, String resource, String clickOption, int resourceSlots) {
        String resourceSlotLocator = "//tr[.//text()='%s']/td/ul[@class='arc-allocated-resource__container']/li[%s]";
        for (int i = 1; i <= resourceSlots; i++) {
            String xpath = String.format(resourceSlotLocator, jobDate, i);
            String current = ((JavascriptExecutor) driver).executeScript(String.format("var xpath = \"%s\";" +
                    "var ele = document.evaluate(xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;" +
                    "var scope = angular.element(ele).scope();" +
                    "return scope.resource.name;", xpath)).toString();
            if (resource.equals(current)) {
                click(driver.findElement(By.xpath(xpath)));
                sleep(1);

                switch (clickOption) {
                    case "TeamLeader":
                        click(driver.findElement(By.cssSelector("a[ng-click*='$ctrl.setResourceAsTeamLeader(resource, job)']")));
                        break;
                    case "Unassign":
                        click(driver.findElement(By.cssSelector("a[ng-click*='$ctrl.unassignResource(resource, job)']")));
                        break;
                    case "UnassignAll":
                        click(driver.findElement(By.cssSelector("a[ng-click*='$ctrl.unassignResource(resource)']")));
                        break;
                }
                return;
            }
        }
    }
}
