package Pages.Salesforce.ARC;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Objects.CourseCatalog;
import Objects.Job;
import Objects.TrainingEvent;
import Pages.PageManager;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.asserts.SoftAssert;

import static Base.BaseUIActions.*;
import static Base.BaseUtil.scenarioContext;
import static Utilities.GenericFunctions.*;

public class SfScheduleFullServicePage {
    private WebDriver driver;

    public SfScheduleFullServicePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "select[ng-model='$ctrl.job.region']")
    public WebElement ddlRegion;

    @FindBy(how = How.CSS, using = "div[name='courseCatalog'] input")
    public WebElement txtCourseCatalog;

    @FindBy(how = How.CSS, using = "div[name='lkAddress'] input")
    public WebElement txtAddress;

    @FindBy(how = How.CSS, using = "div[name='noStudents'] input")
    public WebElement txtNoOfStudents;

    @FindBy(how = How.CSS, using = "div[name='noInstructors'] input")
    public WebElement txtNoOfInstructors;

    @FindBy(how = How.CSS, using = "div[name='classDuration'] input")
    public WebElement txtClassDuration;

    @FindBy(how = How.CSS, using = "div[name='noBreaks'] input")
    public WebElement txtNoOfBreaks;

    @FindBy(how = How.CSS, using = "div[name='breakDuration'] input")
    public WebElement txtBreakDuration;

    @FindBy(how = How.CSS, using = "div[name='classSetupTime'] input")
    public WebElement txtClassSetupTime;

    @FindBy(how = How.CSS, using = "div[name='classPackdownTime'] input")
    public WebElement txtClassPackdownTime;

    @FindBy(how = How.CSS, using = "button[ng-click='$ctrl.saveJob(ACTION_AFTER_SAVING.ALLOCATE_RESOURCE)']")
    public WebElement btnSaveAndAllocate;

    @FindBy(how = How.CSS, using = "button[ng-click='$ctrl.saveJob(ACTION_AFTER_SAVING.CLOSE_MODAL)']")
    public WebElement btnSaveAndClose;

    @FindBy(how = How.CSS, using = "button[ng-click='$ctrl.closeModal('cancel')']")
    public WebElement btnCancel;

    @FindBy(how = How.CSS, using = "div[label='Employment Type']")
    public WebElement ddlEmploymentType;

    @FindBy(how = How.CSS, using = "div[label='Scheduled Hours']")
    public WebElement ddlScheduledHours;

    @FindBy(how = How.ID, using = "chkBookSubcontrator")
    public WebElement chkBookSubcontrator;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Scheduled Jobs')]/ancestor::li[@ng-click='$ctrl.selectTab(tab)']")
    public WebElement tabScheduledJobs;

    @FindBy(how = How.XPATH, using = "//*[contains(text(),'Selected Slots')]/ancestor::li[@ng-click='$ctrl.selectTab(tab)']")
    public WebElement tabSelectedSlots;

    @FindBy(how = How.CSS, using = "button[ng-click='$ctrl.acceptReschedulingJob()']")
    public WebElement btnDoneAcceptReschedulingJob;

    @FindBy(how = How.ID, using = "chkMultiDays")
    public WebElement chkMultiDays;

    @FindBy(how = How.CSS, using = "input[name='noClassDays']")
    public WebElement txtNoOfClassDays;

    public void scheduleFullService(String trainingEventAlias, DataTable data) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        trainingEvent.setValues(data);

        SfTrainingEventPage trainingEventPage = new SfTrainingEventPage(driver);
        trainingEventPage.openTrainingEvent(trainingEvent.getUid());

        click(trainingEventPage.btnScheduleFullService);
        switchToiFrame(driver);
        waitForSpinnerToDisappear();

        // Select Region
        if (!"Any".equals(trainingEvent.getRegion()))
            selectDropdownlistByText(ddlRegion, trainingEvent.getRegion());
        else {
            Select selectRegion = new Select(ddlRegion);
            int size = selectRegion.getOptions().size();
            selectRegion.selectByIndex((int) (Math.random() * (size)) + 1);
            trainingEvent.setRegion(selectRegion.getFirstSelectedOption().getText());
            trainingEvent.setTimezone(new SkeduloAPI().getObjectInfo("regions", "Timezone", String.format("Name =='%s'", trainingEvent.getRegion())));
        }

        // Select course catalog
        sendKeys(txtCourseCatalog, trainingEvent.getCourseCatalog().getName());
        click(driver.findElement(By.xpath(String.format("//*[text()='%s']", trainingEvent.getCourseCatalog().getName()))));

        // Select Address
        sendKeys(txtAddress, trainingEvent.getAddress());
        click(driver.findElement(By.xpath(String.format("//*[text()='%s']", trainingEvent.getAddress()))));
        waitForSpinnerToDisappear();

        // Store timezone on Grid for checking later
        trainingEvent.setTimezoneShortFormat(driver.findElement(By.cssSelector("td.arc-col__header-time")).getText());
        scenarioContext.setValue(trainingEventAlias, trainingEvent);
        LogHelper.getLogger().info("Open Full Schedule Service successfully");
    }

    public void checkCourseCatalogInfo(String courseCatalogAlias) {
        CourseCatalog courseCatalog = (CourseCatalog) scenarioContext.getValue(courseCatalogAlias);
        softAssert = new SoftAssert();
        softAssert.assertEquals(txtNoOfStudents.getAttribute("value"), String.valueOf(courseCatalog.getNoOfStudents()));
        softAssert.assertEquals(txtNoOfInstructors.getAttribute("value"), String.valueOf(courseCatalog.getNoOfInstructors()));
        softAssert.assertEquals(txtClassDuration.getAttribute("value"), String.valueOf(courseCatalog.getClassDuration()));
        softAssert.assertEquals(txtNoOfBreaks.getAttribute("value"), String.valueOf(courseCatalog.getNoOfBreaks()));
        softAssert.assertEquals(txtBreakDuration.getAttribute("value"), String.valueOf(courseCatalog.getBreakDuration()));
        softAssert.assertEquals(txtClassSetupTime.getAttribute("value"), "30");
        softAssert.assertEquals(txtClassPackdownTime.getAttribute("value"), "30");
        softAssert.assertAll();
    }

    public void selectJobSlots(String trainingEventAlias, String multiDays, String button, DataTable data) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        trainingEvent.insertJobs(data);
        setCheckbox(chkBookSubcontrator, true);
        if (!"default".equals(multiDays)) {
            click(tabSelectedSlots);
            setCheckbox(chkMultiDays, true);
            sendKeys(txtNoOfClassDays, String.valueOf(data.raw().size()));
        }
        for (String jobAlias : trainingEvent.getJobs().keySet()) {
            Job job = trainingEvent.getJobs().get(jobAlias);
            clickJobSlot(job);
        }
        if ("Save & Allocate".equals(button))
            click(btnSaveAndAllocate);
        else if ("Save & Close".equals(button))
            click(btnSaveAndClose);
        else if ("Cancel".equals(button))
            click(btnCancel);

        waitForSpinnerToDisappear();

        for (String jobAlias : trainingEvent.getJobs().keySet()) {
            Job job = trainingEvent.getJobs().get(jobAlias);
            String dateInDB = convertDateTimeWithTimezone(String.format("%s %s", job.getStartDate(), job.getStartTime()), "dd/MM/yyyy HH:mm",
                    job.getTimezone(), "yyyy-MM-dd'T'HH:mm:SS.sss'Z'", "UTC");
            job.setUID(new SkeduloAPI().getJobID(String.format("Description == '%s' AND Start ==%s", job.getDescription(), dateInDB)));
            storePropertyToFile("Jobs", job.getUID(), needToDeletePropertyFile);
            job.setJobName(new SkeduloAPI().getJobInfo("Name", String.format("Description == '%s' AND Start ==%s", job.getDescription(), dateInDB)));
            trainingEvent.getJobs().put(jobAlias, job);
        }
        scenarioContext.setValue(trainingEventAlias, trainingEvent);
        LogHelper.getLogger().info("Full Service is scheduled successfully");
    }

    public void rescheduleJobs(String trainingEventAlias, String button, DataTable data) {
        TrainingEvent trainingEvent = (TrainingEvent) scenarioContext.getValue(trainingEventAlias);
        click(new PageManager(driver).getSfTrainingEventPage().btnScheduleFullService);
        switchToiFrame(driver);
        waitForSpinnerToDisappear();
        click(tabScheduledJobs);
        for (int i = 0; i < data.raw().size(); i++) {
            String jobAlias = data.raw().get(i).get(0);
            String dateData = data.raw().get(i).get(1);
            trainingEvent.updateJobs(jobAlias, dateData);
            Job job = trainingEvent.getJobs().get(jobAlias);
            click(driver.findElement(By.xpath(String.format("//a[text()='%s']/ancestor::li[@ng-repeat=\"job in $ctrl.scheduledJobs\"]//button[@ng-click=\"$ctrl.rescheduleJob(job)\"]", job.getJobName()))));
            clickJobSlot(job);
            click(btnDoneAcceptReschedulingJob);
        }
        if (button.equals("Save & Allocate"))
            click(btnSaveAndAllocate);
        else if (button.equals("Save & Close"))
            click(btnSaveAndClose);
        else
            click(btnCancel);
        waitForSpinnerToDisappear();
        LogHelper.getLogger().info("Reschedule jobs successfully");
    }

    private void clickJobSlot(Job job) {
        String startDate = formatDateTime(job.getStartDate(), "dd/MM/yyyy", "MM/dd/yyyy");
        String startTime = formatDateTime(job.getStartTime(), "HH:mm", "h:mm a");
        String currentGridStartDate = driver.findElement(By.cssSelector("input[name='calStartDate']")).getAttribute("value");
        int dateDiff = (int) calculateDateTimeBetween(currentGridStartDate, startDate, "MM/dd/yyyy", "Days");
        WebElement slot = driver.findElement(By.xpath(String.format("//span[text()='%s']/ancestor::tr/td[%s]", startTime, dateDiff + 2)));
        if (slot.findElement(By.xpath(".//input")).getAttribute("checked") == null)
            click(slot);
    }

    private void selectAllListValues(WebElement element) {
        click(element);
        sleep(1);
        click(driver.findElement(By.cssSelector("li[ng-click='toggleSelectAll()']")));
    }
}
