package Pages.Salesforce.ARC;

import Base.LogHelper;
import Objects.Job;
import Objects.NATJob;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.*;

public class SfScheduleNATJobsPage {
    private WebDriver driver;

    public SfScheduleNATJobsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    // ----------- Manage Course Details ------------

    @FindBy(how = How.CSS, using = "div[name='courseCatalog'] input")
    public WebElement txtCourseCatalog;

    @FindBy(how = How.CSS, using = "input[name=\"txtCourseName\"]")
    public WebElement txtCourseName;

    @FindBy(how = How.CSS, using = "select[name=\"jobType\"]")
    public WebElement ddlJobType;

    @FindBy(how = How.CSS, using = "input[name=\"startMonth\"]")
    public WebElement txtStartMonth;

    @FindBy(how = How.CSS, using = "textarea[ng-model=\"$ctrl.natCourse.specialInstructions\"]")
    public WebElement txtSpecialInstructions;

    @FindBy(how = How.CSS, using = "select[name=\"region\"]")
    public WebElement ddlRegion;

    @FindBy(how = How.CSS, using = "input[name=\"noOfEnrolledStudents\"]")
    public WebElement txtMaximumStudents;

    @FindBy(how = How.CSS, using = "input[name=\"maxHoursPerDay\"]")
    public WebElement txtMaxHoursPerDay;

    @FindBy(how = How.CSS, using = "div[name=\"neededSkills\"] input")
    public WebElement txtAttributes;

    //------------ Course Schedule Calendar ------------------------
    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.manageSchedulePattern()\"]")
    public WebElement btnSchedulePattern;

    @FindBy(how = How.CSS, using = "input[name=\"startDate\"]")
    public WebElement txtStartDate;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.applyPattern()\"]")
    public WebElement btnApplyPattern;

    // ---------------- Classes Confirmation --------------------

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.saveCourse(ACTION_AFTER_SAVING.ALLOCATE_RESOURCE)\"]")
    public WebElement btnGenerateCourseAndAllocate;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.saveCourse(ACTION_AFTER_SAVING.CLOSE_MODAL)\"]")
    public WebElement btnGenerateCourseAndClose;

    //------------ Same Elements ------------------------

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.closeModal('cancel')\"]")
    public WebElement btnCancel;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.goNextStep()\"]")
    public WebElement btnNext;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.goPreviousStep()\"]")
    public WebElement btnBack;

    public void scheduleNATJob(String natAlias, String action, DataTable data) {
        NATJob natJob = new NATJob(data);
        natJob.insertJobs();
        SfCommunitySchedulingPage communitySchedulingPage = new SfCommunitySchedulingPage(driver);
        communitySchedulingPage.openNATSchedulingPage();
        selectDropdownlistByText(communitySchedulingPage.ddlRegion, natJob.getRegion());
        waitForSpinnerToDisappear();
        click(communitySchedulingPage.btnCreateNewNAT);
        waitForSpinnerToDisappear();

        inputManageCourseDetails(natJob);
        inputManageClassType(natJob);
        inputCourseScheduleCalendar(natJob);

        switch (action) {
            case "Generate Course & Allocate":
                click(btnGenerateCourseAndAllocate);
                waitForSpinnerToDisappear();
                break;
            case "Generate Course & Close":
                click(btnGenerateCourseAndClose);
                waitForSpinnerToDisappear();
                break;
            case "Back":
                click(btnBack);
                break;
            case "Cancel":
                click(btnCancel);
                break;
        }
        storeObjectUIDsToFile("courses", String.format("Name LIKE '%%%s%%'", natJob.getCourseCatalog().getName()), needToDeletePropertyFile);
        for (Job job : natJob.getJobs())
            job.updateInfoUsingSkeduloAPI("UID Name");

        scenarioContext.setValue(natAlias, natJob);
        LogHelper.getLogger().info("Schedule NAT Job successfully");
    }

    public void inputManageCourseDetails(NATJob natJob) {
        sendKeys(txtCourseCatalog, natJob.getCourseCatalog().getName());
        click(driver.findElement(By.xpath(String.format("//*[text()='%s']", natJob.getCourseCatalog().getName()))));
        selectDropdownlistByText(ddlJobType, natJob.getType());
        sendKeys(txtSpecialInstructions, natJob.getSpecialInstructions());
        sendKeys(txtMaximumStudents, String.valueOf(natJob.getMaxStudents()));
        click(btnNext);
    }

    public void inputManageClassType(NATJob natJob) {
        List<WebElement> locationElements = driver.findElements(By.cssSelector("select[ng-model=\"classType.facility\"]"));
        for (WebElement locationElement : locationElements) {
            selectDropdownlistByText(locationElement, natJob.getLocation());
        }
        sendKeys(txtMaxHoursPerDay, String.valueOf(natJob.getMaxHoursPerDay()));
        click(btnNext);
    }

    public void inputCourseScheduleCalendar(NATJob natJob) {
        waitForSpinnerToDisappear();
        click(btnSchedulePattern);
        for (String day : natJob.getDaysOfWeek().split(",")) {
            setCheckbox(driver.findElement(By.id(String.format("chk%s", day))), true);
        }
        sendKeys(txtStartDate, formatDateTime(natJob.getStartDate(), "dd/MM/yyyy", "MM/dd/yyyy"));
        txtStartDate.sendKeys(Keys.ESCAPE);
        click(btnApplyPattern);
        waitForSpinnerToDisappear();
        click(btnNext);
    }

    private void setCheckbox(WebElement checkbox, Boolean value) {
        if ("true".equals(checkbox.getAttribute("checked")) != value)
            click(checkbox.findElement(By.xpath("./following-sibling::label/span")));
    }
}
