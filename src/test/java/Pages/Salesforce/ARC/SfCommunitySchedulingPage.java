package Pages.Salesforce.ARC;

import Base.LogHelper;
import Objects.CommunityJob;
import Objects.Job;
import Objects.NATJob;
import Pages.Salesforce.SfJobDetailsPage;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;

import static Base.BaseUIActions.*;
import static Base.BaseUtil.userContext;
import static Utilities.GenericFunctions.*;

public class SfCommunitySchedulingPage {
    private WebDriver driver;

    public SfCommunitySchedulingPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.ID, using = "cboRegion")
    public WebElement ddlRegion;

    @FindBy(how = How.CSS, using = ".viewing-day-title__heading")
    public WebElement txtCurrentMonthHeading;

    @FindBy(how = How.CSS, using = ".viewing-day-title button[ng-click='$ctrl.moveBack()']")
    public WebElement btnMoveCalendarBack;

    @FindBy(how = How.CSS, using = ".viewing-day-title button[ng-click='$ctrl.moveNext()']")
    public WebElement btnMoveCalendarNext;

    @FindBy(how = How.CSS, using = "div[model=\"$ctrl.filters.states\"]")
    public WebElement ddlFilterState;

    @FindBy(how = How.CSS, using = "div[model=\"$ctrl.filters.locations\"]")
    public WebElement ddlFilterLocation;

    @FindBy(how = How.CSS, using = "div[model=\"$ctrl.filters.courses\"]")
    public WebElement ddlFilterCourse;

    @FindBy(how = How.CSS, using = "div[model=\"$ctrl.filters.jobStatuses\"]")
    public WebElement ddlFilterJobStatus;

    @FindBy(how = How.CSS, using = "div[model=\"$ctrl.filters.jobAllocations\"]")
    public WebElement ddlFilterJobAllocation;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.applyFilters()\"]")
    public WebElement btnApplyFilters;

    @FindBy(how = How.CSS, using = "button[ng-click=\"$ctrl.createNatCourse()\"]")
    public WebElement btnCreateNewNAT;

    @FindBy(how = How.CSS, using = "table.slds-table")
    public WebElement table;

    public void openCommunitySchedulingPage() {
        driver.navigate().to(String.format("%s/lightning/n/Community_Scheduling", userContext.getLoginResource()));
        switchToiFrame(driver);
    }

    public void openCEPSchedulingPage() {
        driver.navigate().to(String.format("%s/lightning/n/CEP_Scheduling", userContext.getLoginResource()));
        switchToiFrame(driver);
    }

    public void openNATSchedulingPage() {
        driver.navigate().to(String.format("%s/lightning/n/NAT_Scheduling", userContext.getLoginResource()));
        switchToiFrame(driver);
    }

    public void openCourseCatalogRelatedJobs(String courseCatalogUID) {
        driver.navigate().to(String.format("%s/lightning/r/%s/related/Jobs__r/view", userContext.getLoginResource(), courseCatalogUID));
        driver.switchTo().defaultContent();
    }

    public void selectSlotFromGrid(String location, String date) {
        List<WebElement> locations = driver.findElements(By.cssSelector("[row-height-sync=\".arc-csc-data-content-table\"] > li"));
        for (int i = 0; i < locations.size(); i++) {
            if (location.equals(locations.get(i).getText())) {
                String cellLocator = String.format(".arc-csc-data-content-table tr:nth-child(%s) > td:nth-child(%s)", i + 1, formatDateTime(date, "dd/MM/yyyy", "d"));
                click(driver.findElement(By.cssSelector(cellLocator)));
                sleep(1);
                By dropdown = By.cssSelector(".slds-dropdown__list a[ng-click=\"$ctrl.createNewJob(dataItem.facility, day);$dropdownMenu.hideDropdownMenu()\"]");
                if (checkElementExist(dropdown))
                    click(driver.findElement(dropdown));
                break;
            }
        }
        waitForSpinnerToDisappear();
    }

    public void filter(String states, String locations, String courses, String jobStatuses, String jobAllocations) {
        if (states != null)
            selectDropdownWithMultiOptions(ddlFilterState, states);
        if (locations != null)
            selectDropdownWithMultiOptions(ddlFilterLocation, locations);
        if (courses != null)
            selectDropdownWithMultiOptions(ddlFilterCourse, courses);
        if (jobStatuses != null)
            selectDropdownWithMultiOptions(ddlFilterJobStatus, jobStatuses);
        if (jobAllocations != null)
            selectDropdownWithMultiOptions(ddlFilterJobAllocation, jobAllocations);
        click(btnApplyFilters);
        waitForSpinnerToDisappear();
    }

    public void setCalendar(String date) {
        date = formatDateTime(date, "dd/MM/yyyy", "MMMM, yyyy");
        Long daysDiff = calculateDateTimeBetween(txtCurrentMonthHeading.getText(), date, "MMMM, yyyy", "Days");
        while (daysDiff != 0) {
            click((daysDiff > 0) ? btnMoveCalendarNext : btnMoveCalendarBack);
            daysDiff = calculateDateTimeBetween(txtCurrentMonthHeading.getText(), date, "MMMM, yyyy", "Days");
        }
    }

    public void checkJobsInfoForCommunityJobs(String communityAlias) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);

        for (Job job : communityJob.getJobs())
            if ("Deleted".equals(job.getJobStatus()))
                checkJobNotInGridCell(job);
            else if ("Community".equals(job.getJobType()))
                checkCommunityJobBlockPopup(job, communityJob);
            else if (job.getJobType().contains("Nurse Assistant Testing"))
                checkCEPJobBlockPopup(job, communityJob);

        checkCourseCatalogRelatedJobList(communityAlias);

        for (Job job : communityJob.getJobs()) {
            SfJobDetailsPage jobDetailsPage = new SfJobDetailsPage(driver);
            jobDetailsPage.openJobDetails(job.getUID());
            jobDetailsPage.checkJobInformation(job);
            jobDetailsPage.checkJobRelatedAllocations(job);
        }
    }

    public void checkJobsInfoForNATJobs(String natAlias) {
        NATJob natJob = (NATJob) scenarioContext.getValue(natAlias);
        for (Job job : natJob.getJobs())
            checkNATJobBlockPopup(job);

        checkCourseCatalogRelatedJobList(natAlias);

        for (Job job : natJob.getJobs()) {
            SfJobDetailsPage jobDetailsPage = new SfJobDetailsPage(driver);
            jobDetailsPage.openJobDetails(job.getUID());
            jobDetailsPage.checkJobInformation(job);
            jobDetailsPage.checkJobRelatedAllocations(job);
        }
    }

    public void checkCommunityJobBlockPopup(Job job, CommunityJob communityJob) {
        doActionOnJobBlock(job, "Popover");
        String locator = "//div[contains(@class,'acr-csc-job-info-popover')]//dt[text()='%s']/following-sibling::dd";
        softAssert = new SoftAssert();
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Job Number"))).getText(), job.getJobName());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Course Name"))).getText(), communityJob.getCourseCatalog().getName());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Class Sub Type"))).getText(), communityJob.getCommunityClassSubType());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Status"))).getText(), job.getJobStatus());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "No. of Instructors"))).getText(), String.valueOf(communityJob.getCourseCatalog().getNoOfInstructors()));
        if (job.getResources().size() > 0 && !job.isAllResourcesDeleted()) {
            String resources = driver.findElement(By.xpath(String.format(locator, "Allocated Resources"))).getText();
            StringBuilder temp = new StringBuilder();
            for (String resource : job.getResources().keySet()) {
                String status = job.getResources().get(resource);
                if (!"Deleted".equals(status))
                    temp.append(resource).append("\n").append(status).append("\n");
            }
            String expectedResources = temp.substring(0, temp.length() - 1);
            softAssert.assertEquals(resources, expectedResources);
        } else
            softAssert.assertFalse(checkElementExist(By.xpath(String.format(locator, "Allocated Resources"))));
        softAssert.assertAll();
    }

    public void checkCEPJobBlockPopup(Job job, CommunityJob communityJob) {
        doActionOnJobBlock(job, "Popover");
        String locator = "//div[contains(@class,'acr-cep-job-info-popover')]//dt[text()='%s']/following-sibling::dd";
        softAssert = new SoftAssert();
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Job Number"))).getText(), job.getJobName());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Class Name"))).getText(), job.getDescription());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Status"))).getText(), job.getJobStatus());
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Start Time"))).getText(), formatDateTime(job.getStartTime(), "HH:mm", "h:mm a"));
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "End Time"))).getText(), formatDateTime(job.getStartTime(), job.getDuration(), "HH:mm", "h:mm a"));
        softAssert.assertEquals(driver.findElement(By.xpath(String.format(locator, "Special Instructions"))).getText(), job.getSpecialInstructions());
        if (job.getResources().size() > 0 && !job.isAllResourcesDeleted()) {
            String resources = driver.findElement(By.xpath(String.format(locator, "Allocated Resources"))).getText();
            StringBuilder temp = new StringBuilder();
            for (String resource : job.getResources().keySet()) {
                String status = job.getResources().get(resource);
                if (!"Deleted".equals(status))
                    temp.append(resource).append("\n").append(status).append("\n");
            }
            String expectedResources = temp.substring(0, temp.length() - 1);
            softAssert.assertEquals(resources, expectedResources);
        } else
            softAssert.assertFalse(checkElementExist(By.xpath(String.format(locator, "Allocated Resources"))));
        softAssert.assertAll();
    }

    public void checkNATJobBlockPopup(Job job) {

        softAssert = new SoftAssert();
        softAssert.assertAll();

    }

    public void doActionOnJobBlock(Job job, String targetAction) {
        setCalendar(job.getStartDate());
        selectDropdownlistByText(ddlRegion, job.getRegion());
        waitForSpinnerToDisappear();
        WebElement jobBlock = findJobBlock(job);
        switch (targetAction) {
            case "Popover":
                Actions action = new Actions(driver);
                action.moveToElement(jobBlock).build().perform();
                break;
            case "Allocate Resources":
            case "Edit Job":
            case "Delete Job":
            case "Cancel Job":
                String option = targetAction.replaceAll(" ", "");
                option = option.substring(0, 1).toLowerCase() + option.substring(1);
                String buttonLocator = String.format(".slds-dropdown__list a[ng-click*='$ctrl.%s']", option);
                click(jobBlock);
                click(driver.findElement(By.cssSelector(buttonLocator)));
                waitForSpinnerToDisappear();
                break;
            case "View in Skedulo":
                click(jobBlock);
                click(driver.findElement(By.cssSelector(".slds-dropdown__list a[ng-click=\"$dropdownMenu.hideDropdownMenu()\"]")));
                break;
        }
    }

    public void checkNoJobInGridCell(Job job) {
        softAssert = new SoftAssert();
        setCalendar(job.getStartDate());
        selectDropdownlistByText(ddlRegion, job.getRegion());
        waitForSpinnerToDisappear();
        List<WebElement> locations = driver.findElements(By.cssSelector("[row-height-sync=\".arc-csc-data-content-table\"] > li"));
        for (int i = 0; i < locations.size(); i++) {
            if (job.getLocation().equals(locations.get(i).getText())) {
                String cellLocator = String.format(".arc-csc-data-content-table tr:nth-child(%s) > td:nth-child(%s) > div.job-block", i + 1, formatDateTime(job.getStartDate(), "dd/MM/yyyy", "d"));
                softAssert.assertFalse(driver.findElements(By.cssSelector(cellLocator)).size() > 0);
                break;
            }
        }
        softAssert.assertAll();
    }

    public void checkJobNotInGridCell(Job job) {
        setCalendar(job.getStartDate());
        selectDropdownlistByText(ddlRegion, job.getRegion());
        waitForSpinnerToDisappear();
        softAssert = new SoftAssert();
        softAssert.assertNull(findJobBlock(job));
        softAssert.assertAll();
    }

    public void rescheduleJobByDragAndDrop(String communityAlias, DataTable data) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        for (int i = 0; i < data.raw().size(); i++) {
            Job job = communityJob.getJobs().get(Integer.parseInt(data.raw().get(i).get(0)) - 1);
            String newEventTime = data.raw().get(i).get(1);
            String newStartDate = generateDate(newEventTime.split(" at ")[0].trim(), job.getTimezone());
            String newLocation = data.raw().get(i).get(2);
            // Drag and drop
            HashMap<String, Integer> locationMap = new HashMap<>();
            List<WebElement> locations = driver.findElements(By.cssSelector("[row-height-sync=\".arc-csc-data-content-table\"] > li"));
            for (int j = 0; j < locations.size(); j++) locationMap.put(locations.get(j).getText(), j);
            String oldCellLocator = String.format(".arc-csc-data-content-table tr:nth-child(%s) > td:nth-child(%s) > div.job-block", locationMap.get(job.getLocation()) + 1,
                    formatDateTime(job.getStartDate(), "dd/MM/yyyy", "d"));
            String newCellLocator = String.format(".arc-csc-data-content-table tr:nth-child(%s) > td:nth-child(%s)", locationMap.get(newLocation) + 1,
                    formatDateTime(newStartDate, "dd/MM/yyyy", "d"));
            WebElement from = driver.findElement(By.cssSelector(oldCellLocator));
            WebElement to = driver.findElement(By.cssSelector(newCellLocator));
            Actions actions = new Actions(driver);
            actions.clickAndHold(from).moveToElement(to).click(to).build().perform();
            if (checkElementExist(By.cssSelector("button[ng-click=\"closeModal('confirmed')\"]")))
                click(driver.findElement(By.cssSelector("button[ng-click=\"closeModal('confirmed')\"]")));
            // Update job values
            job.setStartDate(newStartDate);
            job.setLocation(newLocation);
        }
        LogHelper.getLogger().info("Reschedule job successfully by drag and drop");
    }

    public void cancelJobs(String communityAlias, String jobs) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        for (String jobIndex : jobs.split(",")) {
            Job job = communityJob.getJobs().get(Integer.parseInt(jobIndex) - 1);
            doActionOnJobBlock(job, "Cancel Job");
            click(driver.findElement(By.cssSelector("button[ng-click=\"confirm()\"]")));
            waitForSpinnerToDisappear();
            job.setJobStatus("Cancelled");
            for (String resource : job.getResources().keySet())
                job.setResources(resource, "Deleted");
        }
        LogHelper.getLogger().info("Cancel jobs for community successfully");
    }

    public void deleteJobs(String communityAlias, String jobs) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        for (String jobIndex : jobs.split(",")) {
            Job job = communityJob.getJobs().get(Integer.parseInt(jobIndex) - 1);
            doActionOnJobBlock(job, "Delete Job");
            click(driver.findElement(By.cssSelector("button[ng-click=\"confirm()\"]")));
            waitForSpinnerToDisappear();
            job.setJobStatus("Deleted");
            removeLineFromFile(String.format("Jobs:%s", job.getUID()), needToDeletePropertyFile);
        }
        LogHelper.getLogger().info("Delete jobs for community successfully");
    }

    private void selectDropdownWithMultiOptions(WebElement dropdown, String options) {
        click(dropdown);
        sleep(1);
        List<WebElement> optionElements = driver.findElements(By.cssSelector(".slds-picklist.slds-dropdown__list li"));
        for (WebElement element : optionElements) {
            String optionName = element.getText();
            Boolean isSelected = element.getAttribute("class").contains("slds-is-selected");
            if (options.contains(optionName) != isSelected)
                click(element);
            if (options.contains("Any"))
                break;
        }
    }

    public void checkCourseCatalogRelatedJobList(String communityAlias) {
        CommunityJob communityJob = (CommunityJob) scenarioContext.getValue(communityAlias);
        openCourseCatalogRelatedJobs(communityJob.getCourseCatalog().getUid());

        HashMap<String, Integer> columnIndex = getColumnIndexForSalesforceTable(table);
        List<WebElement> rows = table.findElements(By.xpath("./tbody/tr"));
        String cellLocator = "./*[self::td or self::th][%s]";

        softAssert = new SoftAssert();
        softAssert.assertEquals(rows.size(), communityJob.countActiveJobs());

        for (Job job : communityJob.getJobs()) {
            if (!"Deleted".equals(job.getJobStatus())) {
                String start = convertDateTimeWithTimezone(String.format("%s %s", job.getStartDate(), job.getStartTime()), "dd/MM/yyyy HH:mm",
                        communityJob.getTimezone(), userContext.getDateTimeFormat(), userContext.getTimezone());
                String end = formatDateTime(start, job.getDuration(), userContext.getDateTimeFormat(), userContext.getDateTimeFormat());
                WebElement row = driver.findElement(By.xpath(String.format("//*[text()='%s']/ancestor::tr", start)));
                softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Job Name") + 1))).getText(), job.getJobName());
                softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Start") + 1))).getText(), start);
                softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("End") + 1))).getText(), end);
                softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Job Status") + 1))).getText(), job.getJobStatus());
            }
        }
        softAssert.assertAll();
    }

    private WebElement findJobBlock(Job job) {
        WebElement result = null;
        List<WebElement> locations = driver.findElements(By.cssSelector("[row-height-sync=\".arc-csc-data-content-table\"] > li"));
        for (int i = 0; i < locations.size(); i++) {
            if (job.getLocation().equals(locations.get(i).getText())) {
                String jobBlockLocators = String.format(".arc-csc-data-content-table tr:nth-child(%s) > td:nth-child(%s) > div.job-block", i + 1, formatDateTime(job.getStartDate(), "dd/MM/yyyy", "d"));
                List<WebElement> jobBlockElements = driver.findElements(By.cssSelector(jobBlockLocators));
                for (int j = 0; j < jobBlockElements.size(); j++) {
                    String cssSelector = String.format("%s:nth-child(%s)", jobBlockLocators, j + 1);
                    String current = ((JavascriptExecutor) driver).executeScript(String.format("" +
                            "var ele = document.querySelector('%s');" +
                            "var scope = angular.element(ele).scope();" +
                            "return scope.job.id;", cssSelector)).toString();
                    if (job.getUID().equals(current)) {
                        result = jobBlockElements.get(j);
                        break;
                    }
                }
                break;
            }
        }
        return result;
    }
}
