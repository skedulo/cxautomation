package Pages.Salesforce;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Objects.GroupEvent;
import Objects.Job;
import Pages.PageManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import javax.swing.*;

import static Base.BaseUIActions.*;

public class SfGroupEventPage {

    private WebDriver driver;
    PageManager pageManager;

    public SfGroupEventPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        pageManager = new PageManager(driver);
    }

    @FindBy(how = How.CSS, using = "a.forceActionLink > div[title='New']")
    public WebElement btnNewGroupEvent;

    public void openGroupEventPage() {
        driver.navigate().to(String.format("%s/lightning/o/skedhealthcare__Group_Event__c/home",
                userContext.getLoginResource()));
        waitForPageToLoad();
    }

    public void openGroupEventDetails(String groupAlias) {
        GroupEvent group = (GroupEvent) scenarioContext.getValue(groupAlias);
        pageManager.getSfGroupEventPage().openGroupEventPage();
        driver.switchTo().defaultContent();
        waitForSpinnerToDisappear();
        WebElement txtSearchGroupEvent = driver.findElement(By.xpath("//input[@name='skedhealthcare__Group_Event__c-search-input']")); //Have to use xpath because cssSelector function is override by Salesforce
        sendKeys(txtSearchGroupEvent, group.getGroupName());
        txtSearchGroupEvent.sendKeys(Keys.ENTER);
        waitForSpinnerToDisappear();
        click(driver.findElement(By.cssSelector(String.format("a[title='%s']", group.getGroupName()))));
        LogHelper.getLogger().info(String.format("%s group is opened successfully", groupAlias));
    }

    public void openJob(String jobAlias, String groupAlias) {
        Job job = (Job) scenarioContext.getValue(jobAlias);
        job.setJobName(new SkeduloAPI().getJobInfo("Name", String.format("Description=='%s'", job.getDescription())));

        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));

        click(driver.findElement(By.xpath(String.format("//td[@scope='row' and ./div[@title='%s']]/ancestor::tr[contains(@ng-repeat,'job in $ctrl.groupEvent.jobs')]//button[@ng-click='$ctrl.editJob(job)']", job.getJobName()))));

        waitForSpinnerToDisappear();

        LogHelper.getLogger().info("Job is opened inside group event");
    }
}
