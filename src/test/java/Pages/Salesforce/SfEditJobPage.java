package Pages.Salesforce;

import Base.LogHelper;
import Objects.Job;
import Pages.PageManager;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.text.ParseException;
import java.util.Iterator;

import static Base.BaseUIActions.*;

public class SfEditJobPage {
    private WebDriver driver;

    public SfEditJobPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "input[name='startDate']")
    public WebElement txtStartDate;

    @FindBy(how = How.CSS, using = "input[name='startTime']")
    public WebElement txtStartTime;

    @FindBy(how = How.CSS, using = "input[name='jobDuration']")
    public WebElement txtJobDuration;

    @FindBy(how = How.CSS, using = "select[name='jobUrgency']")
    public WebElement ddlJobUrgency;

    @FindBy(how = How.CSS, using = "textarea[name='description']")
    public WebElement txtDescription;

    @FindBy(how = How.CSS, using = "div[name='region'] > div > div > input")
    public WebElement txtRegion;

    @FindBy(how = How.CSS, using = "select[name^='serviceDeliveryMethod']")
    public WebElement ddlServiceDeliveryMethod;

    @FindBy(how = How.CSS, using = "select[name^='serviceCategory']")
    public WebElement ddlServiceCategory;

    @FindBy(how = How.CSS, using = "select[name^='serviceAgreementItem']")
    public WebElement ddlServiceAgreementItem;

    @FindBy(how = How.XPATH, using = "//*[text()='Update Job']")
    public WebElement btnUpdateJob;

    @FindBy(how = How.XPATH, using = "//*[text()='Save']")
    public WebElement btnSaveOnConfictCheck;

    @FindBy(how = How.XPATH, using = "//*[text()='Go to Job Details Page']")
    public WebElement btnGotoJobDetailsPage;

    public void editJob(String jobAlias, String params) throws ParseException {
        Job job = (Job) scenarioContext.getValue(jobAlias);
        job.updateValues(params);
        scenarioContext.setValue(jobAlias, job);

        LogHelper.getLogger().info("Open Job through Salesforce");
        driver.navigate().to(String.format("%s/lightning/r/sked__Job__c/%s/view",
                userContext.getLoginResource(),
                job.getUID()));

        click(new PageManager(driver).getSfJobDetailsPage().btnEditJob);
        WebElement iframe = driver.findElement(By.xpath("//iframe"));
        driver.switchTo().frame(iframe);
        waitForElement(btnUpdateJob);

        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "StartDate":
                    sendKeys(txtStartDate, job.getStartDate());
                    txtStartDate.sendKeys(Keys.ESCAPE);
                    break;

                case "StartTime":
                    sendKeys(txtStartTime, job.getStartTime());
                    txtStartTime.sendKeys(Keys.ESCAPE);
                    break;

                case "Duration":
                    sendKeys(txtJobDuration, String.valueOf(job.getDuration()));
                    break;

                case "Urgency":
                    Select selectUrgency = new Select(ddlJobUrgency);
                    selectUrgency.selectByVisibleText(job.getUrgency());
                    break;

                case "Description":
                    sendKeys(txtDescription, job.getDescription());
                    break;

                case "Region":
                    // Todo: Support change Region
                    break;
            }
        }

        click(btnUpdateJob);
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//*[text()='Save']")))
            click(btnSaveOnConfictCheck);
        LogHelper.getLogger().info("Job is updated successfully");
    }
}
