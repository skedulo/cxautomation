package Pages.Salesforce;

import Base.LogHelper;
import Objects.Shift;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.text.ParseException;
import java.util.List;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.formatDateTime;

public class SfCreateAvailabilityPage {
    private WebDriver driver;

    public SfCreateAvailabilityPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.ID, using = "select-region")
    public WebElement ddlRegion;

    @FindBy(how = How.ID, using = "search-provider")
    public WebElement txtSearch;

    @FindBy(how = How.CSS, using = "input[ng-model='state.selectedDate']")
    public WebElement txtCalendar;

    @FindBy(how = How.ID, using = "cbbAvailabilityType")
    public WebElement ddlAvailabilityType;

    @FindBy(how = How.ID, using = "cbbActivityType")
    public WebElement ddlActivityType;

    @FindBy(how = How.ID, using = "cbbStartTime")
    public WebElement ddlStartTime;

    @FindBy(how = How.ID, using = "cbbEndTime")
    public WebElement ddlEndTime;

    @FindBy(how = How.XPATH, using = "//input[@id='chkRecurring']/following-sibling::label/span")
    public WebElement chkRecurring;

    @FindBy(how = How.XPATH, using = "//input[@id='chkSkipWeekends']/following-sibling::label/span")
    public WebElement chkSkipWeekends;

    @FindBy(how = How.XPATH, using = "//input[@id='chkSkipHolidays']/following-sibling::label/span")
    public WebElement chkSkipHolidays;

    @FindBy(how = How.ID, using = "chkRecurring")
    public WebElement inputRecurring;

    @FindBy(how = How.ID, using = "chkSkipWeekends")
    public WebElement inputSkipWeekends;

    @FindBy(how = How.ID, using = "chkSkipHolidays")
    public WebElement inputSkipHolidays;

    @FindBy(how = How.ID, using = "cbbPattern")
    public WebElement ddlPattern;

    @FindBy(how = How.ID, using = "cbbRecur")
    public WebElement ddlRecurFor;

    @FindBy(how = How.ID, using = "activity-address")
    public WebElement txtActivityAddress;

    @FindBy(how = How.CSS, using = "button[ng-click='handleSaveShift()']")
    public WebElement btnSaveShift;

    public void createAvailability(String type, String availableAlias, String resource, DataTable data) throws ParseException {
        // Create availability object
        Shift availability = new Shift(type, data);
        scenarioContext.setValue(availableAlias, availability);

        // Go to Availability Management page
        driver.navigate().to(String.format("%s/lightning/n/skedhealthcare__Availability_Management", userContext.getLoginResource()));
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        waitForSpinnerToDisappear();

        // Todo: Handle other region
        // Select Brisbane region
        click(ddlRegion);
        click(driver.findElement(By.cssSelector("[title='Brisbane']"))); // Todo: Handle to not click if checked
        click(ddlRegion);
        waitForSpinnerToDisappear();

        // Search for the resource
        sendKeys(txtSearch, resource);
        txtSearch.sendKeys(Keys.ENTER);

        // Enter Start Date
        sendKeys(txtCalendar, formatDateTime(availability.getStartDate(), availability.getStartTime(), 0, userContext.getDateTimeFormat().split(" ")[0]));
        txtCalendar.sendKeys(Keys.ENTER);
        txtCalendar.sendKeys(Keys.ESCAPE);
        waitForSpinnerToDisappear();

        // Click on Plus button at Start Date
        WebElement btnPlus = driver.findElement(By.xpath(String.format(
                "//div[@id='resource-availability-header']/following-sibling::table/tbody/tr[.//text()='%s']/td[%s]//div[@role='button' and contains(@ng-click,'handleAddAvailability')]",
                resource, getDateColumnIndex(availability.getStartDate()))));
        click(btnPlus);

        // Input availability information
        inputAvailabilityInfo(availability);

        LogHelper.getLogger().info("Availability is created successfully");
    }

    private void inputAvailabilityInfo(Shift availability) throws ParseException {
        boolean isActivity = "activity".equals(availability.getType().toLowerCase());

        // Click Availability
        click(driver.findElement(By.cssSelector(String.format("a[ng-click='state.creationModel.isActivity = %s']", isActivity))));

        // Select Availability Type
        WebElement type = (isActivity) ? ddlActivityType : ddlAvailabilityType;
        selectDropdownlistByValue(type, availability.getSubType());

        // Enter Start Time
        String start = String.valueOf(formatDateTime(availability.getStartDate(), availability.getStartTime(), 0, "Hmm"));
        selectDropdownlistByValue(ddlStartTime, start);

        // Enter End Time
        String end = String.valueOf(formatDateTime(availability.getStartDate(), availability.getEndTime(), 0, "Hmm"));
        selectDropdownlistByValue(ddlEndTime, end);

        if (isActivity) {
            sendKeys(txtActivityAddress, availability.getAddress());
            click(driver.findElement(By.xpath("//*[text()='Address']")));
        }

        if (availability.isRecurring()) {
            // Set Recurring
            setCheckbox(inputRecurring, chkRecurring, availability.isRecurring());

            // Set Skip Weekend
            setCheckbox(inputSkipWeekends, chkSkipWeekends, availability.isSkippedWeekend());

            // Set Skip Holiday
            setCheckbox(inputSkipHolidays, chkSkipHolidays, availability.isSkippedHolidays());

            // Select Pattern
            selectDropdownlistByText(ddlPattern, availability.getPattern());

            // Select days of week
            setDaysPattern(availability.getDaysOfWeek());

            // Select Recur For
            selectDropdownlistByText(ddlRecurFor, availability.getRecurFor());
        }

        // Click Save button
        click(btnSaveShift);
        waitForSpinnerToDisappear();
    }

    private int getDateColumnIndex(String date) {
        List<WebElement> headers = driver.findElements(By.xpath("//div[@id='resource-availability-header']/following-sibling::div/table//th/div"));

        for (int i = 0; i < headers.size(); i++) {
            if (headers.get(i).getText().contains(date)) {
                return i + 2;
            }
        }
        return 0;
    }

    private void setCheckbox(WebElement stateElement, WebElement clickElement, boolean value) {
        boolean checked = "true".equals(stateElement.getAttribute("checked"));
        if (checked != value)
            click(clickElement);
    }

    private void setDaysPattern(String daysPattern) {
        String[] weeks = daysPattern.split(";");
        for (int i = 0; i < weeks.length; i++) {
            String[] days = weeks[i].split(",");
            for (String day : days) {
                WebElement input = driver.findElement(By.id(String.format("week%s%s", i + 1, day.toLowerCase())));
                WebElement check = input.findElement(By.xpath("./following-sibling::label/span"));
                setCheckbox(input, check, true);
            }
        }
    }
}
