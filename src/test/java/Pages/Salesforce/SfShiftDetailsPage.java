package Pages.Salesforce;

import Base.LogHelper;
import Objects.Shift;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.text.ParseException;

import static Base.BaseUtil.*;
import static Utilities.GenericFunctions.*;

public class SfShiftDetailsPage {
    private WebDriver driver;

    public SfShiftDetailsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Start']/parent::*/following-sibling::*//*[self::lightning-formatted-text or @class='uiOutputDateTime']")
    public WebElement txtStart;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and contains(text(),'End')]/parent::*/following-sibling::*//*[self::lightning-formatted-text or @class='uiOutputDateTime']")
    public WebElement txtEnd;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Duration']/parent::*/following-sibling::*//*[self::lightning-formatted-number or @class='uiOutputNumber']")
    public WebElement txtDuration;


    public void goToShiftDetailsPage(String shiftAlias) {
        Shift shift = (Shift) scenarioContext.getValue(shiftAlias);
        driver.navigate().to(String.format("%s/lightning/r/sked__Shift__c/%s/view", userContext.getLoginResource(), shift.getShiftId()));
        LogHelper.getLogger().info(String.format("Open %s successfully", shiftAlias));
    }

    public void checkShiftDetails(String shiftAlias) throws ParseException {
        Shift shift = (Shift) scenarioContext.getValue(shiftAlias);
        String expectedStart = formatDateTime(String.format("%s %s", shift.getStartDate(), shift.getStartTime()), "dd/MM/yyyy HH:mm", "d/MM/yyyy h:mm a");
        String expectedEnd = formatDateTime(String.format("%s %s", shift.getEndDate(), shift.getEndTime()), "dd/MM/yyyy HH:mm", "d/MM/yyyy h:mm a");
        Long expectedDuration = calculateDateTimeBetween(expectedStart, expectedEnd, "d/MM/yyyy h:mm a", "Minutes");
        driver.switchTo().defaultContent();
        softAssert = new SoftAssert();
        softAssert.assertEquals(txtStart.getText(), expectedStart);
        softAssert.assertEquals(txtEnd.getText(), expectedEnd);
        softAssert.assertEquals(txtDuration.getText(), String.valueOf(expectedDuration));
        softAssert.assertAll();
    }
}
