package Pages.Salesforce;

import Base.LogHelper;
import Objects.GroupEvent;
import Objects.Job;
import Pages.PageManager;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

import static Base.BaseUIActions.*;

public class SfNewGroupEventPage {

    private WebDriver driver;
    private PageManager pageManager;

    public SfNewGroupEventPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        pageManager = new PageManager(driver);
    }

    @FindBy(how = How.CSS, using = "input[name='groupName']")
    public WebElement txtGroupName;

    @FindBy(how = How.CSS, using = "textarea[name='description']")
    public WebElement txtDescription;

    @FindBy(how = How.CSS, using = "select[name='status']")
    public WebElement ddlStatus;

    @FindBy(how = How.CSS, using = "div[name='region'] > div > div > input")
    public WebElement txtRegion;

    @FindBy(how = How.CSS, using = "select[name='addressType']")
    public WebElement ddlAddressType;

    @FindBy(how = How.CSS, using = "div[name='address'] > div > div > input")
    public WebElement txtAddress;

    @FindBy(how = How.CSS, using = "select[name='billableType']")
    public WebElement ddlBillable;

    @FindBy(how = How.CSS, using = "input[name^='noOfAttendees']")
    public WebElement txtNoOfAttendees;

    @FindBy(how = How.CSS, using = "input[name^='noOfResources']")
    public WebElement txtNoOfResources;

    @FindBy(how = How.CSS, using = "select[name='asset']")
    public WebElement ddlAsset;

    @FindBy(how = How.XPATH, using = "//button[contains(@ng-click,'saveGroupEvent') and text()='Save']")
    public WebElement btnSave;

    @FindBy(how = How.XPATH, using = "//button[contains(@ng-click,'ctrl.addClient')]")
    public WebElement btnAddClient;

    public void createGroupEvent(String eventAlias, String params) throws InterruptedException {
        // Open Group Event Home page
        pageManager.getSfGroupEventPage().openGroupEventPage();

        // Click New button
        click(pageManager.getSfGroupEventPage().btnNewGroupEvent);
        waitForSpinnerToDisappear();
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        waitForSpinnerToDisappear();

        // Input information
        inputInformationForNewGroup(eventAlias, params);
    }

    public void inputInformationForNewGroup(String eventAlias, String params) throws InterruptedException {
        GroupEvent groupEvent = new GroupEvent(params);
        scenarioContext.setValue(eventAlias, groupEvent);

        // Input information
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "GroupName":
                    sendKeys(txtGroupName, groupEvent.getGroupName());
                    break;

                case "Description":
                    sendKeys(txtDescription, groupEvent.getDescription());
                    break;

                case "Status":
                    selectDropdownlistByText(ddlStatus, groupEvent.getStatus());
                    break;

                case "Region":
                    sendKeys(txtRegion, groupEvent.getRegion());
                    Thread.sleep(3000);
                    WebElement region = driver.findElement(By.cssSelector("div[title='" + groupEvent.getRegion() + "']"));
                    click(region);
                    break;

                case "AddressType":
                    selectDropdownlistByText(ddlAddressType, groupEvent.getAddressType());
                    break;

                case "Address":
                    sendKeys(txtAddress, groupEvent.getAddress());
                    Thread.sleep(3000);
                    click(driver.findElement(By.cssSelector("div[title='" + groupEvent.getAddress() + "']")));
                    waitForSpinnerToDisappear();
                    break;

                case "Billable":
                    selectDropdownlistByText(ddlBillable, groupEvent.getBillable());
                    break;

                case "MaximumAttendees":
                    sendKeys(txtNoOfAttendees, String.valueOf(groupEvent.getMaximumAttendees()));
                    break;

                case "MaximumResources":
                    sendKeys(txtNoOfResources, String.valueOf(groupEvent.getMaximumResources()));
                    break;

                case "Asset":
                    selectDropdownlistByText(ddlAsset, String.valueOf(groupEvent.getAsset()));
                    break;
            }
        }

        // Add Client
        addClientIntoNewGroupEvent();

        // Click Save button
        click(btnSave);
        LogHelper.getLogger().info("New group is created successfully");
    }

    public void createJobUnderGroup(String jobAlias, String groupAlias, String params) throws ParseException, IOException {
        LogHelper.getLogger().info(String.format("Job %s is created successfully under group %s", jobAlias, groupAlias));
        Job job = new Job(params);
        GroupEvent group = (GroupEvent) scenarioContext.getValue(groupAlias);
        job.setAddressType(group.getAddressType());
        job.setAddress(group.getAddress());
        job.setBillType(group.getBillable());
        job.setJobType("Group Event");
        job.setRegion(group.getRegion());
        scenarioContext.setValue(jobAlias, job);

        //Todo: Open group event

        // Click Schedule Job
        click(driver.findElement(By.xpath("//button[contains(@ng-click,'addJob')]")));
        waitForSpinnerToDisappear();

        // Input information
        pageManager.getSfGroupEventScheduleJobPage().scheduleJob(job);
    }

    private void addClientIntoNewGroupEvent() throws InterruptedException {
        //Todo: Handle default Contact
        String contactName = "Jenny John Dow";

        click(btnAddClient);

        WebElement searchBox = driver.findElement(By.cssSelector("div[name='contact'] input"));
        sendKeys(searchBox, contactName);
        Thread.sleep(3000);
        WebElement name = driver.findElement(By.cssSelector(String.format("div[title='%s']", contactName)));
        click(name);
        waitForSpinnerToDisappear();

        selectDropdownlistByText(driver.findElement(By.cssSelector("select[name='serviceDeliveryMethod']")), "Scheduled Time");

        selectDropdownlistByText(driver.findElement(By.cssSelector("select[name='serviceCategory']")), "Uncategorised");

        selectDropdownlistByIndex(driver.findElement(By.cssSelector("select[name='serviceAgreementItem']")), 1);

        click(driver.findElement(By.xpath("//button[contains(@ng-click,'saveClient') and text()='OK']")));
        waitForSpinnerToDisappear();
    }

    public void checkGroupEventDetails(String groupAlias) {
        waitForSpinnerToDisappear();
        GroupEvent group = (GroupEvent) scenarioContext.getValue(groupAlias);
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        softAssert = new SoftAssert();
        softAssert.assertEquals(txtGroupName.getAttribute("value"), group.getGroupName());
        softAssert.assertEquals(txtDescription.getAttribute("value"), group.getDescription());
        softAssert.assertEquals(new Select(ddlStatus).getFirstSelectedOption().getText(), group.getStatus());
        softAssert.assertEquals(driver.findElement(By.cssSelector("div[name='region'] span.slds-pill__label")).getText(), group.getRegion());
        softAssert.assertEquals(driver.findElement(By.cssSelector("div[name='address'] span.slds-pill__label")).getText(), group.getAddress());
        softAssert.assertEquals(new Select(ddlAddressType).getFirstSelectedOption().getText(), group.getAddressType());
        softAssert.assertEquals(new Select(ddlBillable).getFirstSelectedOption().getText(), group.getBillable());
        softAssert.assertEquals(txtNoOfAttendees.getAttribute("value"), String.valueOf(group.getMaximumAttendees()));
        softAssert.assertEquals(txtNoOfResources.getAttribute("value"), String.valueOf(group.getMaximumResources()));
        softAssert.assertEquals(new Select(ddlAsset).getFirstSelectedOption().getText(), String.valueOf(group.getAsset()));
        softAssert.assertAll();
    }
}
