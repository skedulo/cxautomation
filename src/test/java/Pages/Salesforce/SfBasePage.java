package Pages.Salesforce;

import Base.LogHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.Arrays;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.sleep;

public class SfBasePage {

    private WebDriver driver;

    public SfBasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.XPATH, using = "//span[text()='Setup']/parent::div[contains(@class,'headerTrigger')]")
    public WebElement btnSetup;

    @FindBy(how = How.XPATH, using = "//span[text()='View profile']/parent::div")
    public WebElement btnViewProfile;

    @FindBy(how = How.CLASS_NAME, using = "profile-card-name")
    public WebElement txtProfileCardName;

    @FindBy(how = How.CLASS_NAME, using = "profile-card-domain")
    public WebElement txtProfileCardDomain;

    @FindBy(how = How.CSS, using = "a.profile-link-label.logout.uiOutputURL")
    public WebElement lnkLogout;

    @FindBy(how = How.ID, using = "related_setup_app_home")
    public WebElement btnSetupAppHome;

    @FindBy(how = How.CSS, using = "input[lightning-basecombobox_basecombobox]")
    public WebElement txtSearchType;

    @FindBy(how = How.CSS, using = "input.uiInputTextForAutocomplete")
    public WebElement txtSearchBox;

    public void goToSetupPage() {
        click(btnSetup);
        click(btnSetupAppHome);
    }

    public void openProfilePanel() {
        int count = 2;
        waitForPageToLoad();

        while (!checkElementExist(By.className("profile-card-name")) && count > 0) {
            click(btnViewProfile);
            try {
                waitForElement(txtProfileCardName, 3);
            }
            catch (TimeoutException e) {
                LogHelper.getLogger().info(Arrays.toString(e.getStackTrace()));
                count--;
                sleep(1);
            }
        }
    }

    public String getProfileCardName() {
        openProfilePanel();
        return txtProfileCardName.getText();
    }

    public String getProfileCardDomain() {
        openProfilePanel();
        return txtProfileCardDomain.getText();
    }

    public boolean isContextUserLoggedIn() {
        return userContext.getName().equals(getProfileCardName()) && userContext.getDomain().equals(getProfileCardDomain());
    }

    public void logout() {
        openProfilePanel();
        click(lnkLogout);
        hasUserLoggedIn = false;
    }

    public void openHomePage() {
        driver.navigate().to(userContext.getLoginResource());
    }

    public void openContact(String contactName) throws InterruptedException {
        waitForPageToLoad();
        selectSearchOption(txtSearchType, "Contacts");
        LogHelper.getLogger().info("Select search type as Contacts");
        sendKeys(txtSearchBox, contactName);
        LogHelper.getLogger().info("Send key as " + contactName + " in Search textbox");
        Thread.sleep(3000); //Todo: find a wait to implicit wait element instead of explicit wait
        WebElement contactElement = driver.findElement(By.xpath("//span[@title='" + contactName + "']/parent::*/parent::*"));
        waitForElement(contactElement);
        click(contactElement);


    }

    public void selectSearchOption(WebElement element, String optionName) {
        try {
            waitForElement(element);
            click(element);
            click(driver.findElement(By.xpath("//span[@title='Contacts']")));
        } catch (Exception e) {
            LogHelper.getLogger().error("Got issues when select option " + optionName + "from element " + element.getText());
        }
    }
}
