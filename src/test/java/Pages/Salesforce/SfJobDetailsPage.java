package Pages.Salesforce;

import Objects.Job;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;

import static Base.BaseUIActions.checkElementExist;
import static Base.BaseUIActions.getColumnIndexForSalesforceTable;
import static Base.BaseUtil.*;
import static Utilities.GenericFunctions.convertDateTimeWithTimezone;
import static Utilities.GenericFunctions.formatDateTime;

public class SfJobDetailsPage {
    private WebDriver driver;

    public SfJobDetailsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "button.skedhealthcareSkedLightningSingleBookingEditPopupModal")
    public WebElement btnEditJob;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Description']/parent::*/following-sibling::*//*[@data-output-element-id='output-field' or @class='uiOutputTextArea']")
    public WebElement txtDescription;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Start']/parent::*/following-sibling::*//*[@data-output-element-id='output-field' or @class='uiOutputDateTime']")
    public WebElement txtStart;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Finish']/parent::*/following-sibling::*//*[@data-output-element-id='output-field' or @class='uiOutputDateTime']")
    public WebElement txtFinish;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Duration']/parent::*/following-sibling::*//*[@data-output-element-id='output-field' or @class='uiOutputNumber']")
    public WebElement txtDuration;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Urgency']/parent::*/following-sibling::*//*[@data-output-element-id='output-field' or @data-aura-rendered-by]")
    public WebElement txtUrgency;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Region']/parent::*/following-sibling::*//a")
    public WebElement lnkRegion;

    @FindBy(how = How.XPATH, using = "//*[@class='test-id__field-label' and text()='Type']/parent::*/following-sibling::*//*[@data-output-element-id='output-field' or @data-aura-rendered-by]")
    public WebElement txtJobType;

    @FindBy(how = How.CSS, using = "table.slds-table")
    public WebElement table;

    public void checkJobInformation(String jobAlias) {
        Job job = (Job) scenarioContext.getValue(jobAlias);
        checkJobInformation(job);
    }

    public void checkJobInformation(Job job) {
        driver.switchTo().defaultContent();
        softAssert = new SoftAssert();
        if (!"Deleted".equals(job.getJobStatus())) {
            String expectedStart = convertDateTimeWithTimezone(String.format("%s %s", job.getStartDate(), job.getStartTime()), "dd/MM/yyyy HH:mm",
                    job.getTimezone(), userContext.getDateTimeFormat(), userContext.getTimezone());
            String expectedFinish = formatDateTime(expectedStart, job.getDuration(), userContext.getDateTimeFormat(), userContext.getDateTimeFormat());
            softAssert.assertEquals(txtDescription.getText(), job.getDescription());
            softAssert.assertEquals(txtDuration.getText(), String.valueOf(job.getDuration()));
            softAssert.assertEquals(txtUrgency.getText(), (job.getUrgency() != null ? job.getUrgency() : ""));
            softAssert.assertEquals(lnkRegion.getText(), job.getRegion());
            softAssert.assertEquals(txtStart.getText(), expectedStart);
            softAssert.assertEquals(txtFinish.getText(), expectedFinish);
            softAssert.assertEquals(txtJobType.getText(), job.getJobType());
        } else {
            String errorText = "Looks like there's a problem.\n" +
                    "We couldn't find the record you're trying to access. It may have been deleted by another user, or there may have been a system error. Ask your administrator for help.";
            softAssert.assertEquals(driver.findElement(By.cssSelector("div.bBody")).getText(), errorText);
        }
        softAssert.assertAll();
    }

    public void openJobDetails(String uid) {
        driver.navigate().to(String.format("%s/lightning/r/sked__Job__c/%s/view", userContext.getLoginResource(), uid));
    }

    public void openJobRelatedAllocations(String uid) {
        driver.navigate().to(String.format("%s/lightning/r/%s/related/sked__Job_Allocations__r/view", userContext.getLoginResource(), uid));
    }

    public void checkJobRelatedAllocations(String jobAlias) {
        Job job = (Job) scenarioContext.getValue(jobAlias);
        checkJobRelatedAllocations(job);
    }

    public void checkJobRelatedAllocations(Job job) {
        openJobRelatedAllocations(job.getUID());
        HashMap<String, Integer> columnIndex = getColumnIndexForSalesforceTable(table);
        List<WebElement> rows = table.findElements(By.xpath("./tbody/tr"));
        String cellLocator = "./*[self::td or self::th][%s]";
        softAssert = new SoftAssert();
        softAssert.assertEquals(rows.size(), job.getResources().size());
        for (String resource : job.getResources().keySet()) {
            WebElement row = driver.findElement(By.xpath(String.format("//*[text()='%s']/ancestor::tr", resource)));
            softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Resource Name") + 1))).getText(), resource);
            softAssert.assertEquals(row.findElement(By.xpath(String.format(cellLocator, columnIndex.get("Status") + 1))).getText(), job.getResources().get(resource));
        }
        softAssert.assertAll();
    }
}
