package Pages.Salesforce;

import Objects.User.SkedPackages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.*;

public class SfInstalledPackages {

    private WebDriver driver;

    public SfInstalledPackages(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public boolean IsPackageInstalled(SkedPackages packages) {
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@title,'Installed Packages')]")));
        WebElement table = driver.findElement(By.xpath("//div[@class='listRelatedObject setupBlock']/descendant::table[2]"));

        // Get column indexes
        ArrayList<String> columnToBeIndexed = new ArrayList<>(
                Arrays.asList("Publisher", "Version Number"));
        Dictionary indexDic = new Hashtable();
        List<WebElement> columns = table.findElements(By.xpath("//th"));
        for (int i = 0; i < columns.size(); i++) {
            if (columnToBeIndexed.contains(columns.get(i).getText())) {
                indexDic.put(columns.get(i).getText(), i - 1);
            }
        }

        // Check value for each row
        List<WebElement> rows = table.findElements(By.xpath("//tr[contains(@class,'dataRow')]"));
        for (WebElement row : rows) {
            String packName = "";
            String publisher = "";
            String version = "";

            if (!"Uninstall".equals(row.findElement(By.xpath("child::td[1]")).getText()))
                continue;

            packName = row.findElements(By.xpath("child::th")).get(0).getText();
            publisher = row.findElements(By.xpath("child::td")).get((Integer) indexDic.get("Publisher")).getText();
            version = row.findElements(By.xpath("child::td")).get((Integer) indexDic.get("Version Number")).getText();

            if (packName.equals(packages.getSkedPackageName())
                    && publisher.equals(packages.getPublisher())
                    && version.equals(packages.getVersion().toString())) {
                return true;
            }
        }
        return false;
    }
}
