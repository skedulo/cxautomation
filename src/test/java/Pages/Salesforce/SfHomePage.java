package Pages.Salesforce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class SfHomePage {

    private WebDriver driver;

    public SfHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public boolean IsAtPage() {
        return driver.findElement(By.xpath("//span[@title='Skedulo App' and contains(text(),'Skedulo App')]")).isDisplayed();
    }
}
