package Pages.Salesforce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import static Base.BaseUIActions.*;
import static Base.BaseUtil.userContext;

public class SfLoginPage {

    WebDriver driver;

    public SfLoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.ID, using = "username")
    public WebElement txtUsername;

    @FindBy(how = How.ID, using = "password")
    public WebElement txtPassword;

    @FindBy(how = How.ID, using = "Login")
    public WebElement btnLogin;

    @FindBy(how = How.LINK_TEXT, using = "Forgot Your Password?")
    public WebElement lnkForgotYourPassword;

    public void open() {
        driver.navigate().to(userContext.getLoginResource());
    }

    public void login(String username, String password) {
        sendKeys(txtUsername, username);
        sendKeys(txtPassword, password);
        click(btnLogin);
        waitForPageToLoad();
        if (checkElementExist(By.cssSelector("a.switch-to-lightning")))
            click(driver.findElement(By.cssSelector("a.switch-to-lightning")));
    }

    public boolean isAtLoginPage() {
        return checkElementExist(lnkForgotYourPassword);
    }
}
