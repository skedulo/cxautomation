package Pages.Salesforce;

import APIHelper.SkeduloAPI;
import Objects.Shift;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static Base.BaseUIActions.click;
import static Base.BaseUIActions.waitForSpinnerToDisappear;
import static Base.BaseUtil.*;

public class SfResourceAvailabilitiesPage {
    private WebDriver driver;
    private Map<String, Integer> columnIndex = new HashMap<>();

    public SfResourceAvailabilitiesPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "table.slds-table")
    public WebElement tblAvailabilities;


    public void checkAvailabilities(String availabilityAlias, String resource) {
        Shift availability = (Shift) scenarioContext.getValue(availabilityAlias);
        List<Map<String, String>> eventList = availability.generateShiftList();

        // Get ResourceId by Skedulo API
        String resourceId = new SkeduloAPI().getResourceInfo("UID", String.format("Name =='%s'", resource));

        // Open resource Availabilities / Activities
        if ("availability".equals(availability.getType()))
            driver.navigate().to(String.format("%s/lightning/r/%s/related/sked__Availabilities1__r/view", userContext.getLoginResource(), resourceId));
        else
            driver.navigate().to(String.format("%s/lightning/r/%s/related/sked__Activities__r/view", userContext.getLoginResource(), resourceId));

        driver.switchTo().defaultContent();

        // Check availability list
        List<WebElement> headers = tblAvailabilities.findElements(By.xpath("./thead/tr/th"));
        for (int i = 0; i < headers.size(); i++) {
            String columnName = headers.get(i).getAttribute("aria-label");
            if (columnName != null)
                columnIndex.put(columnName.replace("Finish", "End"), i);
        }

        // Sort Start column by Ascending
        sortTableBy("Start", "ascending");

        // Check Availabilities List
        List<WebElement> rows = tblAvailabilities.findElements(By.xpath("./tbody/tr"));

        softAssert = new SoftAssert();
        softAssert.assertEquals(rows.size(), eventList.size());

        for (int i = 0; i < eventList.size(); i++) {
            String expectedType = availability.getSubType();
            String actualType = rows.get(i).findElements(By.xpath("./*[self::td or self::th]")).get(columnIndex.get("Type")).getText();
            softAssert.assertEquals(actualType, expectedType);

            String expectedStart = eventList.get(i).get("Start");
            String actualStart = rows.get(i).findElements(By.xpath("./*[self::td or self::th]")).get(columnIndex.get("Start")).getText();
            softAssert.assertEquals(actualStart, expectedStart);

            String expectedFinish = eventList.get(i).get("End");
            String actualFinish = rows.get(i).findElements(By.xpath("./*[self::td or self::th]")).get(columnIndex.get("End")).getText();
            softAssert.assertEquals(actualFinish, expectedFinish);

            if ("activity".equals(availability.getType())) {
                String expectedAddress = availability.getAddress();
                String actualAddress = rows.get(i).findElements(By.xpath("./*[self::td or self::th]")).get(columnIndex.get("Address")).getText();
                softAssert.assertEquals(actualAddress, expectedAddress);
            }
        }

        softAssert.assertAll();
    }

    private void sortTableBy(String field, String sortType) {
        List<WebElement> headers = tblAvailabilities.findElements(By.xpath("./thead/tr/th"));
        WebElement header = headers.get(columnIndex.get(field));
        if (!header.getAttribute("class").contains(sortType)) {
            click(header.findElement(By.xpath(".//a")));
            waitForSpinnerToDisappear();
        }
    }
}
