package Pages.Salesforce;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Objects.Job;
import Objects.Shift;
import Pages.PageManager;
import org.apache.commons.collections4.MultiValuedMap;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.*;

public class SfRosterManagement {
    private WebDriver driver;

    public SfRosterManagement(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "table[ng-style*='$ctrl.getTimeslotTableWidth']")
    public WebElement mainTable;

    @FindBy(how = How.ID, using = "region-search")
    public WebElement txtRegionFilter;

    @FindBy(how = How.ID, using = "location-search")
    public WebElement txtLocationFilter;

    @FindBy(how = How.ID, using = "client-search")
    public WebElement txtClientFilter;

    @FindBy(how = How.ID, using = "resource-search")
    public WebElement txtResourceFilter;

    @FindBy(how = How.ID, using = "filter-period")
    public WebElement ddlPeriodFilter;

    @FindBy(how = How.CSS, using = "div[model='$ctrl.state.viewFilter']")
    public WebElement viewFilter;

    @FindBy(how = How.CSS, using = "div[model='$ctrl.state.statusFilter']")
    public WebElement statusFilter;

    @FindBy(how = How.CSS, using = "div[model='$ctrl.state.jobTypeFilter']")
    public WebElement jobTypeFilter;

    public void openRosterManagement() {
        driver.navigate().to(String.format("%s/lightning/n/skedhealthcare__Roster_Management", userContext.getLoginResource()));
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
    }

    public void scheduleJob(String jobAlias, String variables) throws ParseException, IOException {
        openRosterManagement();
        setFilterToDefault();

        // Drag and drop
        Actions actions = new Actions(driver);
        waitForElement(mainTable);
        actions.moveToElement(mainTable, 0, 0)
                .clickAndHold()
                .moveByOffset(0, 60)
                .release()
                .build().perform();

        // Set value for job
        Job job = new Job(variables);
        job.setAddressType("Client");
        job.setAddress("23 Ney Road, Capalaba, QLD 4157, Australia");
        String date = driver.findElement(By.xpath("//base-modal[@modal-heading='Create Job']//modal-content//dt/div[contains(text(),'Date')]/following-sibling::div")).getText();
        job.setStartDate(formatDateTime(date, "MMM d, yyyy", "dd/MM/yyyy"));
        String startTime = driver.findElement(By.xpath("//base-modal[@modal-heading='Create Job']//modal-content//dt/div[contains(text(),'Start Time')]/following-sibling::div")).getText();
        job.setStartTime(formatDateTime(startTime, "h:mma", "HH:mm"));
        String endTime = driver.findElement(By.xpath("//base-modal[@modal-heading='Create Job']//modal-content//dt/div[contains(text(),'End Time')]/following-sibling::div")).getText();
        job.setDuration((int) calculateDateTimeBetween(startTime, endTime, "h:mma", "Minutes"));
        scenarioContext.setValue(jobAlias, job);

        // Input default client contact
        sendKeys(driver.findElement(By.xpath("//base-modal[@modal-heading='Create Job']//input[@id='client-search']")), job.getContactName());
        waitForSpinnerToDisappear();
        click(driver.findElement(By.xpath(String.format("//div[contains(text(),'%s')]", job.getContactName()))));
        waitForSpinnerToDisappear();

        // click Schedule Job
        click(driver.findElement(By.cssSelector("button[ng-click*='$ctrl.confirmCreateSingleEvent']")));
        waitForSpinnerToDisappear();
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'skedhealthcare__skedJobDetails')]")));

        // Input Urgency and Description
        SfScheduleJobPage scheduleJobPage = new SfScheduleJobPage(driver);
        waitForElement(scheduleJobPage.btnCreateJob);
        selectDropdownlistByText(scheduleJobPage.ddlJobUrgency, job.getUrgency());
        sendKeys(scheduleJobPage.txtDescription, job.getDescription());

        // Select Service Agreement
        scheduleJobPage.setServiceAgreement();

        // Save Job
        click(scheduleJobPage.btnCreateJob);
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//*[text()='Save']")))
            click(scheduleJobPage.btnSaveOnConfictCheck);

        // Get Job UID
        String jobUID = new SkeduloAPI().getJobID(String.format("Description=='%s'", job.getDescription()));
        job.setUID(jobUID);

        if (jobUID != null) {
            storePropertyToFile("Jobs", jobUID, needToDeletePropertyFile);
            LogHelper.getLogger().info(String.format("Job UID: %s", jobUID));
            LogHelper.getLogger().info("Job is scheduled successfully");
        } else {
            LogHelper.getLogger().info("Some thing went wrong on scheduling new job");
        }
    }

    public void createShift(String shiftAlias, String resource) throws ParseException, IOException {
        // Go to Roster Management
        openRosterManagement();
        setFilterToDefault();
        inputSearchBox(txtResourceFilter, resource);
        selectDropdownWithMultiOptions(viewFilter, Arrays.asList("Shift"));

        // Drag and drop
        Actions actions = new Actions(driver);
        waitForElement(mainTable);
        actions.moveToElement(mainTable, 0, 0)
                .clickAndHold()
                .moveByOffset(0, 60)
                .release()
                .build().perform();

        Shift shift = new Shift();
        shift.setStartDate(formatDateTime(driver.findElement(By.id("shift-start-date")).getAttribute("value"), "d/M/yyyy", "dd/MM/yyyy"));
        shift.setEndDate(formatDateTime(driver.findElement(By.id("shift-end-date")).getAttribute("value"), "d/M/yyyy", "dd/MM/yyyy"));
        shift.setStartTime(formatDateTime(driver.findElement(By.id("shift-scheduled-start-time")).getAttribute("value"), "h:mm a", "HH:mm"));
        shift.setEndTime(formatDateTime(driver.findElement(By.id("shift-scheduled-end-time")).getAttribute("value"), "h:mm a", "HH:mm"));

        // Click Create button
        click(driver.findElement(By.cssSelector("button[ng-click*='$ctrl.saveShift()']")));
        waitForSpinnerToDisappear();

        // Get shiftId
        MultiValuedMap<String, String> UID = getResourceRelatedObjects("shifts", resource);
        shift.setShiftId(UID.values().toArray()[0].toString());
        scenarioContext.setValue(shiftAlias, shift);

        LogHelper.getLogger().info("Shift is created successfully");
    }

    public void createGroupEvent(String groupAlias, String params) throws InterruptedException {
        // Go to Roster Management
        openRosterManagement();

        // Click Create Group Event
        click(driver.findElement(By.cssSelector("button[ng-click*='$ctrl.doCreateGroupActivity()']")));
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(tabs.size() - 1));
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
        waitForSpinnerToDisappear();

        // Create Group Event
        new PageManager(driver).getSfNewGroupEventPage().inputInformationForNewGroup(groupAlias, params);
    }

    private void selectDropdownWithMultiOptions(WebElement dropdown, List<String> options) {
        click(dropdown);
        List<WebElement> optionElements = dropdown.findElements(By.cssSelector("li"));
        for (WebElement element : optionElements) {
            String optionName = element.getText();
            Boolean isSelected = element.getAttribute("class").contains("slds-is-selected");
            if (options.contains(optionName) != isSelected)
                click(element);
        }
        // Click outside element
        click(driver.findElement(By.cssSelector("div[ng-include*='$ctrl.getTemplateUrl']")));
        waitForSpinnerToDisappear();
    }

    private void inputSearchBox(WebElement searchElement, String value) {
        if (!searchElement.getAttribute("value").equals(value)) {
            clearSearchBox(searchElement);
            if (!value.equals("")) {
                sendKeys(searchElement, value);
                waitForSpinnerToDisappear();
                click(driver.findElement(By.xpath(String.format("//*[text()='%s']", value))));
                waitForSpinnerToDisappear();
            }
        }
    }

    private void clearSearchBox(WebElement searchElement) {
        if (searchElement.getAttribute("class").contains("ng-not-empty")) {
            click(searchElement.findElement(By.xpath("./preceding-sibling::*[contains(@ng-click,'$ctrl.clear')]")));
            waitForSpinnerToDisappear();
        }
    }

    private void setFilter(String region, String location, String client, String resource, String period, List<String> statuses, List<String> views, List<String> jobTypes) {
        if (region != null)
            inputSearchBox(txtRegionFilter, region);
        if (location != null)
            inputSearchBox(txtLocationFilter, location);
        if (client != null)
            inputSearchBox(txtClientFilter, client);
        if (resource != null)
            inputSearchBox(txtResourceFilter, resource);
        if (period != null)
            selectDropdownlistByText(ddlPeriodFilter, period);
        if (statuses != null)
            selectDropdownWithMultiOptions(statusFilter, statuses);
        if (views != null)
            selectDropdownWithMultiOptions(viewFilter, views);
        if (jobTypes != null)
            selectDropdownWithMultiOptions(jobTypeFilter, jobTypes);
    }

    private void setFilterToDefault() {
        setFilter("Brisbane",
                "",
                "",
                "",
                "2 Weeks",
                Arrays.asList("Queued", "Pending Allocation", "Pending Dispatch", "Dispatched", "Ready", "En Route", "On Site", "", "In Progress"),
                null,
                Arrays.asList("Single Booking", "Group Event"));
    }
}
