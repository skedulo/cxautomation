package Pages.Salesforce;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Objects.Job;
import Pages.PageManager;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.ParseException;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.storePropertyToFile;

public class SfGroupEventScheduleJobPage {
    private WebDriver driver;
    private PageManager pageManager;

    public SfGroupEventScheduleJobPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        pageManager = new PageManager(driver);
    }

    @FindBy(how = How.CSS, using = "input[name='startDate']")
    public WebElement txtStartDate;

    @FindBy(how = How.CSS, using = "input[name='startTime']")
    public WebElement txtStartTime;

    @FindBy(how = How.CSS, using = "input[name='jobDuration']")
    public WebElement txtJobDuration;

    @FindBy(how = How.CSS, using = "select[name='jobUrgency']")
    public WebElement ddlJobUrgency;

    @FindBy(how = How.CSS, using = "textarea[ng-model*='ctrl.job.description']")
    public WebElement txtDescription;

    @FindBy(how = How.CSS, using = "button[ng-click*='ctrl.saveJob']")
    public WebElement btnCreateJob;

    @FindBy(how = How.XPATH, using = "//save-job-conflicts-modal//button[contains(@ng-click,'ctrl.save')]")
    public WebElement btnSaveOnConfictCheck;

    @FindBy(how = How.XPATH, using = "//*[text()='Go to Job Details Page']")
    public WebElement btnGotoJobDetailsPage;

    public void scheduleJob(Job job) throws IOException {
        waitForSpinnerToDisappear();
        sendKeys(txtStartDate, job.getStartDate());
        txtStartDate.sendKeys(Keys.ESCAPE);
        sendKeys(txtStartTime, job.getStartTime());
        txtStartTime.sendKeys(Keys.ESCAPE);
        sendKeys(txtJobDuration, String.valueOf(job.getDuration()));
        sendKeys(txtDescription, job.getDescription());
        selectDropdownlistByText(ddlJobUrgency, job.getUrgency());
        click(btnCreateJob);
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//save-job-conflicts-modal//button[contains(@ng-click,'ctrl.save')]")))
            click(btnSaveOnConfictCheck);
        waitForSpinnerToDisappear();

        // Get Job UID
        String jobUID = new SkeduloAPI().getJobID(String.format("Description=='%s'", job.getDescription()));
        job.setUID(jobUID);

        if (jobUID != null) {
            storePropertyToFile("Jobs", jobUID, needToDeletePropertyFile);
            LogHelper.getLogger().info(String.format("Job UID: %s", jobUID));
            LogHelper.getLogger().info("Job is scheduled successfully in group event");
        } else {
            LogHelper.getLogger().info("Some thing went wrong on scheduling new job for group event");
        }
    }

    public void allocateResource(String resource, String jobAlias) {
        click(driver.findElement(By.cssSelector("button[ng-click*='getJobCostingAndAllocate']")));
        waitForSpinnerToDisappear();
        click(driver.findElement(By.cssSelector("button[ng-click*='ctrl.refreshAvailableResources']")));
        waitForSpinnerToDisappear();
        click(driver.findElement(By.xpath(String.format("//li[contains(@ng-click,'toggleResourceSelection') and .//p/text()='%s']", resource))));
        click(driver.findElement(By.xpath(String.format("//p[@title='%s']/ancestor::li[contains(@ng-click,'toggleGetCostingResourceSelection')]//button[contains(@ng-click,'allocateResource')]", resource))));
        click(driver.findElement(By.cssSelector("button[ng-click*='ctrl.saveJob']")));
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//save-job-conflicts-modal//button[contains(@ng-click,'ctrl.save')]")))
            click(btnSaveOnConfictCheck);
        LogHelper.getLogger().info(String.format("Resource %s is allocated successfully through Salesforce", resource));
    }

    public void checkJobDetails(String jobAlias) {
        Job job = (Job) scenarioContext.getValue(jobAlias);
        softAssert = new SoftAssert();
        softAssert.assertEquals(txtStartDate.getAttribute("value"), job.getStartDate());
        softAssert.assertEquals(txtStartTime.getAttribute("value"), job.getStartTime());
        softAssert.assertEquals(txtJobDuration.getAttribute("value"), String.valueOf(job.getDuration()));
        softAssert.assertEquals(new Select(ddlJobUrgency).getFirstSelectedOption().getText(), job.getUrgency());
        softAssert.assertEquals(txtDescription.getAttribute("value"), job.getDescription());
        softAssert.assertAll();
    }

    public void editJob(String jobAlias, String params) throws ParseException {
        Job job = (Job) scenarioContext.getValue(jobAlias);
        job.updateValues(params);
        scenarioContext.setValue(jobAlias, job);
        JSONObject jParams = new JSONObject(params);

        if (jParams.has("StartDate")) {
            sendKeys(txtStartDate, job.getStartDate());
            txtStartDate.sendKeys(Keys.ESCAPE);
        }

        if (jParams.has("StartTime")) {
            sendKeys(txtStartTime, job.getStartTime());
            txtStartTime.sendKeys(Keys.ESCAPE);
        }

        if (jParams.has("Duration")) {
            sendKeys(txtJobDuration, String.valueOf(job.getDuration()));
        }

        if (jParams.has("Urgency")) {
            Select selectUrgency = new Select(ddlJobUrgency);
            selectUrgency.selectByVisibleText(job.getUrgency());
        }

        if (jParams.has("Description")) {
            sendKeys(txtDescription, job.getDescription());
        }

        if (jParams.has("Resource")) {
            String resource = jParams.get("Resource").toString();
            waitForSpinnerToDisappear();
            click(driver.findElement(By.cssSelector("button[ng-click='resourceRequirementsSectionOpened=!resourceRequirementsSectionOpened']")));
            click(driver.findElement(By.cssSelector("button[ng-click*='ctrl.refreshAvailableResources']")));
            waitForSpinnerToDisappear();
            click(driver.findElement(By.xpath(String.format("//li[contains(@ng-click,'toggleResourceSelection') and .//p/text()='%s']", resource))));
            click(driver.findElement(By.xpath(String.format("//p[@title='%s']/ancestor::li[contains(@ng-click,'toggleGetCostingResourceSelection')]//button[contains(@ng-click,'allocateResource')]", resource))));
        }

        click(btnCreateJob);
        waitForSpinnerToDisappear();
        if (checkElementExist(By.xpath("//save-job-conflicts-modal//button[contains(@ng-click,'ctrl.save')]")))
            click(btnSaveOnConfictCheck);
        LogHelper.getLogger().info(String.format("Job %s is updated successfully", jobAlias));
    }

    public void checkResourceStatus(String resource, String status) {
        if (!driver.findElement(By.cssSelector("button[ng-click*='ctrl.refreshAvailableResources']")).isDisplayed())
            click(driver.findElement(By.cssSelector("button[ng-click='resourceRequirementsSectionOpened=!resourceRequirementsSectionOpened']")));

        softAssert = new SoftAssert();
        softAssert.fail("Need to be implemented");
        softAssert.assertAll();
    }
}
