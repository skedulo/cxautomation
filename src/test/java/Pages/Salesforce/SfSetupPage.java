package Pages.Salesforce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SfSetupPage {

    private WebDriver driver;
    private WebDriverWait wait;

    public SfSetupPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = new WebDriverWait(this.driver, 60);
    }

    @FindBy(how = How.XPATH, using = "//input[@placeholder='Quick Find']")
    public WebElement txtQuickSearch;

    public void SearchAndGoToPage(String page) {
        txtQuickSearch.sendKeys(page);
        ClickItem(By.xpath("//div[@title='" + page + "']/child::a"));
    }

    private void ClickItem(By itemLocator) {
        wait.until(ExpectedConditions.elementToBeClickable(itemLocator));
        driver.findElement(itemLocator).click();
    }
}
