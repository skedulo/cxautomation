package Pages;

import Pages.Salesforce.*;
import Pages.Salesforce.ARC.*;
import Pages.Skedulo.SkeduloJobDetails;
import org.openqa.selenium.WebDriver;

public class PageManager {

    private WebDriver driver;
    private SfBasePage sfBasePage;
    private SfHomePage sfHomePage;
    private SfLoginPage sfLoginPage;
    private SfSetupPage sfSetupPage;
    private SfCompanyInfoPage sfCompanyInfoPage;
    private SfInstalledPackages sfInstalledPackages;
    private SfContactDetailPage sfContactDetailPage;
    private SfScheduleJobPage sfScheduleJobPage;
    private SfJobDetailsPage sfJobDetailsPage;
    private SfEditJobPage sfEditJobPage;
    private SkeduloJobDetails skeduloJobDetails;
    private SfGroupEventPage sfGroupEventPage;
    private SfNewGroupEventPage sfNewGroupEventPage;
    private SfGroupEventScheduleJobPage sfGroupEventScheduleJobPage;
    private SfCreateAvailabilityPage sfCreateAvailabilityPage;
    private SfResourceAvailabilitiesPage sfResourceAvailabilitiesPage;
    private SfRosterManagement sfRosterManagement;
    private SfShiftDetailsPage sfShiftDetailsPage;
    private SfTrainingEventPage sfTrainingEventPage;
    private SfScheduleFullServicePage sfScheduleFullServicePage;
    private SfAllocateResourcesPage sfAllocateResourcesPage;
    private SfCommunitySchedulingPage sfCommunitySchedulingPage;
    private SfScheduleCommunityJobPage sfScheduleCommunityJobPage;

    public PageManager(WebDriver driver) {
        this.driver = driver;
    }

    public SfBasePage getSfBasePage() {
        return (sfBasePage == null) ? sfBasePage = new SfBasePage(driver) : sfBasePage;
    }

    public SfLoginPage getSfLoginPage() {
        return (sfLoginPage == null) ? sfLoginPage = new SfLoginPage(driver) : sfLoginPage;
    }

    public SfHomePage getSfHomePage() {
        return (sfHomePage == null) ? sfHomePage = new SfHomePage(driver) : sfHomePage;
    }

    public SfSetupPage getSfSetupPage() {
        return (sfSetupPage == null) ? sfSetupPage = new SfSetupPage(driver) : sfSetupPage;
    }

    public SfCompanyInfoPage getSfCompanyInfoPage() {
        return (sfCompanyInfoPage == null) ? sfCompanyInfoPage = new SfCompanyInfoPage(driver) : sfCompanyInfoPage;
    }

    public SfInstalledPackages getSfInstalledPackages() {
        return (sfInstalledPackages == null) ? sfInstalledPackages = new SfInstalledPackages(driver) : sfInstalledPackages;
    }

    public SfContactDetailPage getSfContactDetailPage() {
        return (sfContactDetailPage == null) ? sfContactDetailPage = new SfContactDetailPage(driver) : sfContactDetailPage;
    }

    public SfScheduleJobPage getSfScheduleJobPage() {
        return (sfScheduleJobPage == null) ? sfScheduleJobPage = new SfScheduleJobPage(driver) : sfScheduleJobPage;
    }

    public SfJobDetailsPage getSfJobDetailsPage() {
        return (sfJobDetailsPage == null) ? sfJobDetailsPage = new SfJobDetailsPage(driver) : sfJobDetailsPage;
    }

    public SkeduloJobDetails getSkeduloJobDetails() {
        return (skeduloJobDetails == null) ? skeduloJobDetails = new SkeduloJobDetails(driver) : skeduloJobDetails;
    }

    public SfEditJobPage getSfEditJobPage() {
        return (sfEditJobPage == null) ? sfEditJobPage = new SfEditJobPage(driver) : sfEditJobPage;
    }

    public SfGroupEventPage getSfGroupEventPage() {
        return (sfGroupEventPage == null) ? sfGroupEventPage = new SfGroupEventPage(driver) : sfGroupEventPage;
    }

    public SfNewGroupEventPage getSfNewGroupEventPage() {
        return (sfNewGroupEventPage == null) ? sfNewGroupEventPage = new SfNewGroupEventPage(driver) : sfNewGroupEventPage;
    }

    public SfGroupEventScheduleJobPage getSfGroupEventScheduleJobPage() {
        return (sfGroupEventScheduleJobPage == null) ? sfGroupEventScheduleJobPage = new SfGroupEventScheduleJobPage(driver) : sfGroupEventScheduleJobPage;
    }

    public SfCreateAvailabilityPage getSfCreateAvailabilityPage() {
        return (sfCreateAvailabilityPage == null) ? sfCreateAvailabilityPage = new SfCreateAvailabilityPage(driver) : sfCreateAvailabilityPage;
    }

    public SfResourceAvailabilitiesPage getSfResourceAvailabilitiesPage() {
        return (sfResourceAvailabilitiesPage == null) ? sfResourceAvailabilitiesPage = new SfResourceAvailabilitiesPage(driver) : sfResourceAvailabilitiesPage;
    }

    public SfRosterManagement getSfRosterManagement() {
        return (sfRosterManagement == null) ? sfRosterManagement = new SfRosterManagement(driver) : sfRosterManagement;
    }

    public SfShiftDetailsPage getSfShiftDetailsPage() {
        return (sfShiftDetailsPage == null) ? sfShiftDetailsPage = new SfShiftDetailsPage(driver) : sfShiftDetailsPage;
    }

    public SfTrainingEventPage getSfTrainingEventPage() {
        return (sfTrainingEventPage == null) ? sfTrainingEventPage = new SfTrainingEventPage(driver) : sfTrainingEventPage;
    }

    public SfScheduleFullServicePage getSfScheduleFullServicePage() {
        return (sfScheduleFullServicePage == null) ? sfScheduleFullServicePage = new SfScheduleFullServicePage(driver) : sfScheduleFullServicePage;
    }

    public SfAllocateResourcesPage getSfAllocateResourcesPage() {
        return (sfAllocateResourcesPage == null) ? sfAllocateResourcesPage = new SfAllocateResourcesPage(driver) : sfAllocateResourcesPage;
    }

    public SfCommunitySchedulingPage getSfCommunitySchedulingPage() {
        return (sfCommunitySchedulingPage == null) ? sfCommunitySchedulingPage = new SfCommunitySchedulingPage(driver) : sfCommunitySchedulingPage;
    }

    public SfScheduleCommunityJobPage getSfScheduleCommunityJobPage() {
        return (sfScheduleCommunityJobPage == null) ? sfScheduleCommunityJobPage = new SfScheduleCommunityJobPage(driver) : sfScheduleCommunityJobPage;
    }
}