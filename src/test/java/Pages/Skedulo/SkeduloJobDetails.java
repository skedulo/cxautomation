package Pages.Skedulo;

import Base.LogHelper;
import Objects.Job;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import java.text.ParseException;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.formatDateTime;

public class SkeduloJobDetails {
    private WebDriver driver;

    public SkeduloJobDetails(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(how = How.CSS, using = "[id='jobDetailsType'] div[data-sk-name]")
    public WebElement txtJobType;

    @FindBy(how = How.CSS, using = "[id='jobDetailsDescription'] div[data-sk-name]")
    public WebElement txtDescription;

    @FindBy(how = How.CSS, using = "[id='jobDetailsAccount'] div[data-sk-name]")
    public WebElement txtAccountName;

    @FindBy(how = How.CSS, using = "[id='jobDetailsContact'] div[data-sk-name]")
    public WebElement txtContactName;

    @FindBy(how = How.CSS, using = "[id='jobDetailsRegion'] div[data-sk-name]")
    public WebElement txtRegion;

    @FindBy(how = How.CSS, using = "[id='jobDetailsAddress'] div[data-sk-name]")
    public WebElement txtAddress;

    @FindBy(how = How.CSS, using = "[id='jobDetailsLocation'] div[data-sk-name]")
    public WebElement txtLocation;

    @FindBy(how = How.CSS, using = "[id='jobDetailsUrgency'] div[data-sk-name]")
    public WebElement txtUrgency;

    @FindBy(how = How.CSS, using = "[id='jobDetailsDurationHour'] input")
    public WebElement txtDurationHour;

    @FindBy(how = How.CSS, using = "[id='jobDetailsDurationMinute'] input")
    public WebElement txtDurationMinute;

    @FindBy(how = How.CSS, using = "input[name='datepicker-date-input'][placeholder='Start date']")
    public WebElement txtStartDate;

    @FindBy(how = How.CSS, using = "[data-sk-name='time-input-wrapper'] input[placeholder='Start time']")
    public WebElement txtStartTime;

    @FindBy(how = How.CSS, using = "input[name='datepicker-date-input'][placeholder='End date']")
    public WebElement txtEndDate;

    @FindBy(how = How.CSS, using = "[data-sk-name='time-input-wrapper'] input[placeholder='End time']")
    public WebElement txtEndTime;

    @FindBy(how = How.CSS, using = "button[data-sk-name='add-allocation']")
    public WebElement btnAddAllocation;

    public void checkJobDetails(Job job) {
        softAssert = new SoftAssert();
        softAssert.assertEquals(driver.findElement(By.cssSelector("#jobDetailsDescription > div > div")).getText(), job.getDescription());
        softAssert.assertEquals(driver.findElement(By.cssSelector("div[data-sk-name='resource-status']")).getText(), "Dispatched");
        softAssert.assertAll();
    }

    public void checkJobDetails(String jobAlias) throws ParseException {
        Job job = (Job) scenarioContext.getValue(jobAlias);

        softAssert = new SoftAssert();
        softAssert.assertEquals(txtJobType.getText(), job.getJobType());
        softAssert.assertEquals(txtDescription.getText(), job.getDescription());
        softAssert.assertEquals(txtRegion.getText(), job.getRegion());
        softAssert.assertEquals(txtAddress.getText(), job.getAddress());
        softAssert.assertEquals(txtUrgency.getText(), job.getUrgency());
        softAssert.assertEquals(txtDurationHour.getAttribute("value"), String.valueOf(job.getDuration() / 60));
        softAssert.assertEquals(txtDurationMinute.getAttribute("value"), String.valueOf(job.getDuration() % 60));
        softAssert.assertEquals(txtStartDate.getAttribute("value"), formatDateTime(job.getStartDate(), job.getStartTime(), 0, userContext.getWebAppDateFormat()));
        softAssert.assertEquals(txtStartTime.getAttribute("value"), formatDateTime(job.getStartDate(), job.getStartTime(), 0, "h:mma").toLowerCase());
        softAssert.assertEquals(txtEndDate.getAttribute("value"), formatDateTime(job.getStartDate(), job.getStartTime(), job.getDuration(),  userContext.getWebAppDateFormat()));
        softAssert.assertEquals(txtEndTime.getAttribute("value"), formatDateTime(job.getStartDate(), job.getStartTime(), job.getDuration(), "h:mma").toLowerCase());

        softAssert.assertAll();
    }

    public void allocateResource(String resource, String jobAlias) {
        click(btnAddAllocation);
        waitForSpinnerToDisappear();

        sendKeys(driver.findElement(By.cssSelector(".search-container input")), resource);
        waitForSpinnerToDisappear();

        int count = 5;
        while (!driver.findElement(By.cssSelector("button[data-sk-name='button-resource-select-save']")).isEnabled() && count > 0) {
            click(driver.findElement(By.xpath(String.format("//div[@data-sk-name='resource-name' and ./span[text()='%s']]/ancestor::div[@data-sk-name='resource-card']//input", resource))));
            count--;
        }
        click(driver.findElement(By.cssSelector("button[data-sk-name='button-resource-select-save']")));
        waitForSpinnerToDisappear();

        LogHelper.getLogger().info(String.format("%s is allocated successfully", resource));
    }

    public void dispatchResource(String resource, String jobAlias) {
        WebElement btnDispatch = driver.findElement(
                By.xpath(String.format("//span[@class='resource-allocation-card__title' and ./a[@data-sk-name='resource-name']/text()='%s']/ancestor::div[@class='resource-allocation-card']//button[@data-sk-name='resource-dispatch-button' and text()='Dispatch']",
                        resource)));

        click(btnDispatch);

        click(driver.findElement(By.cssSelector("button[id='buttonModalConfirmOk']")));

        waitForElement(btnDispatch);

        LogHelper.getLogger().info(String.format("%s is dispatched successfully", resource));
    }

    public void checkResourceStatus(String resource, String status) throws InterruptedException {
        int count = 30;
        By statusLocator = By.xpath(String.format("//span[@class='resource-allocation-card__title' and ./a[@data-sk-name='resource-name']/text()='%s']/ancestor::div[@class='resource-allocation-card']//div[@data-sk-name='resource-status']", resource));
        String actualStatus = driver.findElement(statusLocator).getText();

        while (count > 0 && !actualStatus.equals(status)) {
            Thread.sleep(5000); // Wait for resource to be updated in database
            driver.navigate().refresh();
            actualStatus = driver.findElement(statusLocator).getText();
            count--;
        }
        softAssert = new SoftAssert();
        softAssert.assertEquals(actualStatus, status);
        softAssert.assertAll();
    }
}
