Feature: Login Feature

  Scenario: Login to SF with correct credential
    Given I navigate to SF Login page
    And I enter the following for Login
      | username                 | password      |
      | healthcareqa@skedulo.com | sked2019!     |
      | sales                    | salespassword |
    And I click the Login button
    Then I should be the SF Home page


  Scenario Outline: Login to SF with correct credential using scenario outline
    Given I navigate to SF Login page
    And I enter <username> and <password>
    And I click the Login button
    Then I should be the SF Home page

    Examples:
      | username                 | password      |
      | healthcareqa@skedulo.com | sked2019!     |
      | sales                    | salespassword |
