Feature: User login then check resource setting

  Background: User is logged in
    Given I logged into Salesforce as "Admin"

  Scenario: Check user name
    Given I open profile panel
    Then I should see my name in profile panel

  Scenario: Check my orgID in Company Information page
    Given I go to page Setup
    And I go to page "Company Information" from Setup
    Then I should see my org in Company Info

  Scenario: Check Install packages
    Given I go to page Setup
    And I go to page "Installed Packages" from Setup
    Then I should see my packages in Installed Packages

  Scenario: Check logout
    Given I logged out Salesforce
    Then I should be in Login page

