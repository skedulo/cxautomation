@Admin @ARC @SFDC @CleanUp @ARC013
Feature: ARC013 - Allocate / Unallocate resources for CEP Job

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 1, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Nurse Assistant Testing"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-027 @ARC-TC-SM-028
  Scenario Outline: TC-SM-027 - TC-SM-028 - Allocate resources for CEPs job being Pending Allocation
    Given Schedule CEP job "cep_job" with "Save & Close"
      | CourseCatalog       | course_catalog          |
      | Region              | Central Mid-West CST    |
      | Location            | IL - ArcChiro - NAT     |
      | StartTime           | Next Tuesday at 10:00   |
      | Duration            | 120                     |
      | Evaluators          | 2                       |
      | Type                | Nurse Assistant Testing |
      | SpecialInstructions | Special Instructions    |
      | IsRecurring         | No                      |

    And Allocate resources for cep job "cep_job" by "Allocate Resources" with notification "<notification_status>"
      | All | Sked Resource 10 |
      | 1   | Sked Resource 11 |

    Then Jobs are created correctly for CEP job "cep_job"

    Examples:
      | notification_status |
      | true                |
      | false               |

  @ARC-TC-SM-029
  Scenario Outline: TC-SM-029 - Unassign resources for CEPs jobs
    Given Schedule CEP job "cep_job" with "Save & Allocate"
      | CourseCatalog       | course_catalog          |
      | Region              | Central Mid-West CST    |
      | Location            | IL - ArcChiro - NAT     |
      | StartTime           | Next Tuesday at 10:00   |
      | Duration            | 120                     |
      | Evaluators          | 2                       |
      | Type                | Nurse Assistant Testing |
      | SpecialInstructions | Special Instructions    |
      | IsRecurring         | No                      |

    And Allocate resources for cep job "cep_job" with notification "<notification_status>"
      | All | Sked Resource 10 |
      | 1   | Sked Resource 11 |

    And Unassign resources from CEP job "cep_job" by "Allocate Resources" and "Save" with notification "<notification_status>"
      | 1 | Sked Resource 10, Sked Resource 11 |

    Then Jobs are created correctly for CEP job "cep_job"

    Examples:
      | notification_status |
      | true                |
      | false               |