@Admin @ARC @SFDC @CleanUp @ARC015
Feature: ARC015 - Reschedule CEP jobs by Drag and Drop

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 1, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Nurse Assistant Testing"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-031
  Scenario Outline: TC-SM-031 - Reschedule CEP job (allocated resource) by Drag And Drop
    Given Schedule CEP job "cep_job" with "Save & Allocate"
      | CourseCatalog       | course_catalog          |
      | Region              | Central Mid-West CST    |
      | Location            | IL - ArcChiro - NAT     |
      | StartTime           | Next Tuesday at 10:00   |
      | Duration            | 120                     |
      | Evaluators          | 2                       |
      | Type                | Nurse Assistant Testing |
      | SpecialInstructions | Special Instructions    |
      | IsRecurring         | No                      |

    And Allocate resources for cep job "cep_job" with notification "<notification_status>"
      | All | Sked Resource 10 |
      | 1   | Sked Resource 11 |

    And Reschedule jobs for CEP job "cep_job" by "Drag and Drop"
      | 1 | Next Wednesday | IL - Saint Louis Scott AFB - NAT |

    Then Jobs are created correctly for CEP job "cep_job"

    Examples:
      | notification_status |
      | true                |
      | false               |