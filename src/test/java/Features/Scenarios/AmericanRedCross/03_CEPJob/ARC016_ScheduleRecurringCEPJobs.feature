@Admin @ARC @SFDC @CleanUp @ARC016
Feature: ARC016 - Schedule recurring CEP jobs

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 3, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Nurse Assistant Testing"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-032
  Scenario Outline: TC-SM-032 - Schedule recurring CEP jobs with allocating resources
    Given Schedule CEP job "cep_job" with "Save & Allocate"
      | CourseCatalog       | course_catalog          |
      | Region              | Central Mid-West CST    |
      | Location            | IL - ArcChiro - NAT     |
      | StartTime           | Next Tuesday at 10:00   |
      | Duration            | 120                     |
      | Evaluators          | 2                       |
      | Type                | Nurse Assistant Testing |
      | SpecialInstructions | Special Instructions    |
      | IsRecurring         | Yes                     |
      | ClassDays           | 3                       |
      | RepeatWeeks         | 1                       |
      | WeekDays            | Mon,Wed                 |
      | IsSkippedWeekend    | No                      |
      | IsStackedClass      | No                      |

    And Allocate resources for cep job "cep_job" with notification "<notification_status>"
      | All | Sked Resource 10 |
      | 1   | Sked Resource 11 |

    Then Jobs are created correctly for CEP job "cep_job"

    Examples:
      | notification_status |
      | true                |
      | false               |

  @ARC-TC-SM-033
  Scenario: TC-SM-033 - Schedule recurring CEP jobs without allocating resources
    Given Schedule CEP job "cep_job" with "Save & Close"
      | CourseCatalog       | course_catalog          |
      | Region              | Central Mid-West CST    |
      | Location            | IL - ArcChiro - NAT     |
      | StartTime           | Next Tuesday at 10:00   |
      | Duration            | 120                     |
      | Evaluators          | 2                       |
      | Type                | Nurse Assistant Testing |
      | SpecialInstructions | Special Instructions    |
      | IsRecurring         | Yes                     |
      | ClassDays           | 3                       |
      | RepeatWeeks         | 1                       |
      | WeekDays            | Mon,Wed                 |
      | IsSkippedWeekend    | No                      |
      | IsStackedClass      | No                      |

    Then Jobs are created correctly for CEP job "cep_job"