@Admin @ARC @SFDC @CleanUp @ARC018
Feature: ARC018 - Schedule NAT jobs

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 3, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Nurse Assistant Training"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

    And Create course template "course_template" using SkeduloAPI with:
    """
    { Name: "Course Template ", CourseCatalog: course_catalog, MaximumHoursPerDay: 7, CourseClassDays: "Monday to Friday" }
    """
    And Create course class type "course_class_type" using SkeduloAPI with:
    """
    {
      Name:"Course Class Type ", CourseTemplate: course_template, DisplayOrder: 1,
      StartTime: 900, EndTime: 1600, RatioToInstructor: 10, TotalHours: 30, ClassType: "Theory"
    }
    """

  @ARC-TC-SM-036
  Scenario: TC-SM-036 - Schedule NAT Jobs with allocating resources
    Given Schedule NAT job "nat_job" with "Generate Course & Allocate"
      | Region              | Central Mid-West CST      |
      | CourseCatalog       | course_catalog            |
      | Type                | Nurse Assistant Training  |
      | StartDate           | Next Tuesday              |
      | MaxHoursPerDay      | 8                         |
      | SpecialInstructions | Test Special Instructions |
      | MaxStudents         | 10                        |
      | Location            | IL - ArcChiro - NAT       |
      | IsRecurring         | Yes                       |
      | DaysOfWeek          | Mon,Wed,Fri               |

    And Allocate resources for nat job "nat_job" with notification "true"
      | All | Sked Resource 20 |

    And Jobs are created correctly for NAT job "nat_job"
