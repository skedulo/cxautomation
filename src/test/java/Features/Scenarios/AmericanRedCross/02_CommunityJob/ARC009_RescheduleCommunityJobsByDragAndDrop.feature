@Admin @ARC @SFDC @CleanUp @ARC009
Feature: ARC009 - Reschedule community jobs by Drag and Drop

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 1, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Community"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-017
  Scenario: TC-SM-017 - User can reschedule Community job (no resource) by drag and drop
    Given Schedule community job "community_job" with "Save & Close"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | No                          |

    And Reschedule jobs for community job "community_job" by "Drag and Drop"
      | 1 | Next Wednesday | AR - Rogers, Center for Nonprofits |

    Then Jobs are created correctly for community job "community_job"

  @ARC-TC-SM-019
  Scenario: TC-SM-019 - User can reschedule Community job (allocated resource) by drag and drop
    Given Schedule community job "community_job" with "Save & Allocate"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | No                          |

    And Allocate resources for community job "community_job" with notification "true"
      | All | Sked Resource 1 |
      | 1   | Sked Resource 2 |

    And Reschedule jobs for community job "community_job" by "Drag and Drop"
      | 1 | Next Wednesday | AR - Rogers, Center for Nonprofits |

    Then Jobs are created correctly for community job "community_job"