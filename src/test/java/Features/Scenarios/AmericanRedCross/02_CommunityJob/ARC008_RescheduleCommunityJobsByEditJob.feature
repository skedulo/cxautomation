@Admin @ARC @SFDC @CleanUp @ARC008
Feature: ARC008 - Reschedule community jobs by editing job

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 1, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Community"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-016
  Scenario: TC-SM-016 - User can reschedule Community job (no resource) by changing in Job Details modal
    Given Schedule community job "community_job" with "Save & Close"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | No                          |

    And Reschedule jobs for community job "community_job" by "Edit Job" with "Save & Allocate"
      | 1 | StartTime | Next Wednesday at 13:00 | Location | AR - Rogers, Center for Nonprofits |

    And Allocate resources for community job "community_job" with notification "true"
      | All | Sked Resource 1 |
      | 1   | Sked Resource 2 |

    Then Jobs are created correctly for community job "community_job"

  @ARC-TC-SM-018
  Scenario: TC-SM-018 - User can reschedule Community job (allocated resource) by changing in Job Details modal
    Given Schedule community job "community_job" with "Save & Allocate"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | No                          |

    And Allocate resources for community job "community_job" with notification "true"
      | All | Sked Resource 1 |
      | 1   | Sked Resource 2 |

    And Reschedule jobs for community job "community_job" by "Edit Job" with "Save & Allocate"
      | 1 | StartTime | Next Wednesday at 13:00 | Location | AR - Rogers, Center for Nonprofits |

    And Unassign resources from community job "community_job" and "Keep Allocate Resources opening"
      | 1 | Sked Resource 1, Sked Resource 2 |

    And Allocate resources for community job "community_job" with notification "true"
      | All | Sked Resource 3 |
      | 1   | Sked Resource 4 |

    Then Jobs are created correctly for community job "community_job"