@Admin @ARC @SFDC @CleanUp @ARC007
Feature: ARC007 - Allocate / Un-allocate resources for community jobs

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 1, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Community"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-013 @ARC-TC-SM-014
  Scenario Outline: TC-SM-013 - TC-SM-014 - User can allocate resource for community jobs with status being Pending Allocation
    Given Schedule community job "community_job" with "Save & Close"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | No                          |

    And Allocate resources for community job "community_job" by "Allocate Resources" with notification "<notification_status>"
      | All | Sked Resource 1 |
      | 1   | Sked Resource 2 |

    Then Jobs are created correctly for community job "community_job"

    Examples:
      | notification_status |
      | true                |
      | false               |

  @ARC-TC-SM-015
  Scenario Outline: TC-SM-015 - User can un-allocate resource from Community Job
    Given Schedule community job "community_job" with "Save & Allocate"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | No                          |

    And Allocate resources for community job "community_job" with notification "true"
      | All | Sked Resource 1 |
      | 1   | Sked Resource 2 |

    And Unassign resources from community job "community_job" by "Allocate Resources" and "Save" with notification "<notification_status>"
      | 1 | Sked Resource 1, Sked Resource 2 |

    Then Jobs are created correctly for community job "community_job"

    Examples:
      | notification_status |
      | true                |
      | false               |