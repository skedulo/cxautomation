@Admin @ARC @SFDC @CleanUp @ARC011
Feature: ARC011 - Cancel / Delete Community Job

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Community Course ", NumberOfClasses: 3, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Community"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """

  @ARC-TC-SM-022
  Scenario: TC-SM-022 - User can cancel job was scheduled before
    Given Schedule community job "community_job" with "Save & Allocate"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | Yes                         |
      | ClassDays             | 3                           |
      | RepeatWeeks           | 1                           |
      | WeekDays              | Mon,Wed                     |
      | IsSkippedWeekend      | No                          |
      | IsStackedClass        | No                          |

    And Allocate resources for community job "community_job" with notification "true"
      | All | Sked Resource 1 |
      | 1   | Sked Resource 2 |
      | 2   | Sked Resource 3 |
      | 3   | Sked Resource 4 |

    And Cancel jobs "1,2" for community job "community_job"

    Then Jobs are created correctly for community job "community_job"

  @ARC-TC-SM-023
  Scenario: TC-SM-023 - User can delete job was scheduled before
    Given Schedule community job "community_job" with "Save & Close"
      | CourseCatalog         | course_catalog              |
      | CommunityClassSubType | Any                         |
      | Region                | Central Mid-West CST        |
      | Location              | AR - Little Rock, Red Cross |
      | StartTime             | Next Tuesday at 10:00       |
      | Duration              | 120                         |
      | IsRecurring           | Yes                         |
      | ClassDays             | 3                           |
      | RepeatWeeks           | 1                           |
      | WeekDays              | Mon,Wed                     |
      | IsSkippedWeekend      | No                          |
      | IsStackedClass        | No                          |

    And Delete jobs "1,2" for community job "community_job"

    Then Jobs are created correctly for community job "community_job"