@Admin @ARC @SFDC @CleanUp @ARC001
Feature: ARC001 - Schedule Full Service Job Then Allocate Resource

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Course Catalog", NumberOfClasses: 3, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:120, Type: ["Full Service"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """
    And Create training event "training_event" using SkeduloAPI with:
    """
    { TrainingContactId:"003g000001VqP0qAAF", OpportunityId:"006g000000EsCsOAAV", AccountId:"001g000001hc42ZAAQ" }
    """
    And Clean up all "jobs" for resource "Sked Resource 1, Sked Resource 2, Sked Resource 3, Sked Resource 4"

  @ARC-TC-SM-001
  Scenario: TC-SM-001 - User can schedule job with job type is Full Service (allocate Resource)
    Given Schedule Full Service for training event "training_event" with
      | TrainingEvent | training_event  |
      | Region        | Any             |
      | CourseCatalog | course_catalog  |
      | Address       | California, USA |

    Then "course_catalog" course catalog should populated correctly in Schedule Full Service model
    And Select job slots for training event "training_event" with "Save & Allocate"
      | job_1 | Tomorrow at 09:00     |
      | job_2 | 2 Days Later at 11:00 |
      | job_3 | 3 Days Later at 13:00 |

    And Allocate resources for training event "training_event" by "Schedule Full Service" with notification "true"
      | All   | Sked Resource 1 |
      | job_1 | Sked Resource 2 |
      | job_2 | Sked Resource 3 |
      | job_3 | Sked Resource 4 |

    Then Jobs are created correctly for training event "training_event"

  @ARC-TC-SM-002
  Scenario: TC-SM-002 - User can schedule job with job type is Full Service without allocate Resource
    Given Schedule Full Service for training event "training_event" with
      | TrainingEvent | training_event  |
      | Region        | Any             |
      | CourseCatalog | course_catalog  |
      | Address       | California, USA |

    And Select job slots for training event "training_event" with "Save & Close"
      | job_1 | Tomorrow at 09:00     |
      | job_2 | 2 Days Later at 11:00 |
      | job_3 | 3 Days Later at 13:00 |

    Then Jobs are created correctly for training event "training_event"

  @ARC-TC-SM-003 @ARC-TC-SM-004
  Scenario Outline: TC-SM-003 - TC-SM-004 - User can schedule Full Service job then allocate resource later by Manage Resource
    Given Schedule Full Service for training event "training_event" with
      | TrainingEvent | training_event  |
      | Region        | Any             |
      | CourseCatalog | course_catalog  |
      | Address       | California, USA |

    And Select job slots for training event "training_event" with "Save & Close"
      | job_1 | Tomorrow at 09:00     |
      | job_2 | 2 Days Later at 11:00 |
      | job_3 | 3 Days Later at 13:00 |

    And Allocate resources for training event "training_event" by "Manage Resource" with notification "<notification_status>"
      | All   | Sked Resource 1 |
      | job_1 | Sked Resource 2 |
      | job_2 | Sked Resource 3 |
      | job_3 | Sked Resource 4 |

    Then Jobs are created correctly for training event "training_event"

    Examples:
      | notification_status |
      | true                |
      | false               |

