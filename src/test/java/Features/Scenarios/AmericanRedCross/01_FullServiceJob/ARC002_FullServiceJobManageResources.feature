@Admin @ARC @SFDC @CleanUp @ARC002
Feature: ARC002 - Unassign Resource from Full Service Jobs

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And Create course catalog "course_catalog" using SkeduloAPI with:
    """
    {
      Name:"Course Catalog", NumberOfClasses: 3, NumberOfInstructors:2, NumberOfBreaks: 3, NumberOfStudents: 20,
      BreakDuration:15, RatioToInstructor:"This is ratio to instructor", ClassDuration:180, Type: ["Full Service"],
      IsActive:true, ExternalID:"ExtID", BackupName:"BackupName ABC", MasterClassName:"Master Class"
    }
    """
    And Create training event "training_event" using SkeduloAPI with:
    """
    { TrainingContactId:"003g000001VqP0qAAF", OpportunityId:"006g000000EsCsOAAV", AccountId:"001g000001hc42ZAAQ" }
    """
    And Clean up all "jobs" for resource "Sked Resource 1, Sked Resource 2, Sked Resource 3, Sked Resource 4"

  @ARC-TC-SM-005
  Scenario Outline: TC-SM-005 - User can unassign resource from Job
    Given Schedule Full Service for training event "training_event" with
      | TrainingEvent | training_event  |
      | Region        | Any             |
      | CourseCatalog | course_catalog  |
      | Address       | California, USA |

    And Select job slots for training event "training_event" with "Save & Allocate"
      | job_1 | Tomorrow at 08:00     |
      | job_2 | 2 Days Later at 12:00 |
      | job_3 | 3 Days Later at 16:00 |

    And Allocate resources for training event "training_event" by "Schedule Full Service" with notification "true"
      | All   | Sked Resource 1 |
      | job_1 | Sked Resource 2 |
      | job_2 | Sked Resource 3 |
      | job_3 | Sked Resource 4 |

    And Unassign resources from training event "training_event" by "Manage Resource" and "Save" with notification "<notification_status>"
      | job_1 | Sked Resource 1, Sked Resource 2 |
      | job_2 | Sked Resource 3                  |

    Then Jobs are created correctly for training event "training_event"

    Examples:
      | notification_status |
      | true                |
      | false               |