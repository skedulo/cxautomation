@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU007
Feature: TF007 - Filter Job and Shift on Roster Management

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"

  Scenario: TC-SM-010 - Search Job on Roster Management based on filters
    Given Create "job" job from Roster Management with:
    """
      {
        "Urgency": "Urgent",
        "Description": "TC-SM-007 - Create Single Job from Roster Management"
      }
    """
    And Clean up all "shifts" for resource "Test Resource 1"
    And Create "shift" shift for resource "Test Resource 1" from Roster Management
