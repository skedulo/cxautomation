@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU003
Feature: TF003 - Create Group Event Job

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And I have a group event "group_event" with:
    """
    {
        "GroupName": "GroupEvent",
        "Status": "Active",
        "Region": "Brisbane",
        "AddressType": "Address",
        "Address": "Testers Hollow, Gillieston Heights NSW, Australia",
        "Description": "Description for group event which is created through Salesforce",
        "Billable": "Billable",
        "MaximumAttendees": 1,
        "MaximumResources": 1,
        "Asset": 0,
        "Client": "Default"
    }
    """

  Scenario: TC-SM-002 - Create Group Event Job and dispatch through Webapp
    Given I create job "job" under group "group_event" with:
    """
      {
        "StartDate": "Next Thursday",
        "StartTime": "14:00",
        "Duration": 60,
        "Urgency": "Urgent",
        "Description": "TC-SM-002 - Create Group Event Job and dispatch through Webapp"
      }
    """
    And Go to Job Details Page
    Then "job" job should populate correct information in Webapp
    And Allocate resource "Test Resource 1" for job "job" through Webapp
    And Dispatch resource "Test Resource 1" for job "job" through Webapp
    Then Resource "Test Resource 1" should be "Dispatched" in Webapp

  Scenario: TC-SM-004 - Create Group Event Job and dispatch through SF
    Given I create job "job" under group "group_event" with:
    """
      {
        "StartDate": "Next Wednesday",
        "StartTime": "15:00",
        "Duration": 90,
        "Urgency": "Critical",
        "Description": "TC-SM-004 - Create Group Event Job and dispatch through SF"
      }
    """
    And Allocate resource "Test Resource 2" for group event job "job" through Salesforce
    And Go to Job Details Page
    Then "job" job should populate correct information in Webapp
    Then Resource "Test Resource 2" should be "Dispatched" in Webapp

