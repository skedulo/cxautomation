@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU001
Feature: TF001 - Create a Single Job from Saleforces

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"

  Scenario: TC-SM-001 - Create Single Job from Contact and dispatch through Webapp
    Given Creates "job" job through Salesforce with:
    """
      {
        "StartDate": "Next Friday",
        "StartTime": "09:00",
        "Duration": 120,
        "Urgency": "Normal",
        "Description": "TC-SM-001 - Create Single Job from Contact and dispatch through Webapp",
        "Region":"Brisbane"
      }
    """
    And Go to Job Details Page
    Then "job" job should populate correct information in Webapp
    And Allocate resource "Test Resource 1" for job "job" through Webapp
    And Dispatch resource "Test Resource 1" for job "job" through Webapp
    Then Resource "Test Resource 1" should be "Dispatched" in Webapp

  Scenario: TC-SM-003 - Create Single Job from Contact and dispatch through SF
    Given Creates "job" job through Salesforce with:
    """
      {
        "StartDate": "Next Friday",
        "StartTime": "09:00",
        "Duration": 120,
        "Urgency": "Normal",
        "Description": "TC-SM-001 - Create Single Job from Contact and dispatch through Webapp",
        "Region":"Brisbane"
      }
    """
    And Allocate resource "Test Resource 2" for single job "job" through Salesforce
    And Go to Job Details Page
    Then "job" job should populate correct information in Webapp
    Then Resource "Test Resource 2" should be "Dispatched" in Webapp