@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU006
Feature: TF006 - Create Single Job and Shift from Roster Management

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"

  Scenario: TC-SM-007 - Create Single Job from Roster Management
    Given Create "job" job from Roster Management with:
    """
      {
        "Urgency": "Urgent",
        "Description": "TC-SM-007 - Create Single Job from Roster Management"
      }
    """
    And Go to Job Details Page
    Then "job" job should populate correct information in Webapp

  @AUHC-TC-SM-009
  Scenario: TC-SM-009 - Create Shift from Roster Management Scheduler
    Given Clean up all "shifts" for resource "Test Resource 1"
    And Create "shift" shift for resource "Test Resource 1" from Roster Management
    And Go to shift details page "shift"
    Then "shift" shift should populate correct information in Salesforce
    And Clean up all "shifts" for resource "Test Resource 1"

  Scenario: TC-SM-008 - Create Group Event from Roster Management
    Given Create a group event "group_event" from Roster Management with:
    """
    {
        "GroupName": "GroupEvent",
        "Status": "Active",
        "Region": "Brisbane",
        "AddressType": "Address",
        "Address": "Testers Hollow, Gillieston Heights NSW, Australia",
        "Description": "TC-SM-008 - Create Group Event from Roster Management",
        "Billable": "Billable",
        "MaximumAttendees": 1,
        "MaximumResources": 1,
        "Asset": 0,
        "Client": "Default"
    }
    """
    And Open "group_event" group through Salesforce
    Then "group_event" group event should populate correct information