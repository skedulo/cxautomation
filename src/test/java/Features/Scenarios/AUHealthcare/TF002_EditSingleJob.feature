@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU002
Feature: TF002 - Edit a Single Job from Saleforces

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"

  Scenario: TC-SM-005 - Edit Single Job from Job record through Salesforce
    Given Creates "job_to_edit" job through Salesforce with:
    """
      {
        "StartDate": "Next Monday",
        "StartTime": "10:00",
        "Duration": 60,
        "Urgency": "Normal",
        "Description": "Test create job through Salesforce",
        "Region":"Brisbane"
      }
    """
    And Open "job_to_edit" job through Salesforce
    Then "job_to_edit" job should populate correct information in Salesforce
    And Edit "job_to_edit" job information through Salesforce with:
    """
      {
        "StartDate": "Next Tuesday",
        "StartTime": "12:00",
        "Duration": 90,
        "Urgency": "Urgent",
        "Description": "Update job through Salesforce"
      }
    """
    And Open "job_to_edit" job through Salesforce
    Then "job_to_edit" job should populate correct information in Salesforce