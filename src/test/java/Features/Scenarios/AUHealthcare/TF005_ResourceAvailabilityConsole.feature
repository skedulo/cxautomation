@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU005
Feature: TF005 - Create Availability and Activity on RAC

  Scenario Outline: TC-SM-014 - Create Availability on RAC
    Given I logged into Salesforce as "Admin"
    And Clean up all "availabilities" for resource "Test Resource 5"
    And Create "availability" "a" for "Test Resource 5" as following
      | SubType           | <AvailableType>     |
      | StartDate         | <StartDate>         |
      | StartTime         | <StartTime>         |
      | EndTime           | <EndTime>           |
      | IsRecurring       | <IsRecurring>       |
      | IsSkippedWeekend  | <IsSkippedWeekend>  |
      | IsSkippedHolidays | <IsSkippedHolidays> |
      | Pattern           | <Pattern>           |
      | DaysOfWeek        | <DaysOfWeek>        |
      | RecurFor          | <RecurFor>          |

    Then All recurrences for "availability" "a" "Test Resource 5" are listed correctly
    And Clean up all "availabilities" for resource "Test Resource 5"

    Examples:
      | AvailableType | StartDate     | StartTime | EndTime | IsRecurring | IsSkippedWeekend | IsSkippedHolidays | Pattern | DaysOfWeek  | RecurFor |
      | Available     | Next Tuesday  | 15:00     | 16:00   | No          |                  |                   |         |             |          |
      | Available     | Next Friday   | 15:00     | 16:00   | Yes         | No               | No                | Weekly  | Mon,Wed     | 2 Weeks  |
      | Jury Service  | Next Monday   | 15:00     | 16:00   | Yes         | No               | No                | 2 Weeks | Mon,Wed;Tue | 5 Weeks  |
      | Available     | Next Friday   | 15:00     | 16:00   | Yes         | No               | No                | 2 Weeks | Mon,Wed;Fri | 2 Weeks  |
      | Available     | Next Saturday | 15:00     | 16:00   | Yes         | No               | No                | Weekly  | Sun         | 2 Weeks  |

  Scenario Outline: TC-SM-015 - Create Activity on RAC
    Given I logged into Salesforce as "Admin"
    And Clean up all "activities" for resource "Test Resource 5"
    And Create "activity" "a" for "Test Resource 5" as following
      | SubType           | <ActivityType>      |
      | StartDate         | <StartDate>         |
      | StartTime         | <StartTime>         |
      | EndTime           | <EndTime>           |
      | IsRecurring       | <IsRecurring>       |
      | IsSkippedWeekend  | <IsSkippedWeekend>  |
      | IsSkippedHolidays | <IsSkippedHolidays> |
      | Pattern           | <Pattern>           |
      | DaysOfWeek        | <DaysOfWeek>        |
      | RecurFor          | <RecurFor>          |
      | Address           | <Address>           |

    Then All recurrences for "activity" "a" "Test Resource 5" are listed correctly
    And Clean up all "activities" for resource "Test Resource 5"

    Examples:
      | ActivityType   | StartDate    | StartTime | EndTime | IsRecurring | IsSkippedWeekend | IsSkippedHolidays | Pattern | DaysOfWeek  | RecurFor | Address  |
      | Meal Break     | Next Tuesday | 15:00     | 16:00   | No          |                  |                   |         |             |          | Anywhere |
      | Administration | Next Monday  | 15:00     | 16:00   | Yes         | No               | No                | Weekly  | Mon,Wed     | 2 Weeks  | Anywhere |
      | Travel         | Next Monday  | 15:00     | 16:00   | Yes         | No               | No                | 2 Weeks | Mon,Wed;Tue | 5 Weeks  | Anywhere |