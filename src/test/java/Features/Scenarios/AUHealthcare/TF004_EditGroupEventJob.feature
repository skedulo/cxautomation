@Admin @Healthcare @SFDC @SmokeTest @CleanUp @AU004
Feature: TF004 - Edit Group Event Job

  Background: Admin user is logged into Salesforce
    Given I logged into Salesforce as "Admin"
    And I have a group event "group_event" with:
    """
    {
        "GroupName": "GroupEvent",
        "Status": "Active",
        "Region": "Brisbane",
        "AddressType": "Address",
        "Address": "Testers Hollow, Gillieston Heights NSW, Australia",
        "Description": "Edit Group Event Job",
        "Billable": "Billable",
        "MaximumAttendees": 1,
        "MaximumResources": 1,
        "Asset": 0,
        "Client": "Default"
    }
    """

  Scenario: TC-SM-006 - Edit Group event Job from Job record
    Given I create job "job" under group "group_event" with:
    """
      {
        "StartDate": "Next Thursday",
        "StartTime": "14:00",
        "Duration": 60,
        "Urgency": "Urgent",
        "Description": "TC-SM-006 - Edit Group event Job from Job record"
      }
    """
    And Open "group_event" group through Salesforce
    And Open job "job" in group event "group_event"
    Then "job" job should populate correct information in Group event
    And Edit job "job" under group "group_event" with:
    """
       {
          "StartDate": "Next Friday",
          "StartTime": "13:00",
          "Duration": 90,
          "Urgency": "Normal",
          "Description": "TC-SM-006 - Edit Group event Job from Job record - Updated",
          "Resource": "Test Resource 3"
        }
    """
    And Go to Job Details Page
    Then "job" job should populate correct information in Webapp
    Then Resource "Test Resource 3" should be "Dispatched" in Webapp

#    And Open "group_event" group through Salesforce
#    And Open job "job" in group event "group_event"
#    Then "job" job should populate correct information in Group event
#    Then Resource "Test Resource 3" should be "Dispatched" in Group Event