package Features.StepDefinitions.ARCSteps;

import Base.LogHelper;
import Pages.Salesforce.ARC.SfAllocateResourcesPage;
import Pages.Salesforce.ARC.SfCommunitySchedulingPage;
import Pages.Salesforce.ARC.SfScheduleCommunityJobPage;
import Pages.Salesforce.ARC.SfScheduleNATJobsPage;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static Base.BaseUtil.*;

public class CommunityJobSteps {
    @Given("^Schedule community job \"([^\"]*)\" with \"([^\"]*)\"$")
    public void scheduleCommunityJob(String communityAlias, String followingAction, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), String.format("Schedule community job"));
        LogHelper.getLogger().info(String.format("Schedule community job"));
        new SfScheduleCommunityJobPage(driver).scheduleCommunityJob(communityAlias, followingAction, data, "Community");
    }

    @Then("^Jobs are created correctly for community job \"([^\"]*)\"$")
    public void jobsAreCreatedCorrectlyForCommunityJob(String communityJobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Jobs are created correctly for community job"));
        LogHelper.getLogger().info(String.format("Jobs are created correctly for community job"));
        new SfCommunitySchedulingPage(driver).checkJobsInfoForCommunityJobs(communityJobAlias);
    }

    @And("^Allocate resources for community job \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void allocateResourcesForCommunityJobWithNotification(String communityAlias, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Allocate resources for community job"));
        LogHelper.getLogger().info(String.format("Allocate resources for community job"));
        new SfAllocateResourcesPage(driver).allocateResourcesForCommunityJobs(communityAlias, "default", notificationStatus, data);
    }

    @And("^Allocate resources for community job \"([^\"]*)\" by \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void allocateResourcesForCommunityJobByWithNotification(String communityAlias, String allocationMode, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Allocate resources for community job"));
        LogHelper.getLogger().info(String.format("Allocate resources for community job"));
        new SfAllocateResourcesPage(driver).allocateResourcesForCommunityJobs(communityAlias, allocationMode, notificationStatus, data);
    }

    @And("^Unassign resources from community job \"([^\"]*)\" by \"([^\"]*)\" and \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void unassignResourcesFromCommunityJobWithNotification(String communityAlias, String allocationMode, String action, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Unassign resources from community job with notification"));
        LogHelper.getLogger().info(String.format("Unassign resources from community job with notification"));
        new SfAllocateResourcesPage(driver).unassignResourcesForCommunityJob(communityAlias, allocationMode, action, notificationStatus, data);
    }

    @And("^Unassign resources from community job \"([^\"]*)\" and \"([^\"]*)\"$")
    public void unassignResourcesFromCommunityJob(String communityAlias, String action, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Unassign resources from community job"));
        LogHelper.getLogger().info(String.format("Unassign resources from community job"));
        new SfAllocateResourcesPage(driver).unassignResourcesForCommunityJob(communityAlias, null, action, null, data);
    }

    @And("^Reschedule jobs for community job \"([^\"]*)\" by \"([^\"]*)\" with \"([^\"]*)\"$")
    public void rescheduleJobsForCommunityJobByWith(String communityAlias, String mode, String action, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Reschedule jobs for community job by Edit Job"));
        LogHelper.getLogger().info(String.format("Reschedule jobs for community job by Edit Job"));
        SfScheduleCommunityJobPage scheduleCommunityJobPage = new SfScheduleCommunityJobPage(driver);
        if ("Edit Job".equals(mode))
            scheduleCommunityJobPage.rescheduleJobByEditJob(communityAlias, action, data);
    }

    @And("^Reschedule jobs for community job \"([^\"]*)\" by \"([^\"]*)\"$")
    public void rescheduleJobsForCommunityJobBy(String communityAlias, String mode, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Reschedule jobs for community job"));
        LogHelper.getLogger().info(String.format("Reschedule jobs for community job"));
        SfCommunitySchedulingPage communitySchedulingPage = new SfCommunitySchedulingPage(driver);
        if ("Drag and Drop".equals(mode))
            communitySchedulingPage.rescheduleJobByDragAndDrop(communityAlias, data);
    }

    @And("^Cancel jobs \"([^\"]*)\" for community job \"([^\"]*)\"$")
    public void cancelJobsForCommunityJob(String jobs, String communityAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Cancel jobs for community job"));
        LogHelper.getLogger().info(String.format("Cancel jobs for community job"));
        new SfCommunitySchedulingPage(driver).cancelJobs(communityAlias, jobs);
    }

    @And("^Delete jobs \"([^\"]*)\" for community job \"([^\"]*)\"$")
    public void deleteJobsForCommunityJob(String jobs, String communityAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Delete jobs for community job"));
        LogHelper.getLogger().info(String.format("Delete jobs for community job"));
        new SfCommunitySchedulingPage(driver).deleteJobs(communityAlias, jobs);
    }

    @Given("^Schedule CEP job \"([^\"]*)\" with \"([^\"]*)\"$")
    public void scheduleCEPJobWith(String cepJobAlias, String action, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), String.format("Schedule CEP job"));
        LogHelper.getLogger().info(String.format("Schedule CEP job"));
        new SfScheduleCommunityJobPage(driver).scheduleCommunityJob(cepJobAlias, action, data, "CEP");
    }

    @And("^Allocate resources for cep job \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void allocateResourcesForCepJobWithNotification(String cepJobAlias, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Allocate resources for cep job with notification"));
        LogHelper.getLogger().info(String.format("Allocate resources for cep job with notification"));
        new SfAllocateResourcesPage(driver).allocateResourcesForCommunityJobs(cepJobAlias, "default", notificationStatus, data);
    }

    @Then("^Jobs are created correctly for CEP job \"([^\"]*)\"$")
    public void jobsAreCreatedCorrectlyForCepJob(String cepJobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Jobs are created correctly for CEP job"));
        LogHelper.getLogger().info(String.format("Jobs are created correctly for CEP job"));
        new SfCommunitySchedulingPage(driver).checkJobsInfoForCommunityJobs(cepJobAlias);
    }

    @And("^Allocate resources for cep job \"([^\"]*)\" by \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void allocateResourcesForCepJobByWithNotification(String cepJobAlias, String mode, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Allocate resources for cep job by Allocate Resources"));
        LogHelper.getLogger().info(String.format("Allocate resources for cep job by Allocate Resources"));
        new SfAllocateResourcesPage(driver).allocateResourcesForCommunityJobs(cepJobAlias, mode, notificationStatus, data);
    }

    @And("^Unassign resources from CEP job \"([^\"]*)\" by \"([^\"]*)\" and \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void unassignResourcesFromCEPJobByAndWithNotification(String cepJobAlias, String mode, String action, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Unassign resources from cep jobs"));
        LogHelper.getLogger().info(String.format("Unassign resources from cep jobs"));
        new SfAllocateResourcesPage(driver).unassignResourcesForCommunityJob(cepJobAlias, mode, action, notificationStatus, data);
    }

    @And("^Reschedule jobs for CEP job \"([^\"]*)\" by \"([^\"]*)\" with \"([^\"]*)\"$")
    public void rescheduleJobsForCEPJobByWith(String cepAlias, String mode, String action, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Reschedule jobs for CEP job by Edit Job"));
        LogHelper.getLogger().info(String.format("Reschedule jobs for CEP job by Edit Job"));
        SfScheduleCommunityJobPage scheduleCommunityJobPage = new SfScheduleCommunityJobPage(driver);
        if ("Edit Job".equals(mode))
            scheduleCommunityJobPage.rescheduleJobByEditJob(cepAlias, action, data);
    }

    @And("^Reschedule jobs for CEP job \"([^\"]*)\" by \"([^\"]*)\"$")
    public void rescheduleJobsForCEPJobBy(String cepAlias, String mode, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Reschedule jobs for CEP job"));
        LogHelper.getLogger().info(String.format("Reschedule jobs for CEP job"));
        SfCommunitySchedulingPage communitySchedulingPage = new SfCommunitySchedulingPage(driver);
        if ("Drag and Drop".equals(mode))
            communitySchedulingPage.rescheduleJobByDragAndDrop(cepAlias, data);
    }

    @And("^Cancel jobs \"([^\"]*)\" for CEP job \"([^\"]*)\"$")
    public void cancelJobsForCEPJob(String jobs, String cepAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Cancel jobs for CEP job"));
        LogHelper.getLogger().info(String.format("Cancel jobs for CEP job"));
        new SfCommunitySchedulingPage(driver).cancelJobs(cepAlias, jobs);
    }

    @And("^Delete jobs \"([^\"]*)\" for CEP job \"([^\"]*)\"$")
    public void deleteJobsForCEPJob(String jobs, String cepAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Delete jobs for CEP job"));
        LogHelper.getLogger().info(String.format("Delete jobs for CEP job"));
        new SfCommunitySchedulingPage(driver).deleteJobs(cepAlias, jobs);
    }

    @Given("^Schedule NAT job \"([^\"]*)\" with \"([^\"]*)\"$")
    public void scheduleNATJobWith(String natAlias, String action, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), String.format("Schedule NAT job"));
        LogHelper.getLogger().info(String.format("Schedule NAT job"));
        new SfScheduleNATJobsPage(driver).scheduleNATJob(natAlias, action, data);
    }

    @And("^Allocate resources for nat job \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void allocateResourcesForNatJobWithNotification(String natAlias, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Allocate resources for NAT job with notification"));
        LogHelper.getLogger().info(String.format("Allocate resources for NAT job with notification"));
        new SfAllocateResourcesPage(driver).allocateResourcesForNATJobs(natAlias, "default", notificationStatus, data);
    }

    @And("^Jobs are created correctly for NAT job \"([^\"]*)\"$")
    public void jobsAreCreatedCorrectlyForNATJob(String natAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Jobs are created correctly for NAT jobs"));
        LogHelper.getLogger().info(String.format("Jobs are created correctly for NAT jobs"));
        new SfCommunitySchedulingPage(driver).checkJobsInfoForNATJobs(natAlias);
    }
}
