package Features.StepDefinitions.ARCSteps;

import Base.LogHelper;
import Pages.PageManager;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static Base.BaseUtil.*;

public class FullServiceSteps {
    PageManager pageManager = new PageManager(driver);

    @Given("^Schedule Full Service for training event \"([^\"]*)\" with$")
    public void scheduleFullServiceTrainingEventWith(String trainingEventAlias, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), String.format("Schedule Full Service for training event with"));
        LogHelper.getLogger().info(String.format("Schedule Full Service for training event with"));
        pageManager.getSfScheduleFullServicePage().scheduleFullService(trainingEventAlias, data);
    }

    @Then("^\"([^\"]*)\" course catalog should populated correctly in Schedule Full Service model$")
    public void courseCatalogShouldPopulatedCorrectlyInScheduleFullServiceModel(String courseCatalogAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Course catalog should populated correctly in Schedule Full Service model"));
        LogHelper.getLogger().info(String.format("Course catalog should populated correctly in Schedule Full Service model"));
        pageManager.getSfScheduleFullServicePage().checkCourseCatalogInfo(courseCatalogAlias);
    }

    @And("^Select job slots for training event \"([^\"]*)\" with \"([^\"]*)\"$")
    public void selectJobSlotsWith(String trainingEventAlias, String button, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Select job slots for training event"));
        LogHelper.getLogger().info(String.format("Select job slots for training event"));
        pageManager.getSfScheduleFullServicePage().selectJobSlots(trainingEventAlias, "default", button, data);
    }

    @And("^Select job slots for training event \"([^\"]*)\" with multi days \"([^\"]*)\" and \"([^\"]*)\"$")
    public void selectJobSlotsWithMultiDays(String trainingEventAlias, String multiDays, String button, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Select job slots for training event with multi days"));
        LogHelper.getLogger().info(String.format("Select job slots for training event with multi days"));
        pageManager.getSfScheduleFullServicePage().selectJobSlots(trainingEventAlias, multiDays, button, data);
    }

    @And("^Allocate resources for training event \"([^\"]*)\" by \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void allocateResourcesForTrainingEventWithNotification(String trainingEventAlias, String allocationMode, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Allocate resources for training event with notification %s", notificationStatus));
        LogHelper.getLogger().info(String.format("Allocate resources for training event with notification %s", notificationStatus));
        pageManager.getSfAllocateResourcesPage().allocateResources(trainingEventAlias, allocationMode, notificationStatus, data);
    }

    @Then("^Jobs are created correctly for training event \"([^\"]*)\"$")
    public void jobsAreCreatedCorrectlyForTrainingEvent(String trainingEventAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Jobs are created correctly for training event"));
        LogHelper.getLogger().info(String.format("Jobs are created correctly for training event"));
        pageManager.getSfTrainingEventPage().checkJobsInfo(trainingEventAlias);
    }

    @And("^Unassign resources from training event \"([^\"]*)\" by \"([^\"]*)\" and \"([^\"]*)\" with notification \"([^\"]*)\"$")
    public void unassignResourcesFromTrainingEventWithNotification(String trainingEventAlias, String allocationMode, String followingAction, String notificationStatus, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Unassign resources from training event with notification %s", notificationStatus));
        LogHelper.getLogger().info(String.format("Unassign resources from training event with notification %s", notificationStatus));
        pageManager.getSfAllocateResourcesPage().unassignResources(trainingEventAlias, allocationMode, followingAction, notificationStatus, data);
    }

    @And("^Unassign resources from training event \"([^\"]*)\" by \"([^\"]*)\" and \"([^\"]*)\"$")
    public void unassignResourcesFromTrainingEvent(String trainingEventAlias, String allocationMode, String followingAction, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Unassign resources from training event"));
        LogHelper.getLogger().info(String.format("Unassign resources from training event"));
        pageManager.getSfAllocateResourcesPage().unassignResources(trainingEventAlias, allocationMode, followingAction, "Any", data);
    }

    @And("^Reschedule jobs for training event \"([^\"]*)\" with \"([^\"]*)\"$")
    public void rescheduleJobsForTrainingEventWith(String trainingEventAlias, String button, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Reschedule jobs for training event"));
        LogHelper.getLogger().info(String.format("Reschedule jobs for training event"));
        pageManager.getSfScheduleFullServicePage().rescheduleJobs(trainingEventAlias, button, data);
    }

    @And("^Cancel jobs for training event \"([^\"]*)\" by \"([^\"]*)\"$")
    public void cancelJobsForTrainingEventBy(String trainingEventAlias, String mode) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Reschedule jobs for training event"));
        LogHelper.getLogger().info(String.format("Cancel jobs for training event"));
        pageManager.getSfTrainingEventPage().cancelJobs(trainingEventAlias, mode);
    }
}
