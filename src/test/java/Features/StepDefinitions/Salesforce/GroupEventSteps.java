package Features.StepDefinitions.Salesforce;

import Base.LogHelper;
import Pages.PageManager;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static Base.BaseUtil.*;

public class GroupEventSteps {
    PageManager pageManager = new PageManager(driver);

    @And("^I have a group event \"([^\"]*)\" with:$")
    public void iHaveAGroupEventWith(String groupAlias, String params) throws ClassNotFoundException, InterruptedException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "I have a group event with:");
        LogHelper.getLogger().info(String.format("I have a group event with:: %s", params));
        pageManager.getSfNewGroupEventPage().createGroupEvent(groupAlias, params);
    }

    @Given("^I create job \"([^\"]*)\" under group \"([^\"]*)\" with:$")
    public void iCreateJobUnderGroupWith(String jobAlias, String groupAlias, String params) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "I create job under group with:");
        LogHelper.getLogger().info(String.format("I create job under group with:: %s", params));
        pageManager.getSfNewGroupEventPage().createJobUnderGroup(jobAlias, groupAlias, params);
    }

    @And("^Allocate resource \"([^\"]*)\" for group event job \"([^\"]*)\" through Salesforce$")
    public void allocateResourceForGroupEventJobThroughSalesforce(String resource, String jobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Allocate resource for group event job through Salesforce");
        LogHelper.getLogger().info(String.format("Allocate resource %s for group event job through Salesforce", resource));
        pageManager.getSfGroupEventScheduleJobPage().allocateResource(resource, jobAlias);
    }

    @And("^Open \"([^\"]*)\" group through Salesforce$")
    public void openGroupThroughSalesforce(String groupAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Open group through Salesforce");
        LogHelper.getLogger().info(String.format("Open group %s through Salesforce", groupAlias));
        pageManager.getSfGroupEventPage().openGroupEventDetails(groupAlias);
    }

    @And("^Open job \"([^\"]*)\" in group event \"([^\"]*)\"$")
    public void openJobInGroupEvent(String jobAlias, String groupAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Open job in group event through Salesforce");
        LogHelper.getLogger().info(String.format("Open job %s in group event %s through Salesforce", jobAlias, groupAlias));
        pageManager.getSfGroupEventPage().openJob(jobAlias, groupAlias);
    }

    @Then("^\"([^\"]*)\" job should populate correct information in Group event$")
    public void jobShouldPopulateCorrectInformationInGroupEvent(String jobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "Job should populate correct information in Group event");
        LogHelper.getLogger().info(String.format("%s job should populate correct information in Group event", jobAlias));
        pageManager.getSfGroupEventScheduleJobPage().checkJobDetails(jobAlias);
    }

    @And("^Edit job \"([^\"]*)\" under group \"([^\"]*)\" with:$")
    public void editJobUnderGroupWith(String jobAlias, String groupAlias, String params) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Edit job under group event with:");
        LogHelper.getLogger().info(String.format("Edit job %s under group event with: %s", jobAlias, params));
        pageManager.getSfGroupEventScheduleJobPage().editJob(jobAlias, params);
    }

    @Then("^Resource \"([^\"]*)\" should be \"([^\"]*)\" in Group Event$")
    public void resourceShouldBeInGroupEvent(String resource, String status) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Resource %s should be %s in Group Event", resource, status));
        LogHelper.getLogger().info(String.format("Resource %s should be %s in Group Event", resource, status));
        pageManager.getSfGroupEventScheduleJobPage().checkResourceStatus(resource, status);
    }

    @Then("^\"([^\"]*)\" group event should populate correct information$")
    public void groupEventShouldPopulateCorrectInformation(String groupAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Group event should populate correct information"));
        LogHelper.getLogger().info(String.format("Group event should populate correct information"));
        pageManager.getSfNewGroupEventPage().checkGroupEventDetails(groupAlias);
    }

    @And("^Create a group event \"([^\"]*)\" from Roster Management with:$")
    public void createAGroupEventFromRosterManagementWith(String groupAlias, String params) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Create a group event from Roster Management with:"));
        LogHelper.getLogger().info(String.format("Create a group event %s from Roster Management with: %s", groupAlias, params));
        pageManager.getSfRosterManagement().createGroupEvent(groupAlias, params);
    }
}
