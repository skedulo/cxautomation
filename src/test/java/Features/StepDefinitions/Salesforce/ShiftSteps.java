package Features.StepDefinitions.Salesforce;

import Base.LogHelper;
import Pages.PageManager;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static Base.BaseUtil.*;

public class ShiftSteps {
    PageManager pageManager = new PageManager(driver);

    @Given("^Create \"([^\"]*)\" shift for resource \"([^\"]*)\" from Roster Management$")
    public void createShiftFromRosterManagement(String shiftAlias, String resource) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), String.format("Create shift for resource from Roster Management"));
        LogHelper.getLogger().info(String.format("Create shift for resource from Roster Management"));
        pageManager.getSfRosterManagement().createShift(shiftAlias, resource);
    }

    @And("^Go to shift details page \"([^\"]*)\"$")
    public void goToShiftDetailsPage(String shiftAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Go to shift details page"));
        LogHelper.getLogger().info(String.format("Go to shift details page"));
        pageManager.getSfShiftDetailsPage().goToShiftDetailsPage(shiftAlias);
    }

    @Then("^\"([^\"]*)\" shift should populate correct information in Salesforce$")
    public void shiftShouldPopulateCorrectInformationInSalesforce(String shiftAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Shift should populate correct information in Salesforce"));
        LogHelper.getLogger().info(String.format("Shift should populate correct information in Salesforce"));
        pageManager.getSfShiftDetailsPage().checkShiftDetails(shiftAlias);
    }
}
