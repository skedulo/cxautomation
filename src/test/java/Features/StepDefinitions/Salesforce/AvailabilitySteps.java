package Features.StepDefinitions.Salesforce;

import Base.LogHelper;
import Pages.PageManager;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.lang.reflect.Array;

import static Base.BaseUtil.*;
import static Utilities.GenericFunctions.cleanUpResourceRelatedObjects;

public class AvailabilitySteps {
    PageManager pageManager = new PageManager(driver);

    @And("^Create \"([^\"]*)\" \"([^\"]*)\" for \"([^\"]*)\" as following$")
    public void createAvailabilityForAsFollowing(String type, String availableAlias, String resource, DataTable data) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Create %s for %s as following", type, resource));
        LogHelper.getLogger().info(String.format("Create %s for %s as following", type, resource));
        pageManager.getSfCreateAvailabilityPage().createAvailability(type, availableAlias, resource, data);
    }

    @Then("^All recurrences for \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" are listed correctly$")
    public void allRecurrencesForAvailabilityAreListedCorrectly(String type, String availabilityAlias, String resource) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("All recurrences for %s are listed correctly", type));
        LogHelper.getLogger().info(String.format("All recurrences for %s are listed correctly", type));
        pageManager.getSfResourceAvailabilitiesPage().checkAvailabilities(availabilityAlias, resource);
    }

    @And("^Clean up all \"([^\"]*)\" for resource \"([^\"]*)\"$")
    public void cleanUpAllForResource(String type, String resource) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Clean up all %s for resource %s", type, resource));
        LogHelper.getLogger().info(String.format("Clean up all %s for resource %s", type, resource));
        String[] resourceArray = resource.split(",");
        for (String resourceName : resourceArray)
            cleanUpResourceRelatedObjects(type, resourceName.trim());
    }
}
