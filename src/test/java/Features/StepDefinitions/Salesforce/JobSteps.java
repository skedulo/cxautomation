package Features.StepDefinitions.Salesforce;

import Base.LogHelper;
import Objects.Job;
import Pages.PageManager;
import Pages.Salesforce.SfContactDetailPage;
import Pages.Salesforce.SfScheduleJobPage;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;

import java.util.ArrayList;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.sleep;

public class JobSteps {
    PageManager pageManager = new PageManager(driver);
    SfContactDetailPage contactDetailPage = pageManager.getSfContactDetailPage();
    SfScheduleJobPage scheduleJobPage = pageManager.getSfScheduleJobPage();

    @Given("^Creates \"([^\"]*)\" job through Salesforce with:$")
    public void createsJobThroughSalesforceWith(String jobAlias, String variables) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), "Creates job through Salesforce with:");
        LogHelper.getLogger().info(String.format("Creates job through Salesforce with: %s", variables));
        Job job = new Job(variables);
        job.setAddress("23 Ney Road, Capalaba, QLD 4157, Australia"); //Todo: Handle default contact address

        // Open default contact
        driver.navigate().to(String.format("%s/lightning/r/Contact/%s/view", userContext.getLoginResource(),job.getContactID()));
        LogHelper.getLogger().info("Navigate to contact details page");

        // Click Schedule Job button
        contactDetailPage.clickScheduleJob();

        // Schedule Job
        scheduleJobPage.scheduleJob(job);

        scenarioContext.setValue(jobAlias, job);
    }

    @And("^Allocate resource \"([^\"]*)\" for single job \"([^\"]*)\" through Salesforce$")
    public void allocateResourceForSingleJobThroughSalesforce(String resource, String jobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Allocate resource for single job through Salesforce");
        LogHelper.getLogger().info(String.format("Allocate resource %s for group event job through Salesforce", resource));
        pageManager.getSfScheduleJobPage().allocateResource(resource, jobAlias);
    }

    @Given("^Create \"([^\"]*)\" job from Roster Management with:$")
    public void createJobFromRosterManagementWith(String jobAlias, String variables) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), "Creates job from Roster Management with:");
        LogHelper.getLogger().info(String.format("Creates job from Roster Management with: %s", variables));
        pageManager.getSfRosterManagement().scheduleJob(jobAlias, variables);
    }

    @And("^Go to Job Details Page$")
    public void goToJobDetailsPage() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Go to Job Details Page");
        LogHelper.getLogger().info("Go to Job Details Page");
        scheduleJobPage.goToJobDetailsPage();
    }
}
