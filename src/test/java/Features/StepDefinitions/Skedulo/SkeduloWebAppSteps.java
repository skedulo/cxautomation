package Features.StepDefinitions.Skedulo;

import Base.LogHelper;
import Pages.PageManager;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

import java.text.ParseException;

import static Base.BaseUtil.*;

public class SkeduloWebAppSteps {
    PageManager pageManager = new PageManager(driver);

    @Then("^\"([^\"]*)\" job should populate correct information in Webapp$")
    public void jobShouldPopulateCorrectInformationInWebapp(String jobAlias) throws ClassNotFoundException, ParseException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "Job should populate correct information in Webapp");
        LogHelper.getLogger().info("Job should populate correct information in Webapp");
        pageManager.getSkeduloJobDetails().checkJobDetails(jobAlias);
    }

    @And("^Allocate resource \"([^\"]*)\" for job \"([^\"]*)\" through Webapp$")
    public void allocateResourceForJobThroughWebapp(String resource, String jobAlias) throws ClassNotFoundException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "Allocate resource for job through Webapp");
        LogHelper.getLogger().info(String.format("Allocate resource %s for job through Webapp", resource));
        pageManager.getSkeduloJobDetails().allocateResource(resource, jobAlias);
    }

    @And("^Dispatch resource \"([^\"]*)\" for job \"([^\"]*)\" through Webapp$")
    public void dispatchResourceForJobThroughWebapp(String resource, String jobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Dispatch resource for job through Webapp");
        LogHelper.getLogger().info(String.format("Dispatch resource %s for job through Webapp", resource));
        pageManager.getSkeduloJobDetails().dispatchResource(resource, jobAlias);
    }

    @Then("^Resource \"([^\"]*)\" should be \"([^\"]*)\" in Webapp$")
    public void resourceShouldBeInWebapp(String resource, String status) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), String.format("Resource %s should be %s in Webapp", resource, status));
        LogHelper.getLogger().info(String.format("Resource %s should be %s in Webapp", resource, status));
        pageManager.getSkeduloJobDetails().checkResourceStatus(resource, status);
    }
}
