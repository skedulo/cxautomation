package Features.StepDefinitions.SkeduloAPI;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Objects.CourseCatalog;
import Objects.CourseClassType;
import Objects.CourseTemplate;
import Objects.TrainingEvent;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

import static Base.BaseUtil.*;

public class SkeduloAPISteps {

    @And("^Create course catalog \"([^\"]*)\" using SkeduloAPI with:$")
    public void createCourseCatalogUsingSkeduloAPIWith(String courseCatalogAlias, String variables) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Create course catalog using SkeduloAPI with:", variables));
        LogHelper.getLogger().info(String.format("Create course catalog using SkeduloAPI with: %s", variables));
        CourseCatalog courseCatalog = new CourseCatalog(variables);
        courseCatalog.insertCourseCatalogs();
        scenarioContext.setValue(courseCatalogAlias, courseCatalog);
    }

    @And("^Create training event \"([^\"]*)\" using SkeduloAPI with:$")
    public void createTrainingEventUsingSkeduloAPIWith(String trainingEventAlias, String variables) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Create training event using SkeduloAPI with:", variables));
        LogHelper.getLogger().info(String.format("Create training event using SkeduloAPI with: %s", variables));
        TrainingEvent tranTrainingEvent = new TrainingEvent(variables);
        tranTrainingEvent.insertTrainingEvent();
        scenarioContext.setValue(trainingEventAlias, tranTrainingEvent);
    }

    @And("^Create course template \"([^\"]*)\" using SkeduloAPI with:$")
    public void createCourseTemplateUsingSkeduloAPIWith(String courseTemplateAlias, String variables) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Create course template using SkeduloAPI with:", variables));
        LogHelper.getLogger().info(String.format("Create course template using SkeduloAPI with: %s", variables));
        CourseTemplate courseTemplate = new CourseTemplate(variables);
        courseTemplate.insertCourseTemplate();
        scenarioContext.setValue(courseTemplateAlias, courseTemplate);
    }

    @And("^Create course class type \"([^\"]*)\" using SkeduloAPI with:$")
    public void createCourseClassTypeUsingSkeduloAPIWith(String courseClassTypeAlias, String variables) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), String.format("Create course class type using SkeduloAPI with:", variables));
        LogHelper.getLogger().info(String.format("Create course class type using SkeduloAPI with: %s", variables));
        CourseClassType courseClassType = new CourseClassType(variables);
        courseClassType.insertCourseClassType();
        scenarioContext.setValue(courseClassTypeAlias, courseClassType);
    }
}
