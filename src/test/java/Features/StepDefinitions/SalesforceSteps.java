package Features.StepDefinitions;

import Base.ExtentReportUtil;
import Base.LogHelper;
import Pages.PageManager;
import Pages.Salesforce.*;
import Pages.Skedulo.SkeduloJobDetails;
import Objects.Job;
import Objects.User;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.text.ParseException;
import java.util.ArrayList;

import static Base.BaseUIActions.*;
import static Utilities.GenericFunctions.sleep;

public class SalesforceSteps extends ExtentReportUtil {

    ExtentReportUtil extentReportUtil;
    PageManager pageManager = new PageManager(extentReportUtil.driver);
    SfLoginPage loginPage = pageManager.getSfLoginPage();
    SfSetupPage setupPage = pageManager.getSfSetupPage();
    SfBasePage basePage = pageManager.getSfBasePage();
    SfCompanyInfoPage companyInfoPage = pageManager.getSfCompanyInfoPage();
    SfInstalledPackages installedPackages = pageManager.getSfInstalledPackages();
    SfContactDetailPage contactDetailPage = pageManager.getSfContactDetailPage();
    SfScheduleJobPage scheduleJobPage = pageManager.getSfScheduleJobPage();
    SfJobDetailsPage sfJobDetailsPage = pageManager.getSfJobDetailsPage();
    SkeduloJobDetails skeduloJobDetails = pageManager.getSkeduloJobDetails();
    SfEditJobPage editJobPage = pageManager.getSfEditJobPage();

    Job job = new Job();

    public SalesforceSteps(ExtentReportUtil extentReportUtil) {
        this.extentReportUtil = extentReportUtil;
    }

    @Given("^I logged into Salesforce as \"([^\"]*)\"$")
    public void iLoggedIntoSalesforceAsInConfig(String userType) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), "I logged into Salesforce as " + userType);
        userContext = new User(userType);
        initializeBrowserAndLoggedInSalesforce();
    }

    @Given("^I go to page Setup")
    public void iGoToPageSetupPage() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), "I go to page Setup");
        basePage.goToSetupPage();
        // Todo: Handle tabs with title
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        extentReportUtil.driver.switchTo().window(tabs.get(tabs.size() - 1));
    }

    @And("^I go to page \"([^\"]*)\" from Setup")
    public void iGoToPageFromSetup(String page) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "I go to page " + page);
        setupPage.SearchAndGoToPage(page);
    }

    @And("^I logged out Salesforce$")
    public void iLoggedOutSalesforce() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "I logged out Salesforce");
        basePage.logout();
    }

    @Then("^I should see my name in profile panel$")
    public void iShouldSeeMyNameInProfilePanel() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "I should see my name in profile panel as " + userContext.getName());
        String observedName = basePage.getProfileCardName();
        String expectedName = userContext.getName();
        Assert.assertEquals(observedName, expectedName);
    }

    @Then("^I should see my org in Company Info$")
    public void iShouldSeeMyOrgInCompanyInfo() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "I should see my org in Company Info");
        String orgID = companyInfoPage.GetCompanyOrgId();
        Assert.assertEquals(orgID, userContext.getOrgId());
    }

    @Then("^I should see my packages in Installed Packages$")
    public void iShouldSeeMyPackagesInInstalledPackages() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "I should see my packages in Installed Packages");
        boolean isPackageInstalled = installedPackages.IsPackageInstalled(userContext.getSkedPackages());
        Assert.assertTrue(isPackageInstalled);
    }

    @Given("^I open profile panel$")
    public void iOpenProfilePanel() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), "I open profile panel");
        basePage = pageManager.getSfBasePage();
        basePage.openProfilePanel();
    }

    @Then("^I should be in Login page$")
    public void iShouldBeInLoginPage() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "I should be in Login page");
        Assert.assertTrue(loginPage.isAtLoginPage());
    }

    @And("^Open client contact \"([^\"]*)\"$")
    public void openClientContact(String contactName) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Open client contact " + contactName);
        LogHelper.getLogger().info("Open client contact " + contactName);
        basePage.openContact(contactName);
    }

    @And("^Create a job$")
    public void createAJob() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Create a new job");
        LogHelper.getLogger().info("Create a new job");
        contactDetailPage.clickScheduleJob();
        scheduleJobPage.scheduleJob(job);
    }

    @And("^Click Select Resources$")
    public void clickSelectResources() throws ClassNotFoundException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Click Select Resources");
        LogHelper.getLogger().info("Click Select Resources");
        click(driver.findElement(By.xpath("//*[text()='Allocate a resource']")));
        click(driver.findElement(By.xpath("//input[@data-sk-name='resource-card-checkbox']")));
        click(driver.findElement(By.xpath("//*[text()='Save']")));
    }

    @And("^Select resource and click Dispatch button$")
    public void selectResourceAndClickDispatchButton() throws ClassNotFoundException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Select resource and click Dispatch button");
        LogHelper.getLogger().info("Select resource and click Dispatch button");
        waitForElement(driver.findElement(By.cssSelector("div[data-sk-name='resource-status']")));
        click(driver.findElement(By.id("dispatch-all")));
        click(driver.findElement(By.xpath("//*[text()='Ok']")));
        waitForElement(driver.findElement(By.cssSelector("div.notification-message")));
    }

    @Then("^Job is created and dispatch successful to resource$")
    public void jobIsCreatedAndDispatchSuccessfulToResource() throws ClassNotFoundException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Job is created and dispatch successful to resource");
        LogHelper.getLogger().info("Job is created and dispatch successful to resource");
        driver.navigate().refresh();
        waitForPageToLoad();
        skeduloJobDetails.checkJobDetails(job);
    }

    @And("^Allocate resource through Salesforce$")
    public void allocateResourceThroughSalesforce() throws ClassNotFoundException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Allocate resource through Salesforce");
        LogHelper.getLogger().info("Allocate resource through Salesforce");
        click(driver.findElement(By.xpath("//*[text()='Allocate Resources']")));
        click(driver.findElement(By.xpath("//*[text()='Refresh Available Resources']")));
        click(driver.findElement(By.cssSelector("li[ng-repeat='resource in $ctrl.availableResources']")));
        click(driver.findElement(By.cssSelector("button[ng-click='$ctrl.allocateResource(resource);$event.stopPropagation();']")));
        click(driver.findElement(By.xpath("//*[text()='Update Job']")));
        click(driver.findElement(By.xpath("//*[text()='Save']")));
    }

    @And("^Open \"([^\"]*)\" job through Salesforce$")
    public void openJobThroughSalesforce(String jobAlias) throws ClassNotFoundException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Open job through Salesforce");
        LogHelper.getLogger().info("Open Job through Salesforce");
        driver.navigate().to(String.format("%s/lightning/r/sked__Job__c/%s/view",
                userContext.getLoginResource(),
                ((Job) scenarioContext.getValue(jobAlias)).getUID()));
    }

    @Then("^\"([^\"]*)\" job should populate correct information in Salesforce$")
    public void jobShouldPopulateCorrectInformationInSalesforce(String jobAlias) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "Job should populate correct information in Salesforce");
        LogHelper.getLogger().info(String.format("%s job should populate correct information in Salesforce", jobAlias));
        sfJobDetailsPage.checkJobInformation(jobAlias);
    }

    @And("^Edit \"([^\"]*)\" job information through Salesforce with:$")
    public void editJobInformationThroughSalesforce(String jobAlias, String params) throws ParseException, ClassNotFoundException, InterruptedException {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "Edit job information through Salesforce with:");
        LogHelper.getLogger().info(String.format("Edit %s job information through Salesforce with: %s", jobAlias, params));
        editJobPage.editJob(jobAlias, params);
    }
}
