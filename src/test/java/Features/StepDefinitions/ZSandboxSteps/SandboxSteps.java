package Features.StepDefinitions.ZSandboxSteps;

import cucumber.api.java.en.Given;
import org.testng.asserts.SoftAssert;

import static Base.BaseUtil.executionContext;
import static Base.BaseUtil.softAssert;

public class SandboxSteps {
    @Given("^Assert \"([^\"]*)\" is \"([^\"]*)\"$")
    public void assertIs(String input_1, String input_2) throws Throwable {
        softAssert = new SoftAssert();
        if (!input_1.equals(input_2))
            if (executionContext.getValue("Run") == null)
                executionContext.setValue("Run", 1);
            else
                input_1 = input_2;

        softAssert.assertEquals(input_1, input_2);
        softAssert.assertAll();
    }
}
