package Features.StepDefinitions;

import Base.BaseUtil;
import Base.LogHelper;
import Pages.Salesforce.SfHomePage;
import Pages.Salesforce.SfLoginPage;
import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.testng.Assert;

import java.util.List;

public class LoginSteps extends BaseUtil {

    private BaseUtil base;

    public LoginSteps(BaseUtil base) {
        this.base = base;
    }

    @Given("^I navigate to SF Login page$")
    public void iNavigateToSFLoginPage() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Given"), "I navigate to SF Login page");

        LogHelper.getLogger().info("I navigate to SF Login page");
        base.driver.navigate().to("https://login.salesforce.com");
    }

    @And("^I click the Login button$")
    public void iClickTheLoginButton() throws ClassNotFoundException {
        stepDef =scenarioDef.createNode(new GherkinKeyword("And"), "I click the Login button");

        LogHelper.getLogger().info("I click the Login button");
        SfLoginPage page = new SfLoginPage(base.driver);
    }

    @Then("^I should be the SF Home page$")
    public void iShouldBeTheSFHomePage() throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("Then"), "I should be the SF Home page");

        LogHelper.getLogger().info("I should be the SF Home page");

        Assert.assertTrue(new SfHomePage(base.driver).IsAtPage());
    }

    @And("^I enter the following for Login$")
    public void iEnterUsernameAndPassword(DataTable table) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "I enter the username and password");

        List<List<String>> data = table.raw();
        System.out.print("username is: " + data.get(1).get(0) + " and password is : " + data.get(1).get(1));
        SfLoginPage page = new SfLoginPage(base.driver);
        page.login(table.raw().get(1).get(0), table.raw().get(1).get(1));
    }

    @And("^I enter ([^\"]*) and ([^\"]*)$")
    public void iEnterUsernameAndPassword(String username, String password) throws Throwable {
        stepDef = scenarioDef.createNode(new GherkinKeyword("And"), "I enter the username and password");
        System.out.print("username is: " + username + " and password is : " + password);

        SfLoginPage page = new SfLoginPage(base.driver);
        page.login(username, password);
    }
}
