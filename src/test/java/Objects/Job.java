package Objects;

import APIHelper.SkeduloAPI;
import Base.JSONHandler;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static Base.BaseUtil.needToDeletePropertyFile;
import static Base.BaseUtil.userContext;
import static Utilities.GenericFunctions.*;

public class Job {
    private String uid;
    private String jobName;
    private String startDate;
    private String startTime;
    private int duration;
    private String urgency;
    private String description;
    private String region = "Brisbane";
    private String location;
    private String addressType;
    private String address;
    private String billType;
    private JobService service;
    private String contactID = "0036A00000tp4lWQAQ"; // Todo: Need to add default contact into mappingIDs
    private String contactName = "Jenny John Dow";
    private String jobType = "Single Booking";
    private String timezone;
    private HashMap<String, String> resources = new HashMap<>();
    private String jobStatus;
    private String specialInstructions;

    public Job() {
    }

    public Job(String params) {
        this.timezone = userContext.getTimezone();
        setValues(params);
    }

    public Job(TrainingEvent trainingEvent, String startDate) {
        this.startDate = generateDate(startDate.split(" at ")[0].trim(), trainingEvent.getTimezone());
        this.startTime = startDate.split(" at ")[1].trim();
        this.duration = trainingEvent.getCourseCatalog().getClassDuration();
        this.description = String.format("%s, %s %s", trainingEvent.getCourseCatalog().getName(),
                formatDateTime(startTime, "HH:mm", "hh:mma"), trainingEvent.getTimezoneShortFormat());
        this.region = trainingEvent.getRegion();
        this.address = trainingEvent.getAddress();
        this.timezone = trainingEvent.getTimezone();
        this.contactID = trainingEvent.getTrainingContactId();
        this.jobType = "Full Service";
        this.jobStatus = "Pending Allocation";
    }

    public void updateValues(String params) {
        setValues(params);
    }

    private void setValues(String params) {
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "StartDate":
                    this.startDate = generateDate((String) jParams.get(key));
                    break;

                case "StartTime":
                    this.startTime = (String) jParams.get(key);
                    break;

                case "Duration":
                    this.duration = (Integer) jParams.get(key);
                    break;

                case "Urgency":
                    this.urgency = (String) jParams.get(key);
                    break;

                case "Description":
                    this.description = generateStringWithTimeStamp((String) jParams.get(key) + " ", "");
                    break;

                case "Region":
                    this.region = (String) jParams.get(key);
                    break;
            }
        }
    }

    public void updateInfoUsingSkeduloAPI(String fields) {
        String dateInDB = convertDateTimeWithTimezone(String.format("%s %s", startDate, startTime), "dd/MM/yyyy HH:mm",
                timezone, "yyyy-MM-dd'T'HH:mm:SS.sss'Z'", "UTC");
        String filter = String.format("Description == '%s' AND Start ==%s", description, dateInDB);
        JSONHandler response = new JSONHandler(new SkeduloAPI().getJobInfo("node", filter));
        String[] arrayList = fields.split(" ");
        for (String field : arrayList) {
            switch (field) {
                case "UID":
                    uid = response.findJSONValueByKey(field).toString();
                    storePropertyToFile("Jobs", uid, needToDeletePropertyFile);
                    break;
                case "Name":
                    jobName = response.findJSONValueByKey(field).toString();
                    break;
            }
        }
    }

    public boolean isAllResourcesDeleted() {
        for (String resource : resources.keySet()) {
            if (!"Deleted".equals(resources.get(resource)))
                return false;
        }
        return true;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getUrgency() {
        return urgency;
    }

    public void setUrgency(String urgency) {
        this.urgency = urgency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public JobService getService() {
        return service;
    }

    public void setService(JobService service) {
        this.service = service;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getUID() {
        return uid;
    }

    public void setUID(String uid) {
        this.uid = uid;
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public HashMap<String, String> getResources() {
        return resources;
    }

    public void setResources(String resourceName, String status) {
        this.resources.put(resourceName, status);
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public class JobService {
        private String deliveryMethod;
        private String category;
        private String serviceName;
        private String site;
        private String serviceAgreementItem;
    }

    public class JobResource {
        private ArrayList<String> tags;
        private int requiredResources;
        private int asset;
    }
}



