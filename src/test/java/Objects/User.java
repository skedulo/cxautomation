package Objects;

import Base.BaseUtil;
import Base.LogHelper;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

public class User {

    private String type;
    private String domain, skeduloTeamName, environmentType;
    private String name;
    private String username;
    private String password;
    private String description;
    private String orgId;
    private String loginResource;
    private SkedPackages skedPackages;
    private String validSkeduloToken;
    private boolean initializeFlag = false;
    private String dateTimeFormat, timezone, webAppDateFormat;

    public User(String userType) {
        initializeUser(BaseUtil.baseConfiguration.getResourceConfigFile()
                , BaseUtil.baseConfiguration.getEnvironmentConfiguration()
                , userType);
    }

    public User(String fileName, String environment, String userType) {
        initializeUser(fileName, environment, userType);
    }

    private User initializeUser(String fileName, String environment, String userType) {
        try {
            Yaml yaml = new Yaml();
            InputStream dataFile = new FileInputStream(new File(Paths.get(System.getProperty("user.dir"), "config", fileName).toString()));
            List<HashMap<String, Object>> map = yaml.load(dataFile);

            for (HashMap<String, Object> env : map) {
                if (env.get("Environment").equals(environment)) {
                    List<HashMap<String, Object>> userInfo = (List<HashMap<String, Object>>) env.get("Resources");
                    for (HashMap<String, Object> user : userInfo) {
                        if (user.get("Type").equals(userType)) {
                            setDomain(env.get("Domain").toString());
                            setSkeduloTeamName(env.get("SkeduloTeamName").toString());
                            setEnvironmentType(env.get("EnvironmentType").toString());
                            setType(user.get("Type").toString());
                            setName(user.get("Name").toString());
                            setUsername(user.get("Username").toString());
                            setPassword(user.get("Password").toString());
                            setDateTimeFormat(user.get("DateTimeFormat").toString());
                            setTimezone(user.get("Timezone").toString());
                            setWebAppDateFormat(user.get("WebAppDateFormat").toString());
                            setDescription(user.get("Description") == null ? "" : user.get("Description").toString());
                            setOrgId(user.get("OrgId") == null ? "" : user.get("OrgId").toString());
                            setLoginResource(user.get("LoginResource") == null ? "" : user.get("LoginResource").toString());
                            setValidSkeduloToken((String) ((HashMap<String, Object>) user.get("Authorization")).get("ValidSkeduloToken"));
                            setSkedPackages(new SkedPackages((HashMap<String, Object>) user.get("SkedPackages")));
                            initializeFlag = true;
                            LogHelper.getLogger().info("User resource has been initialized successfully");
                            return this;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LogHelper.getLogger().error(String.format("Problem with reading config file %s for user %s at environment %s", fileName, userType, environment));
            LogHelper.getLogger().info(e);
        }
        return this;
    }

    public boolean getInitializeFlag() {
        return initializeFlag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getLoginResource() {
        return loginResource;
    }

    public void setLoginResource(String loginResource) {
        this.loginResource = loginResource;
    }

    public SkedPackages getSkedPackages() {
        return skedPackages;
    }

    public void setSkedPackages(SkedPackages skedPackages) {
        this.skedPackages = skedPackages;
    }

    public String getValidSkeduloToken() {
        return validSkeduloToken;
    }

    public void setValidSkeduloToken(String validSkeduloToken) {
        this.validSkeduloToken = validSkeduloToken;
    }

    public String getSkeduloTeamName() {
        return skeduloTeamName;
    }

    public void setSkeduloTeamName(String skeduloTeamName) {
        this.skeduloTeamName = skeduloTeamName;
    }

    public String getEnvironmentType() {
        return environmentType;
    }

    public void setEnvironmentType(String environmentType) {
        this.environmentType = environmentType;
    }

    public String getDateTimeFormat() {
        return dateTimeFormat;
    }

    public void setDateTimeFormat(String dateTimeFormat) {
        this.dateTimeFormat = dateTimeFormat;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getWebAppDateFormat() {
        return webAppDateFormat;
    }

    public void setWebAppDateFormat(String webAppDateFormat) {
        this.webAppDateFormat = webAppDateFormat;
    }

    public class SkedPackages {
        private String skedPackageName;
        private String publisher;
        private String version;

        public SkedPackages(HashMap<String, Object> skedPackage) {
            setSkedPackageName(skedPackage.get("SkedPackageName").toString());
            setPublisher(skedPackage.get("Publisher").toString());
            setVersion(skedPackage.get("Version").toString());
        }

        public String getSkedPackageName() {
            return skedPackageName;
        }

        public void setSkedPackageName(String skedPackageName) {
            this.skedPackageName = skedPackageName;
        }

        public Object getPublisher() {
            return publisher;
        }

        public void setPublisher(String publisher) {
            this.publisher = publisher;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }
}
