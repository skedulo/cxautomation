package Objects;

import APIHelper.SkeduloAPI;
import cucumber.api.DataTable;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

import static Base.BaseUtil.scenarioContext;
import static Utilities.GenericFunctions.formatDateTime;
import static Utilities.GenericFunctions.generateDate;

public class TrainingEvent {
    private String uid, name, trainingContactId, opportunityId, accountId;
    private HashMap<String, Job> jobs = new HashMap<>();
    private CourseCatalog courseCatalog;
    private String region;
    private String address;
    private String timezone, timezoneShortFormat;

    public TrainingEvent(String params) {
        setValues(params);
    }

    public void insertTrainingEvent() {
        JSONObject jsonVariables = new JSONObject();
        jsonVariables.put("TrainingContactId", trainingContactId);
        jsonVariables.put("OpportunityId", opportunityId);
        jsonVariables.put("AccountId", accountId);
        uid = new SkeduloAPI().insertObjects("TrainingEvent", jsonVariables.toString());
    }

    public void setValues(String params) {
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "TrainingContactId":
                    trainingContactId = String.valueOf(jParams.get(key));
                    break;

                case "OpportunityId":
                    opportunityId = String.valueOf(jParams.get(key));
                    break;

                case "AccountId":
                    accountId = String.valueOf(jParams.get(key));
                    break;
            }
        }
    }

    public void setValues(DataTable data) {
        for (int i = 0; i < data.raw().size(); i++) {
            if ("Region".equals(data.raw().get(i).get(0))) {
                this.region = data.raw().get(i).get(1);
                if (!"Any".equals(region))
                    this.timezone = new SkeduloAPI().getObjectInfo("regions", "Timezone", String.format("Name =='%s'", region));
            }
            if ("Address".equals(data.raw().get(i).get(0)))
                this.address = data.raw().get(i).get(1);
            if ("CourseCatalog".equals(data.raw().get(i).get(0)))
                this.courseCatalog = (CourseCatalog) scenarioContext.getValue(data.raw().get(i).get(1));
        }
    }

    public void insertJobs(DataTable jobData) {
        for (int i = 0; i < jobData.raw().size(); i++) {
            String jobAlias = jobData.raw().get(i).get(0);
            Job job = new Job(this, jobData.raw().get(i).get(1));
            jobs.put(jobAlias, job);
        }
    }

    public void updateJobs(String jobAlias, String data) {
        this.jobs.get(jobAlias).setStartDate(generateDate(data.split(" at ")[0].trim(), this.timezone));
        this.jobs.get(jobAlias).setStartTime(data.split(" at ")[1].trim());
        String description = String.format("%s, %s %s", this.getCourseCatalog().getName(),
                formatDateTime(this.jobs.get(jobAlias).getStartTime(), "HH:mm", "hh:mma"), this.getTimezoneShortFormat());
        this.jobs.get(jobAlias).setDescription(description);
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CourseCatalog getCourseCatalog() {
        return courseCatalog;
    }

    public void setCourseCatalog(CourseCatalog courseCatalog) {
        this.courseCatalog = courseCatalog;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public HashMap<String, Job> getJobs() {
        return jobs;
    }

    public void setJobs(HashMap<String, Job> jobs) {
        this.jobs = jobs;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTrainingContactId() {
        return trainingContactId;
    }

    public void setTrainingContactId(String trainingContactId) {
        this.trainingContactId = trainingContactId;
    }

    public String getOpportunityId() {
        return opportunityId;
    }

    public void setOpportunityId(String opportunityId) {
        this.opportunityId = opportunityId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getTimezoneShortFormat() {
        return timezoneShortFormat;
    }

    public void setTimezoneShortFormat(String timezoneShortFormat) {
        this.timezoneShortFormat = timezoneShortFormat;
    }
}
