package Objects;

import APIHelper.SkeduloAPI;
import Utilities.GenericFunctions;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import static Base.BaseUtil.scenarioContext;

public class CourseClassType {
    private String uid, name, locationId, classType, createdByIDId, createdDate, lastModifiedByIDId, lastModifiedDate;
    private int displayOrder, ratioToInstructor, startTime, endTime;
    private double totalHours;
    private CourseTemplate courseTemplate;

    public CourseClassType(String params) {
        // Todo: Create mapping with default value
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        createdByIDId = "005g0000004CMu2AAG";
        lastModifiedByIDId = createdByIDId;
        createdDate = dateFormat.format(new Date());
        lastModifiedDate = createdDate;
        setValues(params);
    }

    public void setValues(String params) {
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "Name":
                    name = GenericFunctions.generateStringWithTimeStamp(String.valueOf(jParams.get(key)), "");
                    break;
                case "CourseTemplate":
                    courseTemplate = (CourseTemplate) scenarioContext.getValue(String.valueOf(jParams.get(key)));
                    courseTemplate.setCourseClassTypeList(this);
                    break;
                case "ClassType":
                    classType = String.valueOf(jParams.get(key));
                    break;
                case "DisplayOrder":
                    displayOrder = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "StartTime":
                    startTime = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "EndTime":
                    endTime = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "LocationId":
                    locationId = String.valueOf(jParams.get(key));
                    break;
                case "RatioToInstructor":
                    ratioToInstructor = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "TotalHours":
                    totalHours = Float.parseFloat(String.valueOf(jParams.get(key)));
                    break;
            }
        }
    }

    public void insertCourseClassType() {
        JSONObject jsonVariables = new JSONObject();
        jsonVariables.put("CreatedByIDId", createdByIDId);
        jsonVariables.put("LastModifiedByIDId", lastModifiedByIDId);
        jsonVariables.put("CreatedDate", createdDate);
        jsonVariables.put("LastModifiedDate", lastModifiedDate);
        jsonVariables.put("Name", name);
        jsonVariables.put("CourseTemplateId", courseTemplate.getUid());
        jsonVariables.put("ClassType", classType);
        jsonVariables.put("DisplayOrder", displayOrder);
        jsonVariables.put("StartTime", startTime);
        jsonVariables.put("EndTime", endTime);
        jsonVariables.put("LocationId", locationId);
        jsonVariables.put("RatioToInstructor", ratioToInstructor);
        jsonVariables.put("TotalHours", totalHours);
        uid = new SkeduloAPI().insertObjects("CourseClassType", jsonVariables.toString());
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public String getCreatedByIDId() {
        return createdByIDId;
    }

    public void setCreatedByIDId(String createdByIDId) {
        this.createdByIDId = createdByIDId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedByIDId() {
        return lastModifiedByIDId;
    }

    public void setLastModifiedByIDId(String lastModifiedByIDId) {
        this.lastModifiedByIDId = lastModifiedByIDId;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public int getRatioToInstructor() {
        return ratioToInstructor;
    }

    public void setRatioToInstructor(int ratioToInstructor) {
        this.ratioToInstructor = ratioToInstructor;
    }

    public double getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(double totalHours) {
        this.totalHours = totalHours;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public CourseTemplate getCourseTemplate() {
        return courseTemplate;
    }

    public void setCourseTemplate(CourseTemplate courseTemplate) {
        this.courseTemplate = courseTemplate;
    }
}
