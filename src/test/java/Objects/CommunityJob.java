package Objects;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import cucumber.api.DataTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static Base.BaseUtil.scenarioContext;
import static Utilities.GenericFunctions.*;

public class CommunityJob {
    private CourseCatalog courseCatalog;
    private String communityClassSubType, region, location, startDate, startTime, weekDays;
    private int duration, repeatWeeks, classDays, evaluators;
    private boolean isRecurring, isSkippedWeekend, isStackedClass;
    private List<Job> jobs = new ArrayList<>();
    private String timezone;
    private String jobType;
    private String specialInstructions;

    public CommunityJob(DataTable data, String type) {
        this.jobType = type;
        setValues(data);
    }

    public void setValues(DataTable data) {
        for (int i = 0; i < data.raw().size(); i++) {
            String key = data.raw().get(i).get(0);
            switch (key) {
                case "CourseCatalog":
                    this.courseCatalog = (CourseCatalog) scenarioContext.getValue(data.raw().get(i).get(1));
                    break;
                case "CommunityClassSubType":
                    this.communityClassSubType = data.raw().get(i).get(1);
                    break;
                case "Region":
                    this.region = data.raw().get(i).get(1);
                    if (!"Any".equals(region))
                        this.timezone = new SkeduloAPI().getObjectInfo("regions", "Timezone", String.format("Name =='%s'", region));
                    break;
                case "Location":
                    this.location = data.raw().get(i).get(1);
                    break;
                case "StartTime":
                    String value = data.raw().get(i).get(1);
                    this.startDate = generateDate(value.split(" at ")[0].trim(), this.getTimezone());
                    this.startTime = value.split(" at ")[1].trim();
                    break;
                case "Duration":
                    this.duration = Integer.parseInt(data.raw().get(i).get(1));
                    break;
                case "IsRecurring":
                    this.isRecurring = "Yes".equals(data.raw().get(i).get(1));
                    break;
                case "ClassDays":
                    try {
                        this.classDays = Integer.parseInt(data.raw().get(i).get(1));
                    } catch (Exception e) {
                        this.classDays = 0;
                    }
                    break;
                case "RepeatWeeks":
                    try {
                        this.repeatWeeks = Integer.parseInt(data.raw().get(i).get(1));
                    } catch (Exception e) {
                        this.repeatWeeks = 0;
                    }
                    break;
                case "WeekDays":
                    this.weekDays = data.raw().get(i).get(1);
                    break;
                case "Type":
                    this.jobType = data.raw().get(i).get(1);
                    break;
                case "Evaluators":
                    try {
                        this.evaluators = Integer.parseInt(data.raw().get(i).get(1));
                    } catch (Exception e) {
                        this.evaluators = 0;
                    }
                    break;
                case "IsSkippedWeekend":
                    this.isSkippedWeekend = "Yes".equals(data.raw().get(i).get(1));
                    break;
                case "IsStackedClass":
                    this.isStackedClass = "Yes".equals(data.raw().get(i).get(1));
                    break;
                case "SpecialInstructions":
                    this.specialInstructions = generateStringWithTimeStamp(data.raw().get(i).get(1) + " ", "");
                    break;
            }
        }
    }

    public void insertJobs() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");

        addJob(startDate);
        if (isRecurring) {
            Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone(timezone));
            try {
                c.setTime(dateFormat.parse(startDate));
            } catch (ParseException e) {
                LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
            }
            int startWeek = c.get(Calendar.WEEK_OF_YEAR);
            c.add(Calendar.DATE, 1);
            int currentWeek = c.get(Calendar.WEEK_OF_YEAR);
            while (jobs.size() < classDays) {
                String currentDay = dayFormat.format(c.getTime());
                boolean flag = (currentWeek - startWeek) % repeatWeeks == 0;
                if (flag && weekDays.contains(currentDay)) {
                    addJob(dateFormat.format(c.getTime()));
                }
                c.add(Calendar.DATE, 1);
                currentWeek = c.get(Calendar.WEEK_OF_YEAR);
            }
        }
    }

    private void addJob(String date) {
        Job job = new Job();
        job.setDescription(String.format("%s, %s %s", courseCatalog.getName(), formatDateTime(startTime, "HH:mm", "hh:mma"),
                TimeZone.getTimeZone(timezone).getDisplayName(true, TimeZone.SHORT)));
        job.setJobType(jobType);
        job.setRegion(region);
        job.setTimezone(timezone);
        job.setStartDate(date);
        job.setStartTime(startTime);
        job.setDuration(duration);
        job.setUrgency(null);
        job.setJobStatus("Pending Allocation");
        job.setLocation(location);
        job.setSpecialInstructions(specialInstructions);
        jobs.add(job);
    }

    public int countActiveJobs() {
        int count = 0;
        for (Job job : jobs)
            if (!"Deleted".equals(job.getJobStatus()))
                count++;
        return count;
    }

    public String getCommunityClassSubType() {
        return communityClassSubType;
    }

    public void setCommunityClassSubType(String communityClassSubType) {
        this.communityClassSubType = communityClassSubType;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getWeekDays() {
        return weekDays;
    }

    public void setWeekDays(String weekDays) {
        this.weekDays = weekDays;
    }

    public int getClassDays() {
        return classDays;
    }

    public void setClassDays(int classDays) {
        this.classDays = classDays;
    }

    public int getRepeatWeeks() {
        return repeatWeeks;
    }

    public void setRepeatWeeks(int repeatWeeks) {
        this.repeatWeeks = repeatWeeks;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    public boolean isSkippedWeekend() {
        return isSkippedWeekend;
    }

    public void setSkippedWeekend(boolean skippedWeekend) {
        isSkippedWeekend = skippedWeekend;
    }

    public boolean isStackedClass() {
        return isStackedClass;
    }

    public void setStackedClass(boolean stackedClass) {
        isStackedClass = stackedClass;
    }

    public CourseCatalog getCourseCatalog() {
        return courseCatalog;
    }

    public void setCourseCatalog(CourseCatalog courseCatalog) {
        this.courseCatalog = courseCatalog;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Job job) {
        this.jobs.add(job);
    }

    public int getEvaluators() {
        return evaluators;
    }

    public void setEvaluators(int evaluators) {
        this.evaluators = evaluators;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }
}
