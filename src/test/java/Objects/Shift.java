package Objects;

import Base.LogHelper;
import cucumber.api.DataTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static Base.BaseUtil.userContext;
import static Utilities.GenericFunctions.formatDateTime;
import static Utilities.GenericFunctions.generateDate;

public class Shift {
    private String type, subType, startDate, endDate, startTime, endTime, pattern, daysOfWeek, recurFor, address, shiftId;
    private boolean isRecurring, isSkippedWeekend, isSkippedHolidays;

    public Shift(){}

    public Shift(String type, DataTable data) throws ParseException {
        this.type = type;
        for (int i = 0; i < data.raw().size(); i++) {
            if ("SubType".equals(data.raw().get(i).get(0)))
                this.subType = data.raw().get(i).get(1);

            if ("StartDate".equals(data.raw().get(i).get(0))) {
                this.startDate = generateDate(data.raw().get(i).get(1));
            }

            if ("StartTime".equals(data.raw().get(i).get(0)))
                this.startTime = data.raw().get(i).get(1);

            if ("EndTime".equals(data.raw().get(i).get(0)))
                this.endTime = data.raw().get(i).get(1);

            if ("Address".equals(data.raw().get(i).get(0)))
                this.address = data.raw().get(i).get(1);

            if ("IsRecurring".equals(data.raw().get(i).get(0)))
                this.isRecurring = data.raw().get(i).get(1).equals("Yes");

            if ("IsSkippedWeekend".equals(data.raw().get(i).get(0)))
                this.isSkippedWeekend = data.raw().get(i).get(1).equals("Yes");

            if ("IsSkippedHolidays".equals(data.raw().get(i).get(0)))
                this.isSkippedHolidays = data.raw().get(i).get(1).equals("Yes");

            if ("Pattern".equals(data.raw().get(i).get(0)))
                this.pattern = data.raw().get(i).get(1);

            if ("DaysOfWeek".equals(data.raw().get(i).get(0)))
                this.daysOfWeek = data.raw().get(i).get(1);

            if ("RecurFor".equals(data.raw().get(i).get(0)))
                this.recurFor = data.raw().get(i).get(1);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public String getRecurFor() {
        return recurFor;
    }

    public void setRecurFor(String recurFor) {
        this.recurFor = recurFor;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    public boolean isSkippedWeekend() {
        return isSkippedWeekend;
    }

    public void setSkippedWeekend(boolean skippedWeekend) {
        isSkippedWeekend = skippedWeekend;
    }

    public boolean isSkippedHolidays() {
        return isSkippedHolidays;
    }

    public void setSkippedHolidays(boolean skippedHolidays) {
        isSkippedHolidays = skippedHolidays;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public List<Map<String, String>> generateShiftList() {
        List<Map<String, String>> list = new ArrayList<>();
        try {
            if (!isRecurring) {
                list.add(createEventPoint(startDate, startTime, endTime));
            } else {
                list.add(createEventPoint(startDate, startTime, endTime));

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");

                List<String> weekDetail = new ArrayList<>(Arrays.asList(daysOfWeek.split(";")));
                if (weekDetail.size() < 2) {
                    weekDetail.add(weekDetail.get(0));
                }

                int weekRecur = Integer.parseInt(recurFor.toLowerCase().replace("week", "").replace("s", "").trim());

                Calendar c = Calendar.getInstance();
                c.setTimeZone(TimeZone.getTimeZone(userContext.getTimezone()));
                c.setTime(dateFormat.parse(startDate));
                int startWeek = c.get(Calendar.WEEK_OF_YEAR);
                c.add(Calendar.DATE, 1);
                int currentWeek = c.get(Calendar.WEEK_OF_YEAR);

                while (currentWeek < startWeek + weekRecur) {
                    String currentDay = dayFormat.format(c.getTime());
                    if (weekDetail.get((currentWeek - startWeek) % 2).contains(currentDay))
                        list.add(createEventPoint(dateFormat.format(c.getTime()), startTime, endTime));

                    c.add(Calendar.DATE, 1);
                    currentWeek = c.get(Calendar.WEEK_OF_YEAR);
                }
            }
        } catch (ParseException e) {
            LogHelper.getLogger().error(e.getStackTrace());
        }
        return list;
    }

    private Map<String, String> createEventPoint(String date, String startTime, String endTime) throws ParseException {
        Map<String, String> map = new HashMap<>();
        map.put("Start", formatDateTime(date, startTime, 0, "d/MM/yyyy h:mm a"));
        map.put("End", formatDateTime(date, endTime, 0, "d/MM/yyyy h:mm a"));
        return map;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }
}