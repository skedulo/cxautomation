package Objects;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import cucumber.api.DataTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static Base.BaseUtil.scenarioContext;
import static Utilities.GenericFunctions.*;

public class NATJob {
    private CourseCatalog courseCatalog;
    private String type, startDate, specialInstructions, region, location, daysOfWeek;
    private int maxStudents, maxHoursPerDay;
    private boolean isRecurring;
    private String timezone;
    private List<Job> jobs = new ArrayList<>();

    public NATJob(DataTable data) {
        setValues(data);
    }

    public void setValues(DataTable data) {
        for (int i = 0; i < data.raw().size(); i++) {
            String key = data.raw().get(i).get(0);
            String value = String.valueOf(data.raw().get(i).get(1));
            switch (key) {
                case "CourseCatalog":
                    this.courseCatalog = (CourseCatalog) scenarioContext.getValue(value);
                    break;
                case "Type":
                    this.type = value;
                    break;
                case "StartDate":
                    this.startDate = generateDate(value, this.getTimezone());
                    break;
                case "SpecialInstructions":
                    this.specialInstructions = value;
                    break;
                case "Region":
                    this.region = value;
                    this.timezone = new SkeduloAPI().getObjectInfo("regions", "Timezone", String.format("Name =='%s'", region));
                    break;
                case "Location":
                    this.location = value;
                    break;
                case "MaxStudents":
                    this.maxStudents = Integer.parseInt(value);
                    break;
                case "MaxHoursPerDay":
                    this.maxHoursPerDay = Integer.parseInt(value);
                    break;
                case "IsRecurring":
                    this.isRecurring = "Yes".equals(value);
                    break;
                case "DaysOfWeek":
                    this.daysOfWeek = value;
                    break;
            }
        }
    }

    public void insertJobs() {
        for (CourseClassType courseClassType : courseCatalog.getCourseTemplate().getCourseClassTypeList()) {
            int start = courseClassType.getStartTime();
            int end = courseClassType.getEndTime();
            double totalHours = courseClassType.getTotalHours();
            double classTypeDuration = (end - start) / 100;
            double remainHours = totalHours;
            while (remainHours > 0) {
                double duration = Math.min(Math.min(classTypeDuration, remainHours), maxHoursPerDay);
                double breaks = ((int) ((duration - 5) / 5 + 1)) * 0.5;
                addJob(formatDateTime(String.valueOf(start), "hmm", "hh:mm"), (int) duration * 60);
                remainHours = remainHours - duration + breaks;
            }
        }
        updateJobStartDate();
    }

    private void updateJobStartDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");
        if (isRecurring) {
            Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone(timezone));
            try {
                c.setTime(dateFormat.parse(startDate));
            } catch (ParseException e) {
                LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
            }
            for (int i = 0; i < jobs.size(); i++) {
                String currentDay = dayFormat.format(c.getTime());
                int count = 7; // flag to avoid endless loop if user input incorrect day format
                while (!daysOfWeek.contains(currentDay) && count > 0) {
                    c.add(Calendar.DATE, 1);
                    currentDay = dayFormat.format(c.getTime());
                    count--;
                }
                if (daysOfWeek.contains(currentDay))
                    jobs.get(i).setStartDate(dateFormat.format(c.getTime()));
                c.add(Calendar.DATE, 1);
            }
        }
    }

    private void addJob(String startTime, int duration) {
        Job job = new Job();
        job.setDescription(String.format("%s, %s %s", courseCatalog.getName(), formatDateTime(startTime, "HH:mm", "hh:mma"),
                TimeZone.getTimeZone(timezone).getDisplayName(true, TimeZone.SHORT)));
        job.setJobType(type);
        job.setRegion(region);
        job.setTimezone(timezone);
        job.setStartTime(startTime);
        job.setDuration(duration);
        job.setUrgency(null);
        job.setJobStatus("Pending Allocation");
        job.setLocation(location);
        job.setSpecialInstructions(specialInstructions);
        jobs.add(job);
    }

    public CourseCatalog getCourseCatalog() {
        return courseCatalog;
    }

    public void setCourseCatalog() {
        setCourseCatalog();
    }

    public void setCourseCatalog(CourseCatalog courseCatalog) {
        this.courseCatalog = courseCatalog;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(String daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public int getMaxStudents() {
        return maxStudents;
    }

    public void setMaxStudents(int maxStudents) {
        this.maxStudents = maxStudents;
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Job job) {
        this.jobs.add(job);
    }

    public int getMaxHoursPerDay() {
        return maxHoursPerDay;
    }

    public void setMaxHoursPerDay(int maxHoursPerDay) {
        this.maxHoursPerDay = maxHoursPerDay;
    }
}
