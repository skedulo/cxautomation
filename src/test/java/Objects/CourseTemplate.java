package Objects;

import APIHelper.SkeduloAPI;
import Utilities.GenericFunctions;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

import static Base.BaseUtil.scenarioContext;

public class CourseTemplate {
    private String uid, name, courseClassDays;
    private String externalID, createdByIDId, lastModifiedByIDId, createdDate, lastModifiedDate;
    private int maximumHoursPerDay;
    private boolean defaultTemplate;
    private CourseCatalog courseCatalog;
    private List<CourseClassType> courseClassTypeList = new ArrayList<>();

    public CourseTemplate(String params) {
        // Todo: Create mapping with default value
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        createdByIDId = "005g0000004CMu2AAG";
        lastModifiedByIDId = createdByIDId;
        createdDate = dateFormat.format(new Date());
        lastModifiedDate = createdDate;
        setValues(params);
    }

    public void setValues(String params) {
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "Name":
                    name = GenericFunctions.generateStringWithTimeStamp(String.valueOf(jParams.get(key)), "");
                    break;
                case "CourseCatalog":
                    courseCatalog = (CourseCatalog) scenarioContext.getValue(String.valueOf(jParams.get(key)));
                    courseCatalog.setCourseTemplate(this);
                    break;
                case "CourseClassDays":
                    courseClassDays = String.valueOf(jParams.get(key));
                    break;
                case "DefaultTemplate":
                    defaultTemplate = "Yes".equals(String.valueOf(jParams.get(key)));
                    break;
                case "MaximumHoursPerDay":
                    maximumHoursPerDay = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
            }
        }
    }

    public void insertCourseTemplate() {
        JSONObject jsonVariables = new JSONObject();
        jsonVariables.put("CreatedByIDId", createdByIDId);
        jsonVariables.put("LastModifiedByIDId", lastModifiedByIDId);
        jsonVariables.put("CreatedDate", createdDate);
        jsonVariables.put("LastModifiedDate", lastModifiedDate);
        jsonVariables.put("Name", name);
        jsonVariables.put("CourseCatalogId", courseCatalog.getUid());
        jsonVariables.put("CourseClassDays", courseClassDays);
        jsonVariables.put("DefaultTemplate", defaultTemplate);
        jsonVariables.put("ExternalID", externalID);
        jsonVariables.put("MaximumHoursPerDay", maximumHoursPerDay);
        uid = new SkeduloAPI().insertObjects("CourseTemplate", jsonVariables.toString());
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourseClassDays() {
        return courseClassDays;
    }

    public void setCourseClassDays(String courseClassDays) {
        this.courseClassDays = courseClassDays;
    }

    public String getExternalID() {
        return externalID;
    }

    public void setExternalID(String externalID) {
        this.externalID = externalID;
    }

    public String getCreatedByIDId() {
        return createdByIDId;
    }

    public void setCreatedByIDId(String createdByIDId) {
        this.createdByIDId = createdByIDId;
    }

    public String getLastModifiedByIDId() {
        return lastModifiedByIDId;
    }

    public void setLastModifiedByIDId(String lastModifiedByIDId) {
        this.lastModifiedByIDId = lastModifiedByIDId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public int getMaximumHoursPerDay() {
        return maximumHoursPerDay;
    }

    public void setMaximumHoursPerDay(int maximumHoursPerDay) {
        this.maximumHoursPerDay = maximumHoursPerDay;
    }

    public boolean isDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(boolean defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }

    public CourseCatalog getCourseCatalog() {
        return courseCatalog;
    }

    public void setCourseCatalog(CourseCatalog courseCatalog) {
        this.courseCatalog = courseCatalog;
    }

    public List<CourseClassType> getCourseClassTypeList() {
        return courseClassTypeList;
    }

    public void setCourseClassTypeList(CourseClassType courseClassType) {
        this.courseClassTypeList.add(courseClassType);
    }
}
