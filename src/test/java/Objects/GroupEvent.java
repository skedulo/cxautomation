package Objects;

import Utilities.GenericFunctions;
import org.json.JSONObject;

import java.util.Iterator;

public class GroupEvent {

    private String groupName, status, region, address, addressType, description, billable, client;
    private int maximumAttendees, maximumResources, asset;

    public GroupEvent(String params) {
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "GroupName":
                    this.groupName = GenericFunctions.generateStringWithTimeStamp(jParams.get(key).toString(), "");
                    break;

                case "Description":
                    this.description = GenericFunctions.generateStringWithTimeStamp(String.format("%s ", jParams.get(key).toString()), "");
                    break;

                case "Status":
                    this.status = jParams.get(key).toString();
                    break;

                case "Region":
                    this.region = jParams.get(key).toString();
                    break;

                case "AddressType":
                    this.addressType = jParams.get(key).toString();
                    break;

                case "Address":
                    this.address = jParams.get(key).toString();
                    break;

                case "Billable":
                    this.billable = jParams.get(key).toString();
                    break;

                case "Client":
                    this.client = jParams.get(key).toString();
                    break;

                case "MaximumAttendees":
                    this.maximumAttendees = (Integer) jParams.get(key);
                    break;

                case "MaximumResources":
                    this.maximumResources = (Integer) jParams.get(key);
                    break;

                case "Asset":
                    this.asset = (Integer) jParams.get(key);
                    break;
            }
        }
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBillable() {
        return billable;
    }

    public void setBillable(String billable) {
        this.billable = billable;
    }

    public int getMaximumAttendees() {
        return maximumAttendees;
    }

    public void setMaximumAttendees(int maximumAttendees) {
        this.maximumAttendees = maximumAttendees;
    }

    public int getMaximumResources() {
        return maximumResources;
    }

    public void setMaximumResources(int maximumResources) {
        this.maximumResources = maximumResources;
    }

    public int getAsset() {
        return asset;
    }

    public void setAsset(int asset) {
        this.asset = asset;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
