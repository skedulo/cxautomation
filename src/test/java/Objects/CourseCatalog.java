package Objects;

import APIHelper.SkeduloAPI;
import Base.LogHelper;
import Utilities.GenericFunctions;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CourseCatalog {
    private String uid, name, externalId, backupName, masterClassName, ratioToInstructor;
    private int noOfClasses, noOfInstructors, noOfBreaks, noOfStudents, breakDuration, classDuration;
    private boolean isActive;
    private List<String> types;
    private String createdByIDId, lastModifiedByIDId, createdDate, lastModifiedDate;
    private CourseTemplate courseTemplate;

    public CourseCatalog(String params) {
        // Todo: Create mapping with default value
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        createdByIDId = "005g0000004CMu2AAG";
        lastModifiedByIDId = createdByIDId;
        createdDate = dateFormat.format(new Date());
        lastModifiedDate = createdDate;
        setValues(params);
    }

    public void setValues(String params) {
        JSONObject jParams = new JSONObject(params);
        Iterator<?> keys = jParams.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();

            switch (key) {
                case "Name":
                    name = GenericFunctions.generateStringWithTimeStamp(String.valueOf(jParams.get(key)), "");
                    break;
                case "NumberOfClasses":
                    noOfClasses = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "NumberOfInstructors":
                    noOfInstructors = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "NumberOfBreaks":
                    noOfBreaks = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "NumberOfStudents":
                    noOfStudents = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "BreakDuration":
                    breakDuration = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "RatioToInstructor":
                    ratioToInstructor = String.valueOf(jParams.get(key));
                    break;
                case "ClassDuration":
                    classDuration = Integer.parseInt(String.valueOf(jParams.get(key)));
                    break;
                case "Type":
                    try {
                        types = new ObjectMapper().readValue(String.valueOf(jParams.get(key)), List.class);
                    } catch (IOException e) {
                        LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
                    }
                    break;
                case "IsActive":
                    isActive = (Boolean) jParams.get(key);
                    break;
                case "ExternalID":
                    externalId = String.valueOf(jParams.get(key));
                    break;
                case "BackupName":
                    backupName = String.valueOf(jParams.get(key));
                    break;
                case "MasterClassName":
                    masterClassName = String.valueOf(jParams.get(key));
                    break;
                case "CreatedByIDId":
                    createdByIDId = String.valueOf(jParams.get(key));
                    break;
                case "LastModifiedByIDId":
                    lastModifiedByIDId = String.valueOf(jParams.get(key));
                    break;
                case "CreatedDate":
                    createdDate = String.valueOf(jParams.get(key));
                    break;
                case "LastModifiedDate":
                    lastModifiedDate = String.valueOf(jParams.get(key));
                    break;
            }
        }
    }

    public void insertCourseCatalogs() {
        JSONObject jsonVariables = new JSONObject();
        jsonVariables.put("CreatedByIDId", createdByIDId);
        jsonVariables.put("LastModifiedByIDId", lastModifiedByIDId);
        jsonVariables.put("CreatedDate", createdDate);
        jsonVariables.put("LastModifiedDate", lastModifiedDate);
        jsonVariables.put("Name", name);
        jsonVariables.put("NumberOfClasses", noOfClasses);
        jsonVariables.put("NumberOfInstructors", noOfInstructors);
        jsonVariables.put("NumberOfBreaks", noOfBreaks);
        jsonVariables.put("NumberOfStudents", noOfStudents);
        jsonVariables.put("BreakDuration", breakDuration);
        jsonVariables.put("RatioToInstructor", ratioToInstructor);
        jsonVariables.put("ClassDuration", classDuration);
        jsonVariables.put("Type", new JSONArray(types));
        jsonVariables.put("IsActive", isActive);
        jsonVariables.put("ExternalID", externalId);
        jsonVariables.put("BackupName", backupName);
        jsonVariables.put("MasterClassName", masterClassName);
        uid = new SkeduloAPI().insertObjects("CourseCatalogs", jsonVariables.toString());
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getBackupName() {
        return backupName;
    }

    public void setBackupName(String backupName) {
        this.backupName = backupName;
    }

    public String getMasterClassName() {
        return masterClassName;
    }

    public void setMasterClassName(String masterClassName) {
        this.masterClassName = masterClassName;
    }

    public String getRatioToInstructor() {
        return ratioToInstructor;
    }

    public void setRatioToInstructor(String ratioToInstructor) {
        this.ratioToInstructor = ratioToInstructor;
    }

    public int getNoOfClasses() {
        return noOfClasses;
    }

    public void setNoOfClasses(int noOfClasses) {
        this.noOfClasses = noOfClasses;
    }

    public int getNoOfInstructors() {
        return noOfInstructors;
    }

    public void setNoOfInstructors(int noOfInstructors) {
        this.noOfInstructors = noOfInstructors;
    }

    public int getNoOfBreaks() {
        return noOfBreaks;
    }

    public void setNoOfBreaks(int noOfBreaks) {
        this.noOfBreaks = noOfBreaks;
    }

    public int getNoOfStudents() {
        return noOfStudents;
    }

    public void setNoOfStudents(int noOfStudents) {
        this.noOfStudents = noOfStudents;
    }

    public int getBreakDuration() {
        return breakDuration;
    }

    public void setBreakDuration(int breakDuration) {
        this.breakDuration = breakDuration;
    }

    public int getClassDuration() {
        return classDuration;
    }

    public void setClassDuration(int classDuration) {
        this.classDuration = classDuration;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getCreatedByIDId() {
        return createdByIDId;
    }

    public void setCreatedByIDId(String createdByIDId) {
        this.createdByIDId = createdByIDId;
    }

    public String getLastModifiedByIDId() {
        return lastModifiedByIDId;
    }

    public void setLastModifiedByIDId(String lastModifiedByIDId) {
        this.lastModifiedByIDId = lastModifiedByIDId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public CourseTemplate getCourseTemplate() {
        return courseTemplate;
    }

    public void setCourseTemplate(CourseTemplate courseTemplate) {
        this.courseTemplate = courseTemplate;
    }
}