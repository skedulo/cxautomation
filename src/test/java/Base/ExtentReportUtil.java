package Base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import cucumber.api.Scenario;
import cucumber.runtime.ScenarioImpl;
import gherkin.formatter.model.Result;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import static Utilities.GenericFunctions.generateStringWithTimeStamp;

public class ExtentReportUtil extends BaseUtil {

    public void extentReport() {
        String fileName = Paths.get(reportLocation, generateStringWithTimeStamp("ExtentReport_", ".html")).toString();
        extentReports = new ExtentReports();
        ExtentHtmlReporter extentHtmlReporter = new ExtentHtmlReporter(fileName);
        extentHtmlReporter.config().setTheme(Theme.DARK);
        extentHtmlReporter.config().setDocumentTitle("Skedulo CX Automation Test Result");
        extentHtmlReporter.config().setReportName("Test report");
        extentReports.attachReporter(extentHtmlReporter);
        LogHelper.getLogger().debug("Initialize Extent Report at " + fileName);
    }

    public void extentReportScreenshot(Scenario scenario) throws IOException {
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String screenshotFileName = generateStringWithTimeStamp(scenario.getName().replaceAll("[ $&+,:;=?@#|'<>.^*()%!-]","_").replaceAll("(_)+","_"), ".png");
        Path screenshotPath = Paths.get(reportLocation, screenshotFileName);
        Files.copy(screenshot.toPath(), screenshotPath);

        Field field = FieldUtils.getField(((ScenarioImpl) scenario).getClass(), "stepResults", true);
        field.setAccessible(true);
        String errMsg;
        try {
            ArrayList<Result> results = (ArrayList<Result>) field.get(scenario);
            errMsg = results.get(results.size() - 1).getErrorMessage();
        } catch (Exception e) {
            errMsg = Arrays.toString(e.getStackTrace());
        }
        stepDef.fail(errMsg).addScreenCaptureFromPath(screenshotFileName);
        LogHelper.getLogger().error(errMsg);
    }

    public void flushReport() {
        extentReports.flush();
        LogHelper.getLogger().info("Extent report is flushed");
    }
}
