package Base;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class JSONHandler {
    private JSONObject jObject;

    public JSONHandler() {
        jObject = new JSONObject();
    }

    public JSONHandler(JSONObject j) {
        jObject = j;
    }

    public JSONHandler(String jString) {
        jObject = new JSONObject(jString);
    }

    public JSONObject getjObject() {
        return jObject;
    }

    public void setjObject(JSONObject jObject) {
        this.jObject = jObject;
    }

    public Object findJSONValueByKey(String k) throws JSONException {
        return find(k, jObject);
    }

    public void setJSONValueByKey(String k, String value) {

    }

    private Object find(String k, JSONObject jObj) throws JSONException {
        Iterator<?> keys = jObj.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (key.equals(k)) {
                return jObj.get(key);
            }

            if (jObj.get(key) instanceof JSONObject) {
                return find(k, (JSONObject) jObj.get(key));
            }

            if (jObj.get(key) instanceof JSONArray) {
                JSONArray jar = (JSONArray) jObj.get(key);
                for (int i = 0; i < jar.length(); i++) {
                    JSONObject j = jar.getJSONObject(i);
                    return find(k, j);
                }
            }
        }
        return null;
    }
}
