package Base;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.DriverManagerType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class WebDriverManager extends BaseUtil {
    public enum WebBrowsers {
        CHROME, CHROME_HEADLESS, CHROME_ON_GRID
    }

    public static void initializeWebDriver() {
        WebBrowsers browserType = WebBrowsers.valueOf(baseConfiguration.getBrowserConfiguration());
        ChromeOptions chromeOptions;
        chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-notifications");
        chromeOptions.addArguments("--window-size=1920,1080");
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("disable-infobars");
        String driverVersion = baseConfiguration.getProperty("DriverVersion");
        if (!browserInitializeFlag) {
            switch (browserType) {
                case CHROME:
                    if ("default".equals(driverVersion))
                        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
                    else
                        ChromeDriverManager.getInstance(DriverManagerType.CHROME).version(driverVersion).setup();
                    driver = new ChromeDriver(chromeOptions);
                    break;

                case CHROME_HEADLESS:
                    if ("default".equals(driverVersion))
                        ChromeDriverManager.getInstance(DriverManagerType.CHROME).setup();
                    else
                        ChromeDriverManager.getInstance(DriverManagerType.CHROME).version(driverVersion).setup();
                    chromeOptions.setHeadless(true);
                    driver = new ChromeDriver(chromeOptions);
                    break;

                case CHROME_ON_GRID:
                    break;
            }
            hasUserLoggedIn = false;
            browserInitializeFlag = true;
        }
        driver.manage().timeouts().pageLoadTimeout(defaultWaitTime, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
    }
}

