package Base;

import org.apache.log4j.*;

public class LogHelper {

    private static Logger logger = Logger.getLogger(LogHelper.class);

    public static void initializeLogger(String fileName) {
        logger.setLevel(Level.ALL);

        PatternLayout layOut = new PatternLayout();
        layOut.setConversionPattern("[%d{yyyy-MM-dd HH:mm:ss}]  %15.15C{1}:%-4L %5p : %m%n");

        RollingFileAppender fileAppender = new RollingFileAppender();
        fileAppender.setAppend(true);
        fileAppender.setName("FileLog");
        fileAppender.setMaxFileSize("1MB");
        fileAppender.setMaxBackupIndex(1);
        fileAppender.setFile(fileName);
        fileAppender.activateOptions();
        fileAppender.setLayout(layOut);
        logger.addAppender(fileAppender);

        ConsoleAppender consoleAppender = new ConsoleAppender();
        consoleAppender.setLayout(layOut);
        consoleAppender.setTarget("System.out");
        consoleAppender.setName("ConsoleLog");
        consoleAppender.activateOptions();
        logger.addAppender(consoleAppender);
    }

    public static Logger getLogger() {
        return LogHelper.logger;
    }
}
