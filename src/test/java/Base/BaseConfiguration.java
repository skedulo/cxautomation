package Base;

import org.testng.ITestContext;

import java.util.Map;

public class BaseConfiguration {
    private Map<String, String> configurations;

    public void getBaseConfigurationFromTestNGXML(ITestContext iTestContext) {
        configurations = iTestContext.getCurrentXmlTest().getAllParameters();

        if ("default".equals(getResourceConfigFileFlag())) {
            configurations.replace("ResourceConfigFile", "Resource.yml");
        }
    }

    public String getProperty(String key) {
        return this.configurations.get(key);
    }

    public String getBrowserConfiguration() {
        return getProperty("Browser");
    }

    public String getOrgTypeConfiguration() {
        return getProperty("OrgType");
    }

    public String getEnvironmentConfiguration() {
        return getProperty("Environment");
    }

    public String getResourceConfigFileFlag() {
        return getProperty("ResourceConfigFileFlag");
    }

    public String getResourceConfigFile() {
        return getProperty("ResourceConfigFile");
    }
}
