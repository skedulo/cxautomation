package Base;

import Objects.User;
import Pages.PageManager;
import Pages.Salesforce.SfBasePage;
import Pages.Salesforce.SfLoginPage;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import java.nio.file.Paths;
import java.util.ArrayList;

import static Base.BaseUIActions.*;
import static Base.WebDriverManager.initializeWebDriver;

public class BaseUtil {

    public static WebDriver driver;

    public static ExtentReports extentReports;

    public static ExtentTest features;

    public static ExtentTest scenarioDef;

    public static ExtentTest stepDef;

    public static String reportLocation = Paths.get(System.getProperty("user.dir"), "report").toString();

    public static String needToDeletePropertyFile = Paths.get(System.getProperty("user.dir"), "config", "TestData", "ObjectsNeedToDelete.txt").toString();

    public static User userContext;

    public static HashMapHelper scenarioContext;

    public static HashMapHelper executionContext;

    public static BaseConfiguration baseConfiguration = new BaseConfiguration();

    public static final int defaultWaitTime = 30;

    public static boolean hasUserLoggedIn = false;

    public static boolean browserInitializeFlag = false;

    public static boolean createNewFeatureFlag = true;

    public static SoftAssert softAssert;

    public static void initializeBrowserAndLoggedInSalesforce() {
        PageManager pageManager = new PageManager(driver);
        SfBasePage basePage = pageManager.getSfBasePage();
        SfLoginPage loginPage = pageManager.getSfLoginPage();

        if (hasUserLoggedIn) {
            LogHelper.getLogger().info("There is a user logged into Salesforce");
            driver.switchTo().defaultContent();

            LogHelper.getLogger().info("Remove unnecessary tabs");
            ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
            for (int i = 1; i < tabs.size(); i++) {
                driver.switchTo().window(tabs.get(i));
                driver.close();
            }
            driver.switchTo().window(tabs.get(0));
            LogHelper.getLogger().info("Open Salesforce home page");
            basePage.openHomePage();
            waitForPageToLoad();
            if (checkElementExist(By.cssSelector("a.switch-to-lightning")))
                click(driver.findElement(By.cssSelector("a.switch-to-lightning")));
            LogHelper.getLogger().info("Check if current logged user is as same as user context");
            if (basePage.isContextUserLoggedIn()) {
                LogHelper.getLogger().info("Correct user has been logged into Salesforce");
            } else {
                LogHelper.getLogger().info("Not correct user has been logged into Salesforce");
                LogHelper.getLogger().info("Quit current browser");
                driver.quit();
                browserInitializeFlag = false;
                LogHelper.getLogger().info("Initialize new web browser");
                initializeWebDriver();
                LogHelper.getLogger().info("Log into Salesforce");
                loginPage.open();
                loginPage.login(userContext.getUsername(), userContext.getPassword());
                hasUserLoggedIn = true;
            }
        } else {
            loginPage.open();
            loginPage.login(userContext.getUsername(), userContext.getPassword());
            hasUserLoggedIn = true;
        }
    }
}

