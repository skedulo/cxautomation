package Base;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class BaseUIActions extends BaseUtil {
    public static void waitForElement(WebElement element, int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
        try {
            WebDriverWait wait = new WebDriverWait(driver, seconds);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Throwable t) {
            LogHelper.getLogger().error(Arrays.toString(t.getStackTrace()));
        } finally {
            driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        }
    }

    public static void waitForElement(WebElement element) {
        waitForElement(element, defaultWaitTime);
    }

    public static void waitForElements(List<WebElement> elements, int seconds) {
        for (WebElement element : elements)
            waitForElement(element, seconds);
    }

    public static void waitForElements(List<WebElement> elements) {
        waitForElements(elements, defaultWaitTime);
    }

    public static void waitForPageToLoad(int seconds) throws NullPointerException {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        ExpectedCondition<Boolean> pageLoadCondition = driver -> ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
        wait.until(pageLoadCondition);
    }

    public static void waitForPageToLoad() {
        waitForPageToLoad(defaultWaitTime);
    }

    public static boolean checkElementExist(By locator) {
        return checkElementExist(locator, 1);
    }

    public static boolean checkElementExist(By locator, int seconds) {
        driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
        boolean result = driver.findElements(locator).size() != 0;
        driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        return result;
    }

    public static boolean checkElementExist(WebElement element) {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        Boolean result = false;
        try {
            result = element.isDisplayed();
        } catch (Exception e) {
            LogHelper.getLogger().info(Arrays.toString(e.getStackTrace()));
        }
        driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
        return result;
    }

    public static void waitForSpinnerToDisappear() {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        int size = driver.findElements(By.cssSelector(".slds-spinner")).size();
        int count = 10;
        try {
            while (count > 0 && size > 0) {
                Thread.sleep(1000);
                count--;
                size = driver.findElements(By.cssSelector(".slds-spinner")).size();
            }
        } catch (InterruptedException e) {
            LogHelper.getLogger().info(e.getMessage());
            LogHelper.getLogger().info(e.getStackTrace());
        }
        driver.manage().timeouts().implicitlyWait(defaultWaitTime, TimeUnit.SECONDS);
    }

    public static void click(WebElement element) {
        click(element, defaultWaitTime);
    }

    public static void click(WebElement element, int waitSeconds) {
        try {
            waitForPageToLoad();
            waitForElement(element, waitSeconds);
            element.click();
        } catch (ElementClickInterceptedException e) {
            Actions actions = new Actions(driver);
            actions.moveToElement(element).click().build().perform();
        } catch (WebDriverException e) {
            LogHelper.getLogger().error("Error on click element");
            LogHelper.getLogger().error(e.getMessage());
            LogHelper.getLogger().error(e.getStackTrace());
        }
    }

    public static void sendKeys(WebElement element, String keys) {
        waitForElement(element);
        element.clear();
        element.sendKeys(keys);
    }

    public static void selectDropdownlistByText(WebElement element, String value) {
        waitForElement(element);
        Select select = new Select(element);
        select.selectByVisibleText(value);
    }

    public static void selectDropdownlistByIndex(WebElement element, int value) {
        waitForElement(element);
        Select select = new Select(element);
        select.selectByIndex(value);
    }

    public static void selectDropdownlistByValue(WebElement element, String value) {
        waitForElement(element);
        Select select = new Select(element);
        select.selectByValue(value);
    }

    public static void dragAndDrop(WebElement from, WebElement to) {
        Actions actions = new Actions(driver);
        actions.dragAndDrop(from, to).build().perform();
    }

    public static void dragAndDropBy(WebElement from, int x, int y) {
        Actions actions = new Actions(driver);
        actions.dragAndDropBy(from, x, y).build().perform();
    }

    public static void switchToiFrame(WebDriver driver) {
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe")));
    }

    public static void setCheckbox(WebElement checkbox, Boolean checked) {
        Boolean currentStatus = checkbox.getAttribute("class").contains("ng-not-empty");
        if (currentStatus != checked) {
            click(checkbox.findElement(By.xpath(".//parent::*")));
        }
    }

    public static HashMap<String, Integer> getColumnIndexForSalesforceTable(WebElement table) {
        HashMap<String, Integer> columnIndex = new HashMap<>();
        List<WebElement> headers = table.findElements(By.xpath("./thead/tr/th"));
        for (int i = 0; i < headers.size(); i++) {
            String columnName = headers.get(i).getAttribute("aria-label");
            if (columnName != null)
                columnIndex.put(columnName.replace("Finish", "End"), i);
        }
        return columnIndex;
    }

    public static void sortSalesforceTable(WebElement table, String field, String sortType) {
        List<WebElement> headers = table.findElements(By.xpath("./thead/tr/th"));
        for (WebElement header : headers) {
            String columnName = header.getAttribute("aria-label");
            if (columnName.equals(field)) {
                if (!header.getAttribute("class").contains(sortType)) {
                    click(header.findElement(By.xpath(".//a")));
                    waitForSpinnerToDisappear();
                }
                break;
            }
        }
    }

    public static String selectDropdownRandomly(WebElement dropdown) {
        Select select = new Select(dropdown);
        int size = select.getOptions().size();
        select.selectByIndex(new Random().nextInt(size - 1) + 1);
        return select.getFirstSelectedOption().getText();
    }
}
