package APIHelper;

import Base.JSONHandler;
import Base.LogHelper;
import org.apache.commons.collections4.MultiValuedMap;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import static Base.BaseUtil.needToDeletePropertyFile;
import static Base.BaseUtil.userContext;
import static Utilities.GenericFunctions.sleep;
import static Utilities.GenericFunctions.storePropertyToFile;

public class SkeduloAPI {

    private String baseUrl = "https://api.skedulo.com";
    private HttpURLConnection connection;
    private String operationName;
    private String query;
    private JSONObject variables;

    public SkeduloAPI() {
    }

    public SkeduloAPI(String operationName, String query, JSONObject variables) {
        this.operationName = operationName;
        this.query = query;
        this.variables = variables;
    }

    public String post() {
        String fullResponse = "";
        try {
            URL url = new URL(baseUrl + "/graphql/graphql");
            this.connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", userContext.getValidSkeduloToken());

            connection.setDoOutput(true);
            DataOutputStream outStream = new DataOutputStream(connection.getOutputStream());
            String body = ParameterStringBuilder.getJSONBodyString(operationName, query, variables);
            outStream.writeBytes(body);
            outStream.flush();
            outStream.close();

            fullResponse = ResponseBuilder.getFullResponse(connection);
            connection.disconnect();
            LogHelper.getLogger().info(fullResponse);
        } catch (IOException e) {
            LogHelper.getLogger().error("Error in sending request " + operationName);
            LogHelper.getLogger().info("Query: " + query);
            LogHelper.getLogger().info("Variables: " + variables);
            LogHelper.getLogger().info(e.getStackTrace());
        }
        return fullResponse;
    }

    public boolean isTokenValid() {
        boolean result = false;
        try {
            URL url = new URL(baseUrl + "/auth/whoami");
            this.connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", userContext.getValidSkeduloToken());
            if (connection.getResponseCode() == 200) {
                result = true;
                LogHelper.getLogger().info("Skedulo token is still valid");
            } else
                LogHelper.getLogger().info("Skedulo token is expired or invalid");
            connection.disconnect();
        } catch (IOException e) {
            LogHelper.getLogger().error("Error in sending request " + connection.getURL());
            LogHelper.getLogger().info(e.getStackTrace());
        }
        return result;
    }

    public void deleteSingleObjects(String object, String UID) {
        this.query = String.format("mutation deleteObject($deleteInput: ID!) {  schema {  delete%s(UID: $deleteInput)  }}", object);
        this.operationName = "deleteObject";
        this.variables = new JSONObject().put("deleteInput", UID);
        this.post();// Todo: Handle deleteJob response
    }

    public void deleteMultipleObjects(MultiValuedMap<String, String> objects) {
        if (objects.size() > 0) {
            StringBuilder query = new StringBuilder();
            query.append("mutation delete { schema {");

            for (Map.Entry<String, String> entry : objects.entries()) {
                String key = entry.getKey();
                String value = entry.getValue();
                String deleteStatement = String.format(" Delete_%s_%s: delete%s(UID:\"%s\") ", key, value, key, value);
                query.append(deleteStatement);
            }
            query.append("}}");

            this.operationName = "delete";
            this.query = query.toString();
            this.variables = null;
            this.post();
        }
    }

    public String getJobID(String filter) {
        return getJobInfo("UID", filter);
    }

    public String getJobInfo(String field, String filter) {
        this.query = "query jobSearch($filter : EQLQueryFilterJobs! ){jobs(filter:$filter){edges {node {UID Name Description Start End Type Urgency }}}}";
        this.operationName = "jobSearch";
        this.variables = new JSONObject().put("filter", filter);
        return sendRequestToGet(field);
    }

    public String getObjectInfo(String object, String field, String filter) {
        this.query = String.format("query %s{%s(filter:\"%s\"){edges{ node{ %s}}}}", object, object, filter, field);
        this.operationName = object;
        this.variables = null;
        return sendRequestToGet(field);
    }

    public String getResourceInfo(String field, String filter) {
        this.query = "query resources($filter:EQLQueryFilterResources!){ resources(filter:$filter){ edges{ node { UID Name } } }}";
        this.operationName = "resources";
        this.variables = new JSONObject().put("filter", filter);
        return sendRequestToGet(field);
    }

    public String insertObjects(String objectType, String variables) {
        String uid = null;
        JSONObject jsonVariables = new JSONObject(variables);
        this.operationName = String.format("insert%s", objectType);
        this.query = String.format("mutation %s($createInput: New%s!) {schema {%s(input: $createInput)}}", operationName, objectType, operationName);
        this.variables = new JSONObject().put("createInput", jsonVariables);
        int count = 3;
        String response = this.post();
        while (response.contains("\"data\":null") && count > 0) {
            sleep(2);
            response = this.post();
            count--;
        }
        if (!response.contains("\"data\":null")) {
            Properties properties = new Properties();
            try {
                properties.load(new StringReader(response));
            } catch (IOException ex) {
                LogHelper.getLogger().error(Arrays.toString(ex.getStackTrace()));
            }
            JSONHandler responseObject = new JSONHandler(properties.get("Response").toString());
            uid = responseObject.findJSONValueByKey(operationName).toString();
            storePropertyToFile(objectType, uid, needToDeletePropertyFile);
            LogHelper.getLogger().info(String.format("%s is created successfully with uid %s", objectType, uid));
        } else
            LogHelper.getLogger().info(String.format("%s is not created", objectType));
        return uid;
    }

    private String sendRequestToGet(String field) {
        Object result = null;
        try {
            int count = 5;
            while (result == null && count > 0) {
                String fullResponse = this.post();
                Properties properties = new Properties();
                properties.load(new StringReader(fullResponse));
                JSONHandler response = new JSONHandler(properties.get("Response").toString());
                result = response.findJSONValueByKey(field);
                if (result != null) {
                    LogHelper.getLogger().info(String.format("Return %s : %s", field, result.toString()));
                    break;
                }
                count--;
                Thread.sleep(1000); // Job is created through Salesforce need some time to be fetched
            }
        } catch (IOException | InterruptedException e) {
            LogHelper.getLogger().error("Error in sending Skedulo request");
            LogHelper.getLogger().info(this.query);
            LogHelper.getLogger().info(variables.toString());
            LogHelper.getLogger().info(e.getStackTrace());
        }
        return result.toString();
    }
}
