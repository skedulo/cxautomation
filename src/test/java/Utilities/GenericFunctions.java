package Utilities;

import APIHelper.SkeduloAPI;
import Base.JSONHandler;
import Base.LogHelper;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

import static Base.BaseUtil.userContext;

public class GenericFunctions {

    public static String generateStringWithTimeStamp(String prefix, String suffix) {
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyyMMdd_HHmmss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
        return prefix + dateFormatGmt.format(new Date()) + suffix;
    }

    private static String getDayOfWeek(int value) {
        String day = "";
        switch (value) {
            case 1:
                day = "Sunday";
                break;
            case 2:
                day = "Monday";
                break;
            case 3:
                day = "Tuesday";
                break;
            case 4:
                day = "Wednesday";
                break;
            case 5:
                day = "Thursday";
                break;
            case 6:
                day = "Friday";
                break;
            case 7:
                day = "Saturday";
                break;
        }
        return day;
    }

    public static String generateDate(String dateParam) {
        return generateDate(dateParam, userContext.getTimezone());
    }

    public static String generateDate(String dateParam, String timezone) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone(timezone));
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone(timezone));
        c.setTime(new Date());

        switch (dateParam) {
            case "Today":
                break;

            case "Tomorrow":
                c.add(Calendar.DATE, 1);
                break;

            default:
                // Example: Next Monday
                if (dateParam.contains("Next ")) {
                    String weekDay = dateParam.replace("Next ", "");
                    c.add(Calendar.DATE, 1);
                    while (!weekDay.toLowerCase().equals(getDayOfWeek(c.get(Calendar.DAY_OF_WEEK)).toLowerCase())) {
                        c.add(Calendar.DATE, 1);
                    }
                    break;
                }

                // Example: At 10/08/2019
                if (dateParam.contains("At ")) {
                    try {
                        c.setTime(dateFormat.parse(dateParam.replace("At ", "")));
                    } catch (ParseException e) {
                        LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
                    }
                    break;
                }

                // Example: 5 Days Later
                if (dateParam.contains(" Days Later")) {
                    int days = Integer.parseInt(dateParam.replace(" Days Later", ""));
                    c.add(Calendar.DATE, days);
                    break;
                }
        }
        return dateFormat.format(c.getTime());
    }

    public static void storePropertyToFile(String property, String value, String propertyFile) {
        try (FileWriter fw = new FileWriter(propertyFile, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(String.format("%s:%s", property, value));
        } catch (IOException io) {
            LogHelper.getLogger().error(io.getStackTrace());
        }
    }

    public static void storeObjectUIDsToFile(String object, String filter, String propertyFile) {
        MultiValuedMap<String, String> objects = queryObjects(object, filter);
            for (Map.Entry<String, String> entry : objects.entries()) {
                String key = entry.getKey();
                String value = entry.getValue();
                storePropertyToFile(key, value, propertyFile);
            }
    }

    public static void removeLineFromFile(String lineToRemove, String propertyFile) {
        List<String> lines = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(propertyFile))) {
            stream.filter(line -> !line.trim().equals(lineToRemove))
                    .forEach((line) -> lines.add(line));
        } catch (IOException e) {
            LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
        }
        try (FileWriter fw = new FileWriter(propertyFile, false);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            for (String line : lines)
                out.println(line);
        } catch (IOException io) {
            LogHelper.getLogger().error(io.getStackTrace());
        }
    }

    public static String formatDateTime(String input, Integer duration, String inputFormat, String outputFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(inputFormat);

        Calendar c = Calendar.getInstance();
        try {
            c.setTime(dateFormat.parse(input));
        } catch (ParseException e) {
            LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
        }
        c.add(Calendar.MINUTE, duration);

        SimpleDateFormat out = new SimpleDateFormat(outputFormat);
        return out.format(c.getTime());
    }

    public static String formatDateTime(String date, String time, Integer duration, String outputFormat) {
        return formatDateTime(String.format("%s %s", date, time), duration, "dd/MM/yyyy HH:mm", outputFormat);
    }

    public static String formatDateTime(String input, String inputFormat, String outputFormat) {
        SimpleDateFormat in = new SimpleDateFormat(inputFormat);
        SimpleDateFormat out = new SimpleDateFormat(outputFormat);
        Date date = null;
        try {
            date = in.parse(input);
        } catch (ParseException e) {
            LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
        }
        return out.format(date.getTime());
    }

    public static String convertDateTimeWithTimezone(String inDateTime, String inFormat, String inTimezone, String outFormat, String outTimezone) {
        SimpleDateFormat in = new SimpleDateFormat(inFormat);
        in.setTimeZone(TimeZone.getTimeZone(inTimezone));
        Date date = null;
        try {
            date = in.parse(inDateTime);
        } catch (ParseException e) {
            LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
        }
        SimpleDateFormat out = new SimpleDateFormat(outFormat);
        out.setTimeZone(TimeZone.getTimeZone(outTimezone));
        return out.format(date);
    }

    public static long calculateDateTimeBetween(String start, String end, String format, String timeUnit) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
        } catch (ParseException e) {
            LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
        }
        long diff = endDate.getTime() - startDate.getTime();

        switch (timeUnit) {
            case "Milliseconds":
                return diff;
            case "Seconds":
                return diff / 1000;
            case "Hours":
                return diff / 1000 / 60 / 60;
            case "Days":
                return diff / 1000 / 60 / 60 / 24;
            case "Months":
                return diff / 1000 / 60 / 60 / 24 / 30;
            case "Years":
                return diff / 1000 / 60 / 60 / 24 / 365;
            case "Minutes":
            default:
                return diff / 1000 / 60;

        }
    }

    public static void cleanUpResourceRelatedObjects(String type, String resource) throws IOException {
        deleteObjects(getResourceRelatedObjects(type, resource));
    }

    public static void deleteObjects(MultiValuedMap<String, String> objects) {
        new SkeduloAPI().deleteMultipleObjects(objects);
    }

    public static MultiValuedMap<String, String> getResourceRelatedObjects(String type, String resource) throws IOException {
        String operation = type;
        String query;
        switch (type) {
            case "shifts":
                query = String.format("query %s { %s(filter:\"UID IN (SELECT ShiftId FROM ResourceShifts WHERE Resource.Name =='%s')\") { edges{ node{ UID } } }}", operation, operation, resource);
                break;
            case "jobs":
                query = String.format("query %s { %s(filter:\"UID IN (SELECT JobId FROM JobAllocations WHERE Resource.Name =='%s')\") { edges{ node{ UID } } }}", operation, operation, resource);
                break;
            default:
                query = String.format("query %s { %s(filter:\"Resource.Name =='%s'\") { edges{ node{ UID } } }}", operation, operation, resource);
                break;
        }

        SkeduloAPI skeduloAPI = new SkeduloAPI(operation, query, null);
        String fullResponse = skeduloAPI.post();

        Properties properties = new Properties();
        properties.load(new StringReader(fullResponse));
        JSONHandler response = new JSONHandler(properties.get("Response").toString());
        JSONArray listUID = (JSONArray) response.findJSONValueByKey("edges");

        MultiValuedMap<String, String> objects = new ArrayListValuedHashMap<>();

        String key = operation.substring(0, 1).toUpperCase() + operation.substring(1);

        for (int i = 0; i < listUID.length(); i++) {
            JSONHandler j = new JSONHandler(listUID.getJSONObject(i));
            objects.put(key, j.findJSONValueByKey("UID").toString());
        }
        return objects;
    }

    public static MultiValuedMap<String, String> queryObjects(String object, String filter) {
        String operation = object;
        String query = String.format("query %s { %s(filter:\"%s\") { edges{ node{ UID } } }}", operation, operation, filter);
        SkeduloAPI skeduloAPI = new SkeduloAPI(operation, query, null);
        String fullResponse = skeduloAPI.post();
        Properties properties = new Properties();
        try {
            properties.load(new StringReader(fullResponse));
        } catch (IOException e) {
            LogHelper.getLogger().error(Arrays.toString(e.getStackTrace()));
        }
        JSONHandler response = new JSONHandler(properties.get("Response").toString());
        JSONArray listUID = (JSONArray) response.findJSONValueByKey("edges");
        MultiValuedMap<String, String> objects = new ArrayListValuedHashMap<>();
        String key = operation.substring(0, 1).toUpperCase() + operation.substring(1);
        for (int i = 0; i < listUID.length(); i++) {
            JSONHandler j = new JSONHandler(listUID.getJSONObject(i));
            objects.put(key, j.findJSONValueByKey("UID").toString());
        }
        return objects;
    }

    public static void sleep(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


